FROM openjdk:8
EXPOSE 8080
ADD build/libs/*.jar app.jar
ADD newrelic/newrelic.jar newrelic.jar
ADD newrelic/newrelic.yml newrelic.yml
ENTRYPOINT ["java", "-javaagent:newrelic.jar","-jar","app.jar"]
