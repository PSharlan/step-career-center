package by.itstep.career.utils;

import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.consultation.request.ConsultationEntryUpdateRequest;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import by.itstep.career.model.story.request.StoryCreationRequest;
import by.itstep.career.model.story.request.StoryUpdateRequest;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.enums.Role;
import by.itstep.career.model.user.request.ChangePasswordRequest;
import by.itstep.career.model.user.request.UserCreationRequest;
import by.itstep.career.model.user.request.UserUpdateRequest;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.model.vacancy.request.VacancyUpdateRequest;
import com.github.javafaker.Faker;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import static by.itstep.career.model.consultation.enums.ConsultationStatus.NEW;
import static by.itstep.career.model.user.enums.Role.*;
import static by.itstep.career.model.vacancy.enums.VacancyExperience.JUNIOR;
import static by.itstep.career.model.vacancy.enums.VacancyExperience.MIDDLE;
import static java.time.LocalDateTime.now;

public class TestUtils {

    //FIXME ?
    private static final Faker FAKE = Faker.instance(Locale.US, ThreadLocalRandom.current());
    public static final String SUPER_TOKEN;
    public static final String CONSULTANT_TOKEN;
    public static final String ADMIN_TOKEN;
    public static final String EVENT_MANAGER_TOKEN;
    public static final String VACANCY_MANAGER_TOKEN;
    public static final String STORY_MANAGER_TOKEN;
    private final static long TIME_OUT = 600000; // 10 min
    private final static String SECRET;
    private static final String PREFIX = "Bearer_";

    static {
        SECRET = Base64.getEncoder().encodeToString("secret".getBytes());
        SUPER_TOKEN = createSuperToken();
        ADMIN_TOKEN = createAdminToken();
        CONSULTANT_TOKEN = createConsultantToken();
        EVENT_MANAGER_TOKEN = createEventManagerToken();
        VACANCY_MANAGER_TOKEN = createVacancyManagerToken();
        STORY_MANAGER_TOKEN = createStoryManagerToken();
    }

    // USER
    public static UserCreationRequest generateUserCreationRequest() {
        UserCreationRequest req = new UserCreationRequest();
        req.setFirstName(FAKE.name().firstName());
        req.setLastName(FAKE.name().lastName());
        req.setEmail(FAKE.internet().emailAddress());
        req.setPassword(FAKE.bothify("?????#####"));
        req.setUsername(FAKE.name().username());
        return req;
    }

    public static ChangePasswordRequest generateChangePasswordRequest(String email, String oldPass, String newPass) {
        ChangePasswordRequest req = new ChangePasswordRequest();
        req.setEmail(email);
        req.setOldPassword(oldPass);
        req.setNewPassword(newPass);
        return req;
    }

    public static UserUpdateRequest generateUserUpdateRequest(UUID id) {
        UserUpdateRequest req = new UserUpdateRequest();
        req.setId(id);
        req.setFirstName(FAKE.name().firstName());
        req.setLastName(FAKE.name().lastName());
        return req;
    }

    public static String createToken(User user) {
        Claims claims = Jwts.claims().setSubject(user.getUsername());
        claims.put("roles", getRoleNames(user.getRoles()));
        claims.put("name", user.getUsername());
        Date now = new Date();
        Date validity = new Date(new Date().getTime() + TIME_OUT);

        return PREFIX + Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }

    private static String createSuperToken() {
        User User = new User();
        User.setUsername("superAdmin");
        User.setLastName("superAdmin");
        User.setFirstName("superAdmin");
        User.setEmail("superAdmin@email.com");
        User.setRoles(Arrays.asList(Role.values()));

        return createToken(User);
    }

    private static String createAdminToken() {
        User User = new User();
        User.setUsername("superAdmin");
        User.setLastName("superAdmin");
        User.setFirstName("superAdmin");
        User.setEmail("superAdmin@email.com");
        User.setRoles(Collections.singletonList(ROLE_ADMIN));

        return createToken(User);
    }

    private static String createConsultantToken() {
        User User = new User();
        User.setUsername("superAdmin");
        User.setLastName("superAdmin");
        User.setFirstName("superAdmin");
        User.setEmail("superAdmin@email.com");
        User.setRoles(Collections.singletonList(ROLE_CONSULTANT));

        return createToken(User);
    }

    private static String createEventManagerToken() {
        User user = new User();
        user.setUsername("superAdmin");
        user.setLastName("superAdmin");
        user.setFirstName("superAdmin");
        user.setEmail("superAdmin@email.com");
        user.setRoles(Collections.singletonList(ROLE_EVENT_MANAGER));

        return createToken(user);
    }

    private static String createVacancyManagerToken() {
        User user = new User();
        user.setUsername("superAdmin");
        user.setLastName("superAdmin");
        user.setFirstName("superAdmin");
        user.setEmail("superAdmin@email.com");
        user.setRoles(Collections.singletonList(ROLE_VACANCY_MANAGER));

        return createToken(user);
    }

    private static String createStoryManagerToken() {
        User user = new User();
        user.setUsername("superAdmin");
        user.setLastName("superAdmin");
        user.setFirstName("superAdmin");
        user.setEmail("superAdmin@email.com");
        user.setRoles(Collections.singletonList(ROLE_STORY_MANAGER));

        return createToken(user);
    }

    private static List<String> getRoleNames(List<Role> roles) {
        return roles.stream().map(Enum::name).collect(Collectors.toList());
    }

    // CONSULTATION
    public static ConsultationEntryCreationRequest generateConsultationEntryCreationRequest() {
        ConsultationEntryCreationRequest req = new ConsultationEntryCreationRequest();

        req.setFirstName(FAKE.name().firstName());
        req.setLastName(FAKE.name().lastName());
        req.setEmail("some-email@notexisting.com");
        req.setPhone(FAKE.bothify("### ## #######"));
        req.setStudent(true);
        req.setGroup(FAKE.bothify("??####"));
        req.setComment("Very important comment!");
        req.setAssignedAt(now());
        return req;
    }

    public static ConsultationEntryUpdateRequest generateConsultationEntryUpdateRequest(UUID id) {
        ConsultationEntryUpdateRequest req = new ConsultationEntryUpdateRequest();

        req.setId(id);
        req.setConsultationStatus(NEW);

        req.setFirstName(FAKE.name().firstName());
        req.setLastName(FAKE.name().lastName());
        req.setEmail("updated-email@notexisting.com");
        req.setPhone(FAKE.bothify("### ## #######"));
        req.setStudent(true);
        req.setGroup(FAKE.bothify("??####"));
        req.setComment("Very important updated comment!");
        req.setAssignedAt(now());
        return req;
    }

    // EVENT
    public static EventCreationRequest generateEventCreationRequest() {
        EventCreationRequest req = new EventCreationRequest();

        req.setAddress("Some address");
        req.setDescription("Very interesting description");
        req.setName("Very interesting event");
        req.setTime(now());
        req.setType("Open lesson");
        return req;
    }

    public static EventUpdateRequest generateEventUpdateRequest(UUID id) {
        EventUpdateRequest req = new EventUpdateRequest();

        req.setId(id);
        req.setAddress("Some updated address");
        req.setDescription("Very interesting updated description");
        req.setName("Very interesting updated event");
        req.setTime(now());
        req.setType("Workshop");
        return req;
    }

    // EVENT ENTRY
    public static EventEntryCreationRequest generateEventEntryCreationRequest(UUID eventId) {
        EventEntryCreationRequest req = new EventEntryCreationRequest();

        req.setCompany(FAKE.company().name());
        req.setEmail("pasha.sharlan@gmail.com");
        req.setEventId(eventId);
        req.setFirstName(FAKE.name().firstName());
        req.setLastName(FAKE.name().lastName());
        req.setStudent(false);
        return req;
    }

    // VACANCY
    public static VacancyCreationRequest generateVacancyCreationRequest() {
        VacancyCreationRequest req = new VacancyCreationRequest();

        req.setCompany(FAKE.company().name());
        req.setDescription("Very interesting description");
        req.setName("Very interesting event");
        req.setExperience(JUNIOR);
        return req;
    }

    public static VacancyUpdateRequest generateVacancyUpdateRequest(UUID id) {
        VacancyUpdateRequest req = new VacancyUpdateRequest();

        req.setId(id);
        req.setCompany(FAKE.company().name());
        req.setDescription("Very interesting updated description");
        req.setName("Very interesting updated event");
        req.setExperience(MIDDLE);
        return req;
    }

    // STORY
    public static StoryCreationRequest generateStoryCreationRequest() {
        StoryCreationRequest req = new StoryCreationRequest();

        req.setAuthorName(FAKE.name().name());
        req.setDescription("Very interesting description");
        req.setHeader("Very interesting story");
        req.setVideoUrl("https://www.youtube.com/watch?v=bhz85_3YHVU");
        return req;
    }

    public static StoryUpdateRequest generateStoryUpdateRequest(UUID id) {
        StoryUpdateRequest req = new StoryUpdateRequest();

        req.setId(id);
        req.setAuthorName("Some updated name");
        req.setDescription("Very interesting updated description");
        req.setHeader("Very interesting updated story");
        req.setVideoUrl("https://www.youtube.com/watch?v=ec9_1-0xE4k");
        return req;
    }

}
