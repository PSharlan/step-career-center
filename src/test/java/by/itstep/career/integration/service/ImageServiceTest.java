package by.itstep.career.integration.service;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.image.Image;
import org.junit.Test;
import org.springframework.web.multipart.MultipartFile;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.*;
import static by.itstep.career.service.impl.AmazonServiceImpl.ImageType.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;

public class ImageServiceTest extends AbstractIntegrationTest {

    @Test
    public void saveEventImage_happyPath() throws Exception {
        // GIVEN
        MultipartFile image = getMockMultiPartFile();

        // WHEN
        Image saved = imageService.save(image, EVENT_IMAGE);

        // THEN
        assertSoftly(softly -> {
            softly.assertThat(saved.getId()).isNotNull();
            softly.assertThat(saved.getMain().contains(EVENT_MAIN.getFolder()));
            softly.assertThat(saved.getPreview().contains(EVENT_PREVIEW.getFolder()));
        });
    }


    @Test
    public void saveCvImage_happyPath() throws Exception {
        // GIVEN
        MultipartFile image = getMockMultiPartFile();

        // WHEN
        Image saved = imageService.save(image, CV_IMAGE);

        // THEN
        assertSoftly(softly -> {
            softly.assertThat(saved.getId()).isNotNull();
            softly.assertThat(saved.getMain().contains(CV_MAIN.getFolder()));
        });
    }


    @Test
    public void saveStoryImage_happyPath() throws Exception {
        // GIVEN
        MultipartFile image = getMockMultiPartFile();

        // WHEN
        Image saved = imageService.save(image, STORY_IMAGE);

        // THEN
        assertSoftly(softly -> {
            softly.assertThat(saved.getId()).isNotNull();
            softly.assertThat(saved.getMain().contains(STORY_MAIN.getFolder()));
        });
    }

}
