package by.itstep.career.integration.api;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.consultation.ConsultationEntryPreview;
import by.itstep.career.model.consultation.enums.ConsultationStatus;
import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.consultation.request.ConsultationEntryUpdateRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;

import static by.itstep.career.model.consultation.enums.ConsultationStatus.*;
import static by.itstep.career.utils.TestUtils.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ConsultationEntryApiTest extends AbstractIntegrationTest {

    @Test
    public void getConsultationById_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        final ConsultationEntry entry = createConsultationEntry(req);

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/consultations/entries/{id}", entry.getId())
                            .header(AUTHORIZATION, ADMIN_TOKEN))
                            .andExpect(status().isOk())
                            .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final ConsultationEntry foundEntry = objectMapper.readValue(content, ConsultationEntry.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntry.getId()).isNotNull();
            softly.assertThat(foundEntry.getConsultationStatus()).isEqualTo(NEW);
            softly.assertThat(foundEntry.getConfirmedAt()).isNull();
            softly.assertThat(foundEntry.getEmail()).isEqualTo(entry.getEmail());
            softly.assertThat(foundEntry.getFirstName()).isEqualTo(entry.getFirstName());
        });
    }

    @Test
    public void getConsultationByIdIfNotExists() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/consultations/entries/{id}", id)
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void getConsultationByIdIfNotAdmin() throws Exception {
        //GIVEN
        UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(
                get(BASE_URL + "/consultations/entries/{id}", id));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getConsultationsByStatus_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();

        ConsultationEntry entry = createConsultationEntry(req);
        ConsultationEntry entry1 = createConsultationEntry(req);
        ConsultationEntry entry2 = createConsultationEntry(req);
        ConsultationEntry entry3 = createConsultationEntry(req);
        ConsultationEntry entry4 = createConsultationEntry(req);

        updateConsultationEntryStatus(entry.getId(), PROVIDED);
        updateConsultationEntryStatus(entry1.getId(), CONFIRMED);
        updateConsultationEntryStatus(entry2.getId(), CONFIRMED);
        updateConsultationEntryStatus(entry3.getId(), CANCELLED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/consultations/entries/status?status={status}&page={page}&size={size}", CONFIRMED, page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<ConsultationEntryPreview> foundEntries = objectMapper
                .readValue(content, new TypeReference<List<ConsultationEntryPreview>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(2);
        });
    }

    @Test
    public void getConsultationsByStatusIdIfNotAdmin() throws Exception {
        //GIVEN
        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(
                get(BASE_URL +
                        "/consultations/entries/status?status={status}&page={page}&size={size}", NEW, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getConsultationsByEmail_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        createConsultationEntry(req);
        createConsultationEntry(req);

        req.setEmail("another-email@notexisting.com");
        createConsultationEntry(req);
        createConsultationEntry(req);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/consultations/entries/email?email={email}&page={page}&size={size}", req.getEmail(), page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<ConsultationEntryPreview> foundEntries = objectMapper
                .readValue(content, new TypeReference<List<ConsultationEntryPreview>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(2);
        });
    }

    @Test
    public void getConsultationsByEmailIfNotExists() throws Exception {
        //GIVEN
        final String email = "notExisting@email.com";

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/consultations/entries/email?email={email}&page={page}&size={size}", email, page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<ConsultationEntryPreview> foundEntries = objectMapper
                .readValue(content, new TypeReference<List<ConsultationEntryPreview>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(0);
        });
    }

    @Test
    public void getConsultationsByEmailIfNotAdmin() throws Exception {
        //GIVEN
        String email = "random@gmail.com";

        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(
                get(BASE_URL +
                        "/consultations/entries/email?email={email}&page={page}&size={size}", email, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getConsultationsByPhone_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        createConsultationEntry(req);
        createConsultationEntry(req);

        req.setPhone("375 29 9999999");
        createConsultationEntry(req);
        createConsultationEntry(req);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/consultations/entries/phone?phone={phone}&page={page}&size={size}", req.getPhone(), page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<ConsultationEntryPreview> foundEntries = objectMapper
                .readValue(content, new TypeReference<List<ConsultationEntryPreview>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(2);
        });
    }

    @Test
    public void getConsultationsByPhoneIfNotExists() throws Exception {
        //GIVEN
        final String phone = "375 33 0000000";

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/consultations/entries/phone?phone={phone}&page={page}&size={size}", phone, page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<ConsultationEntry> foundEntries = objectMapper.readValue(content, new TypeReference<List<ConsultationEntry>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(0);
        });
    }

    @Test
    public void getConsultationsByPhoneIfNotAdmin() throws Exception {
        //GIVEN
        String phone = "375 29 3333333";

        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(
                get(BASE_URL +
                        "/consultations/entries/phone?phone={phone}&page={page}&size={size}", phone, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void createConsultation_happyPath() throws Exception {
        //GIVEN
        ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();

        //WHEN
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/consultations/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final ConsultationEntry entry = objectMapper.readValue(content, ConsultationEntry.class);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        //THEN
        assertSoftly(softly -> {
            softly.assertThat(entry.getId()).isNotNull();
            softly.assertThat(entry.getConsultationStatus()).isEqualTo(NEW);
            softly.assertThat(entry.getAssignedAt().format(format)).isEqualTo(req.getAssignedAt().format(format));
            softly.assertThat(entry.getComment()).isEqualTo(req.getComment());
            softly.assertThat(entry.getEmail()).isEqualTo(req.getEmail());
            softly.assertThat(entry.getConfirmedAt()).isNull();
        });
    }

    @Test
    public void createConsultationIfNotValid() throws Exception {
        //GIVEN
        ConsultationEntryCreationRequest invalidReq = generateConsultationEntryCreationRequest();
        invalidReq.setEmail("invalid.email");

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/consultations/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void getAllConsultations_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();

        ConsultationEntry entry = createConsultationEntry(req);
        ConsultationEntry entry1 = createConsultationEntry(req);
        ConsultationEntry entry2 = createConsultationEntry(req);
        ConsultationEntry entry3 = createConsultationEntry(req);

        updateConsultationEntryStatus(entry2.getId(), CONFIRMED);
        updateConsultationEntryStatus(entry3.getId(), CANCELLED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/consultations/entries?page={page}&size={size}", page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<ConsultationEntryPreview> foundEntries = objectMapper
                .readValue(content, new TypeReference<List<ConsultationEntryPreview>>() {});

        //THEN
        assertEquals(4, foundEntries.size());
    }

    @Test
    public void getAllConsultationsIfNotAdmin() throws Exception {
        //GIVEN
        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL +
                "/consultations/entries?page={page}&size={size}", page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void deleteConsultation_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        ConsultationEntry entry = createConsultationEntry(req);

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/consultations/entries/{id}", entry.getId())
                .header(AUTHORIZATION, CONSULTANT_TOKEN));

        //THEN
        resultAction.andExpect(status().isNoContent());
    }

    @Test
    public void deleteConsultationIfNotConsultant() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        final ConsultationEntry entry = createConsultationEntry(req);

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/consultations/entries/{id}", entry.getId())
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateConsultation_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        ConsultationEntry entry = createConsultationEntry(req);

        ConsultationEntryUpdateRequest entryUpdateReq = generateConsultationEntryUpdateRequest(entry.getId());

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/consultations/entries")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, CONSULTANT_TOKEN)
                .content(objectMapper.writeValueAsString(entryUpdateReq)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        ConsultationEntry updatedEntry = objectMapper.readValue(content, ConsultationEntry.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedEntry.getId()).isEqualTo(entry.getId());

            softly.assertThat(updatedEntry.getEmail()).isNotEqualTo(entry.getEmail());
            softly.assertThat(updatedEntry.getFirstName()).isNotEqualTo(entry.getFirstName());
            softly.assertThat(updatedEntry.getLastName()).isNotEqualTo(entry.getLastName());
        });
    }

    @Test
    public void updateConsultationIfNotConsultant() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        final ConsultationEntry entry = createConsultationEntry(req);

        final ConsultationEntryUpdateRequest entryUpdateReq = generateConsultationEntryUpdateRequest(entry.getId());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/consultations/entries")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(entryUpdateReq)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateConsultationIfNotValid() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        final ConsultationEntry entry = createConsultationEntry(req);

        final ConsultationEntryUpdateRequest entryUpdateReq = generateConsultationEntryUpdateRequest(entry.getId());
        entryUpdateReq.setFirstName("");

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/consultations/entries")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, CONSULTANT_TOKEN)
                .content(objectMapper.writeValueAsString(entryUpdateReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void updateConsultationStatus_happyPath() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        ConsultationEntry entry = createConsultationEntry(req);

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/consultations/entries/{id}/status", entry.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, CONSULTANT_TOKEN)
                .content(objectMapper.writeValueAsString(CONFIRMED)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        ConsultationEntry updatedEntry = objectMapper.readValue(content, ConsultationEntry.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedEntry.getId()).isEqualTo(entry.getId());
            softly.assertThat(updatedEntry.getEmail()).isEqualTo(entry.getEmail());
            softly.assertThat(updatedEntry.getFirstName()).isEqualTo(entry.getFirstName());
            softly.assertThat(updatedEntry.getLastName()).isEqualTo(entry.getLastName());


            softly.assertThat(updatedEntry.getConsultationStatus()).isNotEqualTo(entry.getConsultationStatus());
            softly.assertThat(updatedEntry.getConsultationStatus()).isEqualTo(CONFIRMED);
        });
    }

    @Test
    public void updateConsultationStatusIfNotConsultant() throws Exception {
        //GIVEN
        createRecipient();

        final ConsultationEntryCreationRequest req = generateConsultationEntryCreationRequest();
        final ConsultationEntry entry = createConsultationEntry(req);

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/consultations/entries/{id}/status", entry.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(CONFIRMED)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

}
