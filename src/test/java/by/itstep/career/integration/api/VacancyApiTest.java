package by.itstep.career.integration.api;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.vacancy.Vacancy;
import by.itstep.career.model.vacancy.VacancyPreview;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.model.vacancy.request.VacancyUpdateRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.UUID;

import static by.itstep.career.utils.TestUtils.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VacancyApiTest extends AbstractIntegrationTest {

    @Test
    public void createVacancy_happyPath() throws Exception {
        //GIVEN
        final VacancyCreationRequest req = generateVacancyCreationRequest();

        //WHEN
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/vacancies")
                .header(AUTHORIZATION, VACANCY_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Vacancy vacancy = objectMapper.readValue(content, Vacancy.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(vacancy.getId()).isNotNull();
            softly.assertThat(vacancy.getExperience()).isEqualTo(req.getExperience());
            softly.assertThat(vacancy.getName()).isEqualTo(req.getName());
            softly.assertThat(vacancy.getDescription()).isEqualTo(req.getDescription());
        });
    }

//    @Test
//    public void createVacancyIfNotValid() throws Exception {
//        //GIVEN
//        final VacancyCreationRequest invalidReq = generateVacancyCreationRequest();
//        invalidReq.setName("");
//
//        //WHEN
//        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/vacancies")
//                .header(AUTHORIZATION, VACANCY_MANAGER_TOKEN)
//                .contentType(APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(invalidReq)));
//
//        //THEN
//        resultAction.andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void createVacancyIfNotVacancyManager() throws Exception {
//        //GIVEN
//        final VacancyCreationRequest req = generateVacancyCreationRequest();
//
//        //WHEN
//        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/vacancies")
//                .header(AUTHORIZATION, ADMIN_TOKEN)
//                .contentType(APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(req)));
//
//        //THEN
//        resultAction.andExpect(status().isForbidden());
//    }
//
//    @Test
//    public void getVacancyById_happyPath() throws Exception {
//        //GIVEN
//        Vacancy saved = createVacancy(generateVacancyCreationRequest());
//
//        //WHEN
//        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/vacancies/{id}", saved.getId()))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        byte[] content = mvcResult.getResponse().getContentAsByteArray();
//        final Vacancy found = objectMapper.readValue(content, Vacancy.class);
//
//        //THEN
//        assertSoftly(softly -> {
//            softly.assertThat(found.getId()).isNotNull();
//            softly.assertThat(found.getId()).isEqualTo(saved.getId());
//            softly.assertThat(found.getExperience()).isEqualTo(saved.getExperience());
//            softly.assertThat(found.getName()).isEqualTo(saved.getName());
//            softly.assertThat(found.getDescription()).isEqualTo(saved.getDescription());
//        });
//    }
//
//    @Test
//    public void getVacancyByIdIfNotExists() throws Exception {
//        //GIVEN
//        final UUID id = UUID.randomUUID();
//
//        //WHEN
//        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/vacancies/{id}", id));
//
//        //THEN
//        resultAction.andExpect(status().isNotFound());
//    }
//
//    @Test
//    public void getAllVacancies_happyPath() throws Exception {
//        //GIVEN
//        VacancyCreationRequest req = generateVacancyCreationRequest();
//
//        final Vacancy vacancy = createVacancy(req);
//        final Vacancy vacancy1 = createVacancy(req);
//        final Vacancy vacancy2 = createVacancy(req);
//        final Vacancy vacancy3 = createVacancy(req);
//        final Vacancy vacancy4 = createVacancy(req);
//
//        deleteVacancy(vacancy.getId());
//        deleteVacancy(vacancy1.getId());
//
//        int page = 0;
//        int size = 10;
//
//        //WHEN
//        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
//                "/vacancies?page={page}&size={size}", page, size))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        byte[] content = mvcResult.getResponse().getContentAsByteArray();
//        List<VacancyPreview> vacanices = objectMapper.readValue(content, new TypeReference<List<VacancyPreview>>() {});
//
//        //THEN
//        assertEquals(3, vacanices.size());
//    }
//
//    @Test
//    public void updateVacancy_happyPath() throws Exception {
//        //GIVEN
//        final Vacancy saved = createVacancy(generateVacancyCreationRequest());
//        final VacancyUpdateRequest req = generateVacancyUpdateRequest(saved.getId());
//
//        //WHEN
//        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/vacancies")
//                .header(AUTHORIZATION, VACANCY_MANAGER_TOKEN)
//                .contentType(APPLICATION_JSON)
//                .content(objectMapper.writeValueAsString(req)))
//                .andExpect(status().isOk())
//                .andReturn();
//
//        byte[] content = mvcResult.getResponse().getContentAsByteArray();
//        final Vacancy updated = objectMapper.readValue(content, Vacancy.class);
//
//        //THEN
//        assertSoftly(softly -> {
//            softly.assertThat(updated.getId()).isEqualTo(saved.getId());
//
//            softly.assertThat(updated.getCompany()).isNotEqualTo(saved.getCompany());
//            softly.assertThat(updated.getExperience()).isNotEqualTo(saved.getExperience());
//            softly.assertThat(updated.getName()).isNotEqualTo(saved.getName());
//            softly.assertThat(updated.getDescription()).isNotEqualTo(saved.getDescription());
//        });
//    }
//
//    @Test
//    public void updateEventIfNotVacancyManager() throws Exception {
//        //GIVEN
//        final Vacancy saved = createVacancy(generateVacancyCreationRequest());
//        final VacancyUpdateRequest req = generateVacancyUpdateRequest(saved.getId());
//
//        //WHEN
//        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/vacancies")
//                .contentType(APPLICATION_JSON)
//                .header(AUTHORIZATION, ADMIN_TOKEN)
//                .content(objectMapper.writeValueAsString(req)));
//
//        //THEN
//        resultAction.andExpect(status().isForbidden());
//    }
//
//    @Test
//    public void updateEventIfNotValid() throws Exception {
//        //GIVEN
//        final Vacancy saved = createVacancy(generateVacancyCreationRequest());
//        final VacancyUpdateRequest req = generateVacancyUpdateRequest(saved.getId());
//        req.setName("");
//
//        //WHEN
//        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/vacancies")
//                .contentType(APPLICATION_JSON)
//                .header(AUTHORIZATION, VACANCY_MANAGER_TOKEN)
//                .content(objectMapper.writeValueAsString(req)));
//
//        //THEN
//        resultAction.andExpect(status().isBadRequest());
//    }
//
//    @Test
//    public void deleteVacancy_happyPath() throws Exception {
//        //GIVEN
//        final Vacancy saved = createVacancy(generateVacancyCreationRequest());
//
//        //WHEN
//        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/vacancies/{id}", saved.getId())
//                .header(AUTHORIZATION, VACANCY_MANAGER_TOKEN));
//
//        //THEN
//        resultAction.andExpect(status().isNoContent());
//    }
//
//    @Test
//    public void deleteVacancyIfNotVacancyManager() throws Exception {
//        //GIVEN
//        final UUID id = UUID.randomUUID();
//
//        //WHEN
//        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/vacancies/{id}", id)
//                .header(AUTHORIZATION, ADMIN_TOKEN));
//
//        //THEN
//        resultAction.andExpect(status().isForbidden());
//    }

}
