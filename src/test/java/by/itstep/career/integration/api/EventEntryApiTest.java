package by.itstep.career.integration.api;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventEntry;
import by.itstep.career.model.event.EventEntryPreview;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.UUID;

import static by.itstep.career.model.event.enums.EventStatus.FINISHED;
import static by.itstep.career.model.event.enums.EventStatus.PUBLISHED;
import static by.itstep.career.utils.TestUtils.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EventEntryApiTest extends AbstractIntegrationTest {

    @Test
    public void createEventEntry_happyPath() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());
        updateEventStatus(event.getId(), PUBLISHED);

        final EventEntryCreationRequest req = generateEventEntryCreationRequest(event.getId());

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/events/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final EventEntry saved = objectMapper.readValue(content, EventEntry.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(saved.getId()).isNotNull();
            softly.assertThat(saved.getCompany()).isEqualTo(req.getCompany());
            softly.assertThat(saved.getEmail()).isEqualTo(req.getEmail());
            softly.assertThat(saved.getFirstName()).isEqualTo(req.getFirstName());
            softly.assertThat(saved.getLastName()).isEqualTo(req.getLastName());
            softly.assertThat(saved.getEvent().getId()).isEqualTo(req.getEventId());
        });
    }

    @Test
    public void createEventEntryIfNotValid() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());
        updateEventStatus(event.getId(), PUBLISHED);

        final EventEntryCreationRequest invalidReq = generateEventEntryCreationRequest(event.getId());
        invalidReq.setFirstName("");

        //WHEN
        final ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/events/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void createEventEntryIfNotPublished() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());
        updateEventStatus(event.getId(), FINISHED);

        final EventEntryCreationRequest req = generateEventEntryCreationRequest(event.getId());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/events/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void createEventEntryIfNotExists() throws Exception {
        //GIVEN
        final EventEntryCreationRequest req = generateEventEntryCreationRequest(UUID.randomUUID());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/events/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void createEventEntryIfEmailIsTaken() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());
        updateEventStatus(event.getId(), PUBLISHED);

        final EventEntry entry = createEventEntry(generateEventEntryCreationRequest(event.getId()));

        final EventEntryCreationRequest req = generateEventEntryCreationRequest(event.getId());
        req.setEmail(entry.getEmail());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/events/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void getEventEntryById_happyPath() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());
        updateEventStatus(event.getId(), PUBLISHED);

        final EventEntry entry = createEventEntry(generateEventEntryCreationRequest(event.getId()));

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/events/entries/{id}", entry.getId())
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final EventEntry foundEntry = objectMapper.readValue(content, EventEntry.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntry.getId()).isNotNull();
            softly.assertThat(foundEntry.getId()).isEqualTo(entry.getId());
            softly.assertThat(foundEntry.getEvent().getId()).isEqualTo(entry.getEvent().getId());
            softly.assertThat(foundEntry.getLastName()).isEqualTo(entry.getLastName());
            softly.assertThat(foundEntry.getStudent()).isEqualTo(entry.getStudent());
            softly.assertThat(foundEntry.getEmail()).isEqualTo(entry.getEmail());
        });
    }

    @Test
    public void getEventEntryByIdIfNotExists() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/events/entries/{id}", id)
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void getEventEntryByIdIfNotAdmin() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/events/entries/{id}", id));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getEventEntriesByEmail_happyPath() throws Exception {
        //GIVEN
        final Event event1 = createEvent(generateEventCreationRequest());
        updateEventStatus(event1.getId(), PUBLISHED);
        final Event event2 = createEvent(generateEventCreationRequest());
        updateEventStatus(event2.getId(), PUBLISHED);

        EventEntryCreationRequest req1 = generateEventEntryCreationRequest(event1.getId());
        EventEntryCreationRequest req2 = generateEventEntryCreationRequest(event2.getId());
        EventEntryCreationRequest req3 = generateEventEntryCreationRequest(event2.getId());

        req2.setEmail("notexisting@moc.liamg");

        final EventEntry entry1 = createEventEntry(req1);
        final EventEntry entry2 = createEventEntry(req2);
        final EventEntry entry3 = createEventEntry(req3);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/events/entries/email?email={email}&page={page}&size={size}", entry1.getEmail(), page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<EventEntryPreview> foundEntries = objectMapper.readValue(content, new TypeReference<List<EventEntryPreview>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(2);
            softly.assertThat(foundEntries.get(0).getEventId()).isNotNull();
            softly.assertThat(foundEntries.get(0).getEventName()).isNotNull();

        });
    }

    @Test
    public void getEventEntriesByEmailIfNotAdmin() throws Exception {
        //GIVEN
        final String email = "random@email.com";

        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL +
                "/events/entries/email?email={email}&page={page}&size={size}", email, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getEventEntriesByEventId_happyPath() throws Exception {
        //GIVEN
        final Event event1 = createEvent(generateEventCreationRequest());
        updateEventStatus(event1.getId(), PUBLISHED);
        final Event event2 = createEvent(generateEventCreationRequest());
        updateEventStatus(event2.getId(), PUBLISHED);

        EventEntryCreationRequest req1 = generateEventEntryCreationRequest(event1.getId());
        EventEntryCreationRequest req2 = generateEventEntryCreationRequest(event2.getId());
        EventEntryCreationRequest req3 = generateEventEntryCreationRequest(event2.getId());

        req2.setEmail("notexisting@moc.liamg");

        final EventEntry entry1 = createEventEntry(req1);
        final EventEntry entry2 = createEventEntry(req2);
        final EventEntry entry3 = createEventEntry(req3);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/events/{id}/entries?page={page}&size={size}", event2.getId(), page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final List<EventEntryPreview> foundEntries = objectMapper.readValue(content, new TypeReference<List<EventEntryPreview>>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEntries.size()).isEqualTo(2);
            softly.assertThat(foundEntries.get(0).getEventId()).isNotNull();
            softly.assertThat(foundEntries.get(0).getEventName()).isNotNull();
        });
    }

    @Test
    public void getEventEntriesByEventIdIfNotAdmin() throws Exception {
        //GIVEN
        final UUID eventId = UUID.randomUUID();

        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL +
                "/events/{id}/entries?page={page}&size={size}", eventId, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getEventEntryByEventIdIfNotExists() throws Exception {
        //GIVEN
        final UUID eventId = UUID.randomUUID();

        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL +
                "/events/{id}/entries?page={page}&size={size}", eventId, page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void deleteEventEntry_happyPath() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());
        updateEventStatus(event.getId(), PUBLISHED);

        final EventEntry entry = createEventEntry(generateEventEntryCreationRequest(event.getId()));

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/events/entries/{id}", entry.getId())
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNoContent());
    }

    @Test
    public void deleteConsultationIfNotConsultant() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/events/entries/{id}", id)
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

}
