package by.itstep.career.integration.api;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.story.request.StoryCreationRequest;
import by.itstep.career.model.story.request.StoryUpdateRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.STORY_IMAGE;
import static by.itstep.career.model.story.enums.StoryStatus.NEW;
import static by.itstep.career.model.story.enums.StoryStatus.PUBLISHED;
import static by.itstep.career.utils.TestUtils.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class StoryApiTest extends AbstractIntegrationTest {

    @Test
    public void getStoryById_happyPath() throws Exception {
        //GIVEN
        final Story story = createStory(generateStoryCreationRequest());

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/stories/{id}", story.getId()))
                            .andExpect(status().isOk())
                            .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Story foundStory = objectMapper.readValue(content, Story.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundStory.getId()).isNotNull();
            softly.assertThat(foundStory.getStatus()).isEqualTo(NEW);
            softly.assertThat(foundStory.getAuthorName()).isEqualTo(story.getAuthorName());
            softly.assertThat(foundStory.getDescription()).isEqualTo(story.getDescription());
            softly.assertThat(foundStory.getVideoUrl()).isEqualTo(story.getVideoUrl());
            softly.assertThat(foundStory.getHeader()).isEqualTo(story.getHeader());
        });
    }

    @Test
    public void getStoryByIdIfNotExists() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/stories/{id}", id)
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void createStory_happyPath() throws Exception {
        //GIVEN
        final UUID imageId = addFakeImage(STORY_IMAGE);
        final StoryCreationRequest req = generateStoryCreationRequest();
        req.setImageId(imageId);

        //WHEN
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/stories")
                .header(AUTHORIZATION, STORY_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Story story = objectMapper.readValue(content, Story.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(story.getId()).isNotNull();
            softly.assertThat(story.getStatus()).isEqualTo(NEW);
            softly.assertThat(story.getHeader()).isEqualTo(req.getHeader());
            softly.assertThat(story.getVideoUrl()).isEqualTo(req.getVideoUrl());
            softly.assertThat(story.getAuthorName()).isEqualTo(req.getAuthorName());
            softly.assertThat(story.getDescription()).isEqualTo(req.getDescription());
        });
    }

    @Test
    public void createStoryIfNotValid() throws Exception {
        //GIVEN
        final StoryCreationRequest invalidReq = generateStoryCreationRequest();
        invalidReq.setHeader("");

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/stories")
                .header(AUTHORIZATION, STORY_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void createStoryIfNotStoryManager() throws Exception {
        //GIVEN
        final UUID imageId = addFakeImage(STORY_IMAGE);
        final StoryCreationRequest req = generateStoryCreationRequest();
        req.setImageId(imageId);

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/stories")
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getAllStories_happyPath() throws Exception {
        //GIVEN
        final StoryCreationRequest req = generateStoryCreationRequest();

        final Story story = createStory(req);
        final Story story1 = createStory(req);
        final Story story2 = createStory(req);
        final Story story3 = createStory(req);
        final Story story4 = createStory(req);

        updateStoryStatus(story4.getId(), PUBLISHED);
        updateStoryStatus(story3.getId(), PUBLISHED);
        updateStoryStatus(story2.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/stories?page={page}&size={size}", page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<Story> events = objectMapper.readValue(content, new TypeReference<List<Story>>() {});

        //THEN
        assertEquals(5, events.size());
    }

    @Test
    public void getAllStoriesIfNotAdmin() throws Exception {
        //GIVEN
        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(
                get(BASE_URL + "/stories?page={page}&size={size}", page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getAllPublishedStories_happyPath() throws Exception {
        //GIVEN
        final StoryCreationRequest req = generateStoryCreationRequest();

        final Story story = createStory(req);
        final Story story1 = createStory(req);
        final Story story2 = createStory(req);
        final Story story3 = createStory(req);
        final Story story4 = createStory(req);

        updateStoryStatus(story4.getId(), PUBLISHED);
        updateStoryStatus(story3.getId(), PUBLISHED);
        updateStoryStatus(story2.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/stories/published?page={page}&size={size}", page, size))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<Story> stories = objectMapper.readValue(content, new TypeReference<List<Story>>() {});

        //THEN
        assertEquals(3, stories.size());
    }

    @Test
    public void getAllStoriesByStatus_happyPath() throws Exception {
        //GIVEN
        final StoryCreationRequest req = generateStoryCreationRequest();

        final Story story = createStory(req);
        final Story story1 = createStory(req);
        final Story story2 = createStory(req);
        final Story story3 = createStory(req);
        final Story story4 = createStory(req);

        updateStoryStatus(story4.getId(), PUBLISHED);
        updateStoryStatus(story3.getId(), PUBLISHED);
        updateStoryStatus(story2.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/stories/status?status={status}&page={page}&size={size}", NEW, page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<Story> stories = objectMapper.readValue(content, new TypeReference<List<Story>>() {});

        //THEN
        assertEquals(2, stories.size());
    }

    @Test
    public void getAllStoriesByStatusIfNotAdmin() throws Exception {
        //GIVEN
        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL +
                "/stories/status?status={status}&page={page}&size={size}", NEW, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateStory_happyPath() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());
        final StoryUpdateRequest req = generateStoryUpdateRequest(saved.getId());

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/stories")
                .header(AUTHORIZATION, STORY_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Story updated = objectMapper.readValue(content, Story.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updated.getId()).isEqualTo(saved.getId());
            softly.assertThat(updated.getStatus()).isEqualTo(saved.getStatus());

            softly.assertThat(updated.getHeader()).isNotEqualTo(saved.getHeader());
            softly.assertThat(updated.getVideoUrl()).isNotEqualTo(saved.getVideoUrl());
            softly.assertThat(updated.getAuthorName()).isNotEqualTo(saved.getAuthorName());
            softly.assertThat(updated.getDescription()).isNotEqualTo(saved.getDescription());
        });
    }

    @Test
    public void updateStoryIfNotStoryManager() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());
        final StoryUpdateRequest req = generateStoryUpdateRequest(saved.getId());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/stories")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateStoryIfNotValid() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());
        final StoryUpdateRequest req = generateStoryUpdateRequest(saved.getId());
        req.setVideoUrl("");

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/stories")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, STORY_MANAGER_TOKEN)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void updateStoryStatus_happyPath() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/stories/{id}/status", saved.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, STORY_MANAGER_TOKEN)
                .content(objectMapper.writeValueAsString(PUBLISHED)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        Story updated = objectMapper.readValue(content, Story.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updated.getId()).isEqualTo(saved.getId());
            softly.assertThat(updated.getAuthorName()).isEqualTo(saved.getAuthorName());
            softly.assertThat(updated.getDescription()).isEqualTo(saved.getDescription());
            softly.assertThat(updated.getHeader()).isEqualTo(saved.getHeader());
            softly.assertThat(updated.getVideoUrl()).isEqualTo(saved.getVideoUrl());

            softly.assertThat(updated.getStatus()).isNotEqualTo(saved.getStatus());
            softly.assertThat(updated.getStatus()).isEqualTo(PUBLISHED);
        });
    }

    @Test
    public void updateStoryStatusIfNotStoryManager() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/stories/{id}/status", saved.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(PUBLISHED)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void deleteStory_happyPath() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/stories/{id}", saved.getId())
                .header(AUTHORIZATION, STORY_MANAGER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNoContent());
    }

    @Test
    public void deleteStoryIfNotStoryManager() throws Exception {
        //GIVEN
        final Story saved = createStory(generateStoryCreationRequest());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/stories/{id}", saved.getId())
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

}
