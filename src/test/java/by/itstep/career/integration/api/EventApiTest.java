package by.itstep.career.integration.api;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventPreview;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.EVENT_IMAGE;
import static by.itstep.career.model.event.enums.EventStatus.*;
import static by.itstep.career.utils.TestUtils.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class EventApiTest extends AbstractIntegrationTest {

    @Test
    public void getEventById_happyPath() throws Exception {
        //GIVEN
        final Event event = createEvent(generateEventCreationRequest());

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/events/{id}", event.getId()))
                            .andExpect(status().isOk())
                            .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Event foundEvent = objectMapper.readValue(content, Event.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundEvent.getId()).isNotNull();
            softly.assertThat(foundEvent.getStatus()).isEqualTo(DEVELOPING);
            softly.assertThat(foundEvent.getAddress()).isEqualTo(event.getAddress());
            softly.assertThat(foundEvent.getName()).isEqualTo(event.getName());
            softly.assertThat(foundEvent.getTime()).isEqualTo(event.getTime());
        });
    }

    @Test
    public void getEventByIdIfNotExists() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/events/{id}", id)
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void createEvent_happyPath() throws Exception {
        //GIVEN
        final UUID imageId = addFakeImage(EVENT_IMAGE);
        final EventCreationRequest req = generateEventCreationRequest();
        req.setImageId(imageId);

        //WHEN
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/events")
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Event event = objectMapper.readValue(content, Event.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(event.getId()).isNotNull();
            softly.assertThat(event.getStatus()).isEqualTo(DEVELOPING);
            softly.assertThat(event.getTime().getMinute()).isEqualTo(req.getTime().getMinute());
            softly.assertThat(event.getName()).isEqualTo(req.getName());
            softly.assertThat(event.getDescription()).isEqualTo(req.getDescription());
        });
    }

    @Test
    public void createEventIfNotValid() throws Exception {
        //GIVEN
        final EventCreationRequest invalidReq = generateEventCreationRequest();
        invalidReq.setName("");

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/events")
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void createEventIfNotEventManager() throws Exception {
        //GIVEN
        final UUID imageId = addFakeImage(EVENT_IMAGE);
        final EventCreationRequest req = generateEventCreationRequest();
        req.setImageId(imageId);

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/events")
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getAllEvents_happyPath() throws Exception {
        //GIVEN
        final EventCreationRequest req = generateEventCreationRequest();

        final Event event = createEvent(req);
        final Event event1 = createEvent(req);
        final Event event2 = createEvent(req);
        final Event event3 = createEvent(req);
        final Event event4 = createEvent(req);

        updateEventStatus(event4.getId(), FINISHED);
        updateEventStatus(event3.getId(), DEVELOPING);
        updateEventStatus(event2.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/events?page={page}&size={size}", page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<EventPreview> events = objectMapper.readValue(content, new TypeReference<List<EventPreview>>() {});

        //THEN
        assertEquals(5, events.size());
    }

    @Test
    public void getAllEventsIfNotAdmin() throws Exception {
        //GIVEN
        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/events?page={page}&size={size}", page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void getAllPublishedEvents_happyPath() throws Exception {
        //GIVEN
        final EventCreationRequest req = generateEventCreationRequest();

        Event event = createEvent(req);
        Event event1 = createEvent(req);
        Event event2 = createEvent(req);
        Event event3 = createEvent(req);
        Event event4 = createEvent(req);

        updateEventStatus(event4.getId(), FINISHED);
        updateEventStatus(event3.getId(), DEVELOPING);
        updateEventStatus(event2.getId(), PUBLISHED);
        updateEventStatus(event1.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/events/published?page={page}&size={size}", page, size))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<EventPreview> events = objectMapper.readValue(content, new TypeReference<List<EventPreview>>() {});

        //THEN
        assertEquals(2, events.size());
    }

    @Test
    public void getAllEventsByStatus_happyPath() throws Exception {
        //GIVEN
        final EventCreationRequest req = generateEventCreationRequest();

        Event event = createEvent(req);
        Event event1 = createEvent(req);
        Event event2 = createEvent(req);
        Event event3 = createEvent(req);
        Event event4 = createEvent(req);

        updateEventStatus(event4.getId(), FINISHED);
        updateEventStatus(event3.getId(), DEVELOPING);
        updateEventStatus(event2.getId(), PUBLISHED);
        updateEventStatus(event1.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL +
                "/events/status?status={status}&page={page}&size={size}", DEVELOPING, page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<EventPreview> events = objectMapper.readValue(content, new TypeReference<List<EventPreview>>() {});

        //THEN
        assertEquals(2, events.size());
    }

    @Test
    public void getAllEventsByStatusIfNotAdmin() throws Exception {
        //GIVEN
        final EventCreationRequest req = generateEventCreationRequest();

        Event event = createEvent(req);
        Event event1 = createEvent(req);
        Event event2 = createEvent(req);
        Event event3 = createEvent(req);
        Event event4 = createEvent(req);

        updateEventStatus(event4.getId(), FINISHED);
        updateEventStatus(event3.getId(), DEVELOPING);
        updateEventStatus(event2.getId(), PUBLISHED);
        updateEventStatus(event1.getId(), PUBLISHED);

        int page = 0;
        int size = 10;

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL +
                "/events/status?status={status}&page={page}&size={size}", DEVELOPING, page, size));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateEvent_happyPath() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());
        final EventUpdateRequest req = generateEventUpdateRequest(saved.getId());

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/events")
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN)
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final Event updated = objectMapper.readValue(content, Event.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updated.getId()).isEqualTo(saved.getId());
            softly.assertThat(updated.getStatus()).isEqualTo(saved.getStatus());

            softly.assertThat(updated.getName()).isNotEqualTo(saved.getName());
            softly.assertThat(updated.getDescription()).isNotEqualTo(saved.getDescription());
        });
    }

    @Test
    public void updateEventIfNotEventManager() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());

        final EventUpdateRequest req = generateEventUpdateRequest(saved.getId());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/events")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateEventIfNotValid() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());

        EventUpdateRequest req = generateEventUpdateRequest(saved.getId());
        req.setName("");

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/events")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void updateEventStatus_happyPath() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/events/{id}/status", saved.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN)
                .content(objectMapper.writeValueAsString(PUBLISHED)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        Event updated = objectMapper.readValue(content, Event.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updated.getId()).isEqualTo(saved.getId());
            softly.assertThat(updated.getName()).isEqualTo(saved.getName());
            softly.assertThat(updated.getDescription()).isEqualTo(saved.getDescription());
            softly.assertThat(updated.getTime()).isEqualTo(saved.getTime());

            softly.assertThat(updated.getStatus()).isNotEqualTo(saved.getStatus());
            softly.assertThat(updated.getStatus()).isEqualTo(PUBLISHED);
        });
    }

    @Test
    public void updateEventStatusIfNotEventManager() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/events/{id}/status", saved.getId())
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(PUBLISHED)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void deleteEvent_happyPath() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/events/{id}", saved.getId())
                .header(AUTHORIZATION, EVENT_MANAGER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNoContent());
    }

    @Test
    public void deleteEventIfNotEventManager() throws Exception {
        //GIVEN
        final Event saved = createEvent(generateEventCreationRequest());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/events/{id}", saved.getId())
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

}
