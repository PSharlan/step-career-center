package by.itstep.career.integration.api;

import by.itstep.career.integration.AbstractIntegrationTest;
import by.itstep.career.model.user.request.ChangePasswordRequest;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.request.UserCreationRequest;
import by.itstep.career.model.user.request.UserUpdateRequest;
import by.itstep.career.model.user.enums.Role;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static by.itstep.career.model.user.enums.Role.ROLE_ADMIN;
import static by.itstep.career.model.user.enums.Role.ROLE_SUPERUSER;
import static by.itstep.career.utils.TestUtils.*;
import static org.assertj.core.api.SoftAssertions.assertSoftly;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserApiTest extends AbstractIntegrationTest {

    @Test
    public void getUserById_happyPath() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/users/{id}", user.getId())
                            .header(AUTHORIZATION, ADMIN_TOKEN))
                            .andExpect(status().isOk())
                            .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final User foundUser = objectMapper.readValue(content, User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundUser.getId()).isNotNull();
            softly.assertThat(foundUser.getRoles().size()).isEqualTo(1);
            softly.assertThat(foundUser.getRoles().get(0)).isEqualTo(ROLE_ADMIN);
            softly.assertThat(foundUser.getEmail()).isEqualTo(user.getEmail());
            softly.assertThat(foundUser.getFirstName()).isEqualTo(user.getFirstName());
        });
    }

    @Test
    public void getUserByIdIfNotExists() throws Exception {
        //GIVEN
        final UUID id = UUID.randomUUID();

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/users/{id}", id)
                .header(AUTHORIZATION, SUPER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void getUserByEmail_happyPath() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/users/email?email={email}", user.getEmail())
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final User foundUser = objectMapper.readValue(content, new TypeReference<User>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundUser.getId()).isNotNull();
            softly.assertThat(foundUser.getRoles().size()).isEqualTo(1);
            softly.assertThat(foundUser.getRoles().get(0)).isEqualTo(ROLE_ADMIN);
            softly.assertThat(foundUser.getEmail()).isEqualTo(user.getEmail());
            softly.assertThat(foundUser.getFirstName()).isEqualTo(user.getFirstName());
        });
    }

    @Test
    public void getUserByEmailIfNotExists() throws Exception {
        //GIVEN
        final String email = "notExisting@email.com";

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/users/email?email={email}", email)
                .header(AUTHORIZATION, SUPER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void getUserByUsername_happyPath() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/users/username?username={username}", user.getUsername())
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        final User foundUser = objectMapper.readValue(content, new TypeReference<User>() {});

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(foundUser.getId()).isNotNull();
            softly.assertThat(foundUser.getRoles().size()).isEqualTo(1);
            softly.assertThat(foundUser.getRoles().get(0)).isEqualTo(ROLE_ADMIN);
            softly.assertThat(foundUser.getEmail()).isEqualTo(user.getEmail());
            softly.assertThat(foundUser.getFirstName()).isEqualTo(user.getFirstName());
        });
    }

    @Test
    public void getUserByUsernameIfNotExists() throws Exception {
        //GIVEN
        final String username = "notExistingUsername";

        //WHEN
        final ResultActions resultAction = mockMvc.perform(get(BASE_URL + "/users/username?username={username}", username)
                .header(AUTHORIZATION, SUPER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void saveUser_happyPath() throws Exception {
        //GIVEN
        UserCreationRequest req = generateUserCreationRequest();

        //WHEN
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        User savedUser = objectMapper.readValue(content, User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(savedUser.getRoles().size()).isEqualTo(1);
            softly.assertThat(savedUser.getRoles().get(0)).isEqualTo(ROLE_ADMIN);
        });
    }

    @Test
    public void saveUserIfNotSuperuser() throws Exception {
        //GIVEN
        UserCreationRequest req = generateUserCreationRequest();

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(req)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void saveUserIfNotValid() throws Exception {
        //GIVEN
        UserCreationRequest invalidReq = generateUserCreationRequest();
        invalidReq.setEmail("invalid.email");

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void saveUserIfEmailIsTaken() throws Exception {
        //GIVEN
        UserCreationRequest userToSave = generateUserCreationRequest();
        createUser(userToSave);
        String takenEmail = userToSave.getEmail();

        UserCreationRequest invalidReq = generateUserCreationRequest();
        invalidReq.setEmail(takenEmail);

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void saveListOfUsersIfUsernameIsTaken() throws Exception {
        //GIVEN
        UserCreationRequest userToSave = generateUserCreationRequest();
        createUser(userToSave);
        String takenUsername = userToSave.getUsername();

        UserCreationRequest invalidReq = generateUserCreationRequest();
        invalidReq.setUsername(takenUsername);

        //WHEN
        ResultActions resultAction = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(invalidReq)));

        //THEN
        resultAction.andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void getAllUsers_happyPath() throws Exception {
        //GIVEN
        createUser(generateUserCreationRequest());
        createUser(generateUserCreationRequest());
        createUser(generateUserCreationRequest());

        User savedUser = createUser(generateUserCreationRequest());
        deleteUser(savedUser.getId());

        int page = 0;
        int size = 10;

        //WHEN
        final MvcResult mvcResult = mockMvc.perform(get(BASE_URL + "/users?page={page}&size={size}", page, size)
                .header(AUTHORIZATION, ADMIN_TOKEN))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        List<User> users = objectMapper.readValue(content, new TypeReference<List<User>>() {});

        //THEN
        assertEquals(3, users.size());
    }

    @Test
    public void deleteUser_happyPath() throws Exception {
        //GIVEN
        UserCreationRequest userCreationReq = generateUserCreationRequest();
        User savedUser = createUser(userCreationReq);

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/users/{id}", savedUser.getId())
                .header(AUTHORIZATION, SUPER_TOKEN));

        //THEN
        resultAction.andExpect(status().isNoContent());
    }

    @Test
    public void deleteUserIfNotSuperuser() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        //WHEN
        final ResultActions resultAction = mockMvc.perform(delete(BASE_URL + "/users/{id}", user.getId())
                .header(AUTHORIZATION, ADMIN_TOKEN));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void changePassword_happyPath() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        final String oldPassword = req.getPassword();
        final String newPassword = "1234asdf";

        ChangePasswordRequest changePswReq = generateChangePasswordRequest(user.getEmail(), oldPassword, newPassword);

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/users/current/password")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(changePswReq)))
                .andExpect(status().isOk())
                .andReturn();
        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        User updatedUser = objectMapper.readValue(content, User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedUser.getId()).isNotNull();
            softly.assertThat(updatedUser.getRoles().size()).isGreaterThan(0);
            softly.assertThat(updatedUser.getEmail()).isEqualTo(changePswReq.getEmail());
        });
    }

    @Test
    public void changePasswordIfEmailNotExists() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        createUser(req);

        final String oldPassword = req.getPassword();
        final String newPassword = "1234asdf";

        ChangePasswordRequest changePswReq = generateChangePasswordRequest("random@email.com", oldPassword, newPassword);

        //WHEN
        ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/users/current/password")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(changePswReq)));

        //THEN
        resultAction.andExpect(status().isNotFound());
    }

    @Test
    public void changePasswordIfWrongOldPassword() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        final String oldPassword = "random1234";
        final String newPassword = "1234asdf";

        ChangePasswordRequest changePswReq = generateChangePasswordRequest(user.getEmail(), oldPassword, newPassword);

        //WHEN
        ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/users/current/password")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(changePswReq)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void changePasswordIfNewPasswordIsNotValid() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        final String oldPassword = req.getPassword();
        final String newPassword = "short";

        ChangePasswordRequest changePswReq = generateChangePasswordRequest(user.getEmail(), oldPassword, newPassword);

        //WHEN
        ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/users/current/password")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(changePswReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

    @Test
    public void changeRoles_happyPath() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        UserCreationRequest userCreationReq = generateUserCreationRequest();
        User savedUser = createUser(userCreationReq);

        List<Role> newRoles = Arrays.asList(ROLE_ADMIN, ROLE_SUPERUSER);

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/users/" + savedUser.getId() + "/roles")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(newRoles)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        User updatedUser = objectMapper.readValue(content, User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedUser.getId()).isEqualTo(savedUser.getId());
            softly.assertThat(updatedUser.getRoles().size()).isEqualTo(2);

            softly.assertThat(updatedUser.getRoles().contains(ROLE_ADMIN));
            softly.assertThat(updatedUser.getRoles().contains(ROLE_SUPERUSER));

            softly.assertThat(updatedUser.getEmail()).isEqualTo(savedUser.getEmail());
            softly.assertThat(updatedUser.getFirstName()).isEqualTo(savedUser.getFirstName());
        });
    }

    @Test
    public void changeRolesIfNotSuperuser() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        UserCreationRequest userCreationReq = generateUserCreationRequest();
        User savedUser = createUser(userCreationReq);

        List<Role> newRoles = Arrays.asList(Role.values());

        //WHEN
        ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/users/" + savedUser.getId() + "/roles")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, ADMIN_TOKEN)
                .content(objectMapper.writeValueAsString(newRoles)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateAnotherUserIfSuperuser_happyPath() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        UserUpdateRequest userUpdateReq = generateUserUpdateRequest(user.getId());

        //WHEN
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(userUpdateReq)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] content = mvcResult.getResponse().getContentAsByteArray();
        User updatedUser = objectMapper.readValue(content, User.class);

        //THEN
        assertSoftly(softly -> {
            softly.assertThat(updatedUser.getId()).isEqualTo(user.getId());

            softly.assertThat(updatedUser.getRoles()).isNotNull();
            softly.assertThat(updatedUser.getRoles()).isEqualTo(user.getRoles());

            softly.assertThat(updatedUser.getEmail()).isEqualTo(user.getEmail());
            softly.assertThat(updatedUser.getFirstName()).isNotEqualTo(user.getFirstName());
            softly.assertThat(updatedUser.getLastName()).isNotEqualTo(user.getLastName());
        });
    }

    @Test
    public void updateAnotherUserIfNotSuperuser() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);
        final String token = createToken(user);

        final UserCreationRequest userCreationReq = generateUserCreationRequest();
        final User savedUser = createUser(userCreationReq);

        final UserUpdateRequest userUpdateReq = generateUserUpdateRequest(savedUser.getId());

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, token)
                .content(objectMapper.writeValueAsString(userUpdateReq)));

        //THEN
        resultAction.andExpect(status().isForbidden());
    }

    @Test
    public void updateUserIfNotValid() throws Exception {
        //GIVEN
        final UserCreationRequest req = generateUserCreationRequest();
        final User user = createUser(req);

        final UserCreationRequest userCreationReq = generateUserCreationRequest();
        final User savedUser = createUser(userCreationReq);

        final UserUpdateRequest userUpdateReq = generateUserUpdateRequest(savedUser.getId());
        userUpdateReq.setFirstName("");

        //WHEN
        final ResultActions resultAction = mockMvc.perform(put(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(userUpdateReq)));

        //THEN
        resultAction.andExpect(status().isBadRequest());
    }

}
