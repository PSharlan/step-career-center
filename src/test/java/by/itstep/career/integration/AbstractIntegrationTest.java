package by.itstep.career.integration;

import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.entity.image.enums.ImageTypeEntity;
import by.itstep.career.integration.initializer.MaildevInitializer;
import by.itstep.career.integration.initializer.MinioStorageInitializer;
import by.itstep.career.integration.initializer.PostgresInitializer;
import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.consultation.enums.ConsultationStatus;
import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventEntry;
import by.itstep.career.model.event.enums.EventStatus;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.story.enums.StoryStatus;
import by.itstep.career.model.story.request.StoryCreationRequest;
import by.itstep.career.model.story.request.StoryUpdateRequest;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.request.UserCreationRequest;
import by.itstep.career.model.user.enums.Role;
import by.itstep.career.model.vacancy.Vacancy;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.repository.*;
import by.itstep.career.service.ImageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.EVENT_IMAGE;
import static by.itstep.career.entity.image.enums.ImageTypeEntity.STORY_IMAGE;
import static by.itstep.career.utils.TestUtils.SUPER_TOKEN;
import static by.itstep.career.utils.TestUtils.generateUserCreationRequest;
import static java.time.LocalDateTime.now;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = {
        PostgresInitializer.class,
        MaildevInitializer.class,
        MinioStorageInitializer.class
})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

    protected static final String BASE_URL = "/api/v1";
    protected static final String IMAGE_PATH = "classpath:./";
    protected static final String IMAGE_NAME = "example_image.png";

    @Autowired
    private ConsultationEntryRepository consultationEntryRepository;

    @Autowired
    private EventEntryRepository eventEntryRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    protected ImageService imageService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VacancyRepository vacancyRepository;

    @Autowired
    private StoryRepository storyRepository;

    @Autowired
    protected MockMvc mockMvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @Autowired
    protected ResourceLoader resourceLoader;

    @Before
    public void prepareDatabase() {
        consultationEntryRepository.deleteAll();
        eventEntryRepository.deleteAll();
        eventRepository.deleteAll();
        userRepository.deleteAll();
        vacancyRepository.deleteAll();
        imageRepository.deleteAll();
        storyRepository.deleteAll();
    }

    //IMAGE
    protected MockMultipartFile getMockMultiPartFile() throws IOException {
        InputStream inputStream = resourceLoader.getResource(IMAGE_PATH + IMAGE_NAME).getInputStream();
        return new MockMultipartFile("image", IMAGE_NAME, "multipart/form-data",
                inputStream);
    }

    //USER
    protected User createUser(UserCreationRequest req) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);
    }

    protected User updateUserRoles(UUID id, List<Role> roles) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/users/" + id + "/roles")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(roles)))
                .andExpect(status().isOk())
                .andReturn();
        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);
    }

    protected void deleteUser(UUID id) throws Exception {
        mockMvc.perform(delete(BASE_URL + "/users/" + id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    protected void createRecipient() throws Exception {
        UserCreationRequest req = generateUserCreationRequest();
        req.setEmail("pasha.sharlan@gmail.com");

        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/users")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        User user = objectMapper
                .readValue(mvcResult.getResponse().getContentAsByteArray(), User.class);

        List<Role> newRoles = Arrays.asList(Role.values());

        mockMvc.perform(put(BASE_URL + "/users/" + user.getId() + "/roles")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(newRoles)))
                .andExpect(status().isOk())
                .andReturn();
    }

    //CONSULTATION
    protected ConsultationEntry createConsultationEntry(ConsultationEntryCreationRequest req) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/consultations/entries")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), ConsultationEntry.class);
    }

    protected ConsultationEntry updateConsultationEntryStatus(UUID id, ConsultationStatus status) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/consultations/entries/{id}/status", id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(status)))
                .andExpect(status().isOk())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), ConsultationEntry.class);
    }

    //EVENT
    protected Event createEvent(EventCreationRequest req) throws Exception {
        UUID imageId = addFakeImage(EVENT_IMAGE);
        req.setImageId(imageId);

        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/events")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Event.class);
    }

    protected Event updateEvent(EventUpdateRequest req) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/events")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Event.class);
    }

    protected Event updateEventStatus(UUID id, EventStatus status) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/events/{id}/status", id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(status)))
                .andExpect(status().isOk())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Event.class);
    }

    protected UUID addFakeImage(ImageTypeEntity type){
        final ImageEntity image = new ImageEntity();
        image.setOriginal("link-to-original");
        image.setMain("link-to-main");
        image.setPreview("link-to-preview");
        image.setImageType(type);
        image.setCreatedAt(now());

        final ImageEntity saved = imageRepository.save(image);
        return saved.getId();
    }

    //EVENT ENTRY
    protected EventEntry createEventEntry(EventEntryCreationRequest req) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/events/entries")
                .contentType(APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), EventEntry.class);
    }

    //VACANCY
    protected Vacancy createVacancy(VacancyCreationRequest req) throws Exception {
        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/vacancies")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Vacancy.class);
    }

    protected void deleteVacancy(UUID id) throws Exception {
        mockMvc.perform(delete(BASE_URL + "/vacancies/" + id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN))
                .andExpect(status().isNoContent())
                .andReturn();
    }

    //STORY
    protected Story createStory(StoryCreationRequest req) throws Exception {
        UUID imageId = addFakeImage(STORY_IMAGE);
        req.setImageId(imageId);

        MvcResult mvcResult = mockMvc.perform(post(BASE_URL + "/stories")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Story.class);
    }

    protected Story updateStory(StoryUpdateRequest req) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/stories")
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(req)))
                .andExpect(status().isCreated())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Story.class);
    }

    protected Story updateStoryStatus(UUID id, StoryStatus status) throws Exception {
        MvcResult mvcResult = mockMvc.perform(put(BASE_URL + "/stories/{id}/status", id)
                .contentType(APPLICATION_JSON)
                .header(AUTHORIZATION, SUPER_TOKEN)
                .content(objectMapper.writeValueAsString(status)))
                .andExpect(status().isOk())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsByteArray(), Story.class);
    }

}
