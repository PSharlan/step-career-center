package by.itstep.career.integration.initializer;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class MinioStorageInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final int PORT = 9000;
    private static final String ACCESS_KEY = "careerCenterMinIOAccessKey";
    private static final String SECRET_KEY = "careerCenterMinIOSecretKey";
    private static final String BUCKET_NAME = "itstep-career-center";

    private static final GenericContainer MINIO_STORAGE =
            new GenericContainer("minio/minio:latest")
                    .withEnv("MINIO_ACCESS_KEY", ACCESS_KEY)
                    .withEnv("MINIO_SECRET_KEY", SECRET_KEY)
                    .withCommand("server /var/lib/minio/storage")
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(PORT);

    static {
        MINIO_STORAGE.start();
    }

    private String getEndpointUrl() {
        return getStorageUrl() + BUCKET_NAME + "/";
    }

    private String getStorageUrl() {
        return String.format("http://%s:%s/", getHost(), getPort());
    }

    private String getHost() {
        return MINIO_STORAGE.getContainerIpAddress();
    }

    private int getPort() {
        return MINIO_STORAGE.getMappedPort(PORT);
    }

    @Override
    public void initialize(final ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

    private void applyProperties(final ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "amazonProperties.storageUrl:" + getStorageUrl(),
                "amazonProperties.accessKey:" + ACCESS_KEY,
                "amazonProperties.secretKey:" + SECRET_KEY,
                "amazonProperties.bucketName:" + BUCKET_NAME,
                "amazonProperties.endpointUrl:" + getEndpointUrl()
        ).applyTo(applicationContext);
    }

}
