package by.itstep.career.integration.initializer;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;

public class MaildevInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final int WEB_PORT = 80;
    private static final int SMTP_PORT = 25;

    private static final GenericContainer MAILDEV =
            new GenericContainer("djfarrelly/maildev")
                    .waitingFor(Wait.forListeningPort())
                    .withExposedPorts(WEB_PORT, SMTP_PORT);

    static {
        MAILDEV.start();
    }

    private static String getHost() {
        return MAILDEV.getContainerIpAddress();
    }

    private static int getSmtpPort() {
        return MAILDEV.getMappedPort(SMTP_PORT);
    }

    @Override
    public void initialize(final ConfigurableApplicationContext applicationContext) {
        applyProperties(applicationContext);
    }

    private void applyProperties(final ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "spring.mail.host:" + getHost(),
                "spring.mail.port:" + getSmtpPort(),
                "spring.mail.properties.mail.smtp.auth:" + false,
                "spring.mail.properties.mail.smtp.starttls.enable:" + false
        ).applyTo(applicationContext);
    }

}
