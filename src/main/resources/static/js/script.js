'use strict';

//let serverApiUrl = "http://localhost:8081/api/v1";
let serverApiUrl = "/api/v1";

let burgerMenu = document.getElementById('burger');
let dropMenu = document.getElementById('myMenu');
let phoneIcon = document.getElementById('phoneIcon');
let phoneMenu = document.getElementById('phoneMenu');
let assignedAtDaySelect = document.querySelector('#inputStateDay');

burgerMenu.onclick = function () {
    this.classList.toggle('burger-menu-active');
    dropMenu.classList.toggle(('menu-active'));
};

phoneIcon.onclick = function () {
    phoneMenu.classList.toggle(('menu-active'));
};

assignedAtDaySelect.onchange = function(e){
    var req = new XMLHttpRequest();
    req.overrideMimeType("application/json");
    let url = serverApiUrl + "/consultations/entries/time/available?day=" + e.target.value;
    req.open('GET', url, true);
    req.send();
    req.onload  = function() {

        var jsonResponse = JSON.parse(req.responseText) + '';
        var slots = jsonResponse.split(",");

        var selectTime = document.getElementById('inputStateTime');

        while (selectTime.options.length > 0) {
            selectTime.remove(0);
        }

        var opt;

        if(slots.length === 1 && slots[0] === "") {
            opt = document.createElement('option');
            opt.value = "Нет доступного времени";
            opt.innerHTML = "Нет доступного времени";
            selectTime.appendChild(opt);
        } else {
            for (var i = 0; i < slots.length; i++){
                let timePrefix = "";
                if(slots[i] < 10) timePrefix = "0";

                var time = timePrefix + slots[i] + ":00";
                opt = document.createElement('option');
                opt.value = time;
                opt.innerHTML = time;
                selectTime.appendChild(opt);
            }
        }
    };
};

function openStories(id) {
    document.querySelector('#story-video-frame').src = "";
    document.getElementById('storiesTitle').textContent = "";

    var req = new XMLHttpRequest();
    req.overrideMimeType("application/json");
    let url = serverApiUrl + "/stories/" + id;
    req.open('GET', url, true);
    req.send();
    req.onload  = function() {
        var jsonResponse = JSON.parse(req.responseText);
        var videoUrl = jsonResponse.videoEmbedUrl;
        var storyHeader = jsonResponse.header;

        var frame = document.querySelector('#story-video-frame');
        frame.src = videoUrl;

        var title = document.getElementById('storiesTitle');
        title.textContent = storyHeader;
    };
    $('#modalStories').modal('toggle');
}

function openVacancyModal(id) {
    let modalBody = document.getElementById('modalBodyVacancy');

    var req = new XMLHttpRequest();
    req.overrideMimeType("application/json");
    let url = serverApiUrl + "/vacancies/" + id;
    console.log('URL: ' + url);
    req.open('GET', url, true);
    req.send();
    req.onload  = function() {
        var jsonResponse = JSON.parse(req.responseText);
        console.log('RESPONSE: ' + jsonResponse);

        let companyText = jsonResponse.company;
        let positionText = jsonResponse.name;
        let experienceText = jsonResponse.experience;
        let descriptionText = jsonResponse.description;
        console.log('RESPONSE COMPANY: ' + companyText);
        console.log('RESPONSE POSITION: ' + positionText);
        console.log('RESPONSE EXP: ' + experienceText);
        console.log('RESPONSE DESC: ' + descriptionText);

        modalBody.getElementsByClassName('company')[0].innerHTML = companyText;
        modalBody.getElementsByClassName('position')[0].innerHTML = positionText;
        modalBody.getElementsByClassName('experience')[0].innerHTML = experienceText;
        modalBody.getElementsByClassName('description')[0].innerHTML = descriptionText;
    };

    $('#modalVacancy').modal('toggle');
}



function formatDateString(date) { // MM/dd/yyyy -> yyyy-MM-dd
    let newDate = '';
    if(date !== '') {
        let values = date.split("/");
        newDate = values[2] + '-' + values[0] + "-" + values[1];
    }
    return newDate;
}

//Валидаяция форм
window.onload = function () {

    //build options for picking consultations day and time
    var dayNames = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
    var selectDay = document.getElementById('inputStateDay');
    var day = new Date();
    for (var i = 1; i<=7; i++){
        if(day.getDay() !== 6 && day.getDay() !== 0) {
            let monthPrefix = "";
            if(day.getMonth() < 9) monthPrefix = "0";
            let dayPrefix = "";
            if(day.getDate() < 9) dayPrefix = "0";

            var dayString = day.getFullYear()  + "-" + monthPrefix + (day.getMonth()+1) + "-" + dayPrefix + day.getDate();
            var opt = document.createElement('option');
            opt.value = dayString;
            opt.innerHTML = day.toLocaleDateString('en-GB') + "  (" + dayNames[day.getDay()] + ")";
            selectDay.appendChild(opt);
        }
        day.setDate(day.getDate() + 1);
    }

    //Модаль Консультации!---------------------------------------------------------------------------------------------
    let studentStep = document.getElementById('studentStep');
    let numberGroup = document.getElementById('numberGroup');
    studentStep.onclick = showGroup;
    function showGroup() {
        numberGroup.classList.toggle('display_none');
    }

    //Div с ошибкой под формой
    let divError = document.createElement('div');
    divError.className = 'invalid-feedback';
    divError.innerHTML = 'Поле не заполнено';
    //Все обязательные поля для заполнения
    let inputAllConsult = document.querySelectorAll('form[name="formConsult"] input[type="text"],' +
                                                             'form[name="formConsult"] input[type="email"],' +
                                                             'form[name="formConsult"] input[type="number"]');

    let submit = document.getElementById('submit');
    submit.onclick = function (e) {
        let valid = true;
        for (let i = 0; i<inputAllConsult.length; i++) {
            if (inputAllConsult[i].value === '' && inputAllConsult[i].className!=='form-control display_none') {
                e.preventDefault();
                inputAllConsult[i].classList.add('is-invalid');
                inputAllConsult[i].onfocus = removeError;
                inputAllConsult[i].after(divError);
                valid = false;
            }
        }

        var selectedTime = document.getElementById('inputStateTime');
        var selectedDay = document.getElementById('inputStateDay');
        if(selectedDay.options[selectedDay.selectedIndex].value === "Выберите день") {
            e.preventDefault();
            selectedDay.classList.add('is-invalid');
            selectedDay.onfocus = removeError;
            selectedDay.after(divError);
            valid = false;
        } else if(selectedTime.options[0].value === "Нет доступного времени") {
            e.preventDefault();
            selectedTime.classList.add('is-invalid');
            selectedTime.onfocus = removeError;
            selectedTime.after(divError);
            valid = false;
        }

        if(valid === true) {
            let name = document.querySelector('#name').value;
            let lastName = document.querySelector('#surname').value;
            let email = document.querySelector('#email').value;
            let phone = document.querySelector('#tel').value;
            let isStudent = document.querySelector('#studentStep').checked;
            let group = document.querySelector('#numberGroup').value;
            let comment = document.querySelector('#exampleFormControlTextarea1').value;
            let assignedAtDay = document.querySelector('#inputStateDay').value;
            let assignedAtTime = document.querySelector('#inputStateTime').value;

            let xhr = new XMLHttpRequest();
            let url = serverApiUrl + "/consultations/entries";
            xhr.open("POST", url, true);
            xhr.setRequestHeader("Content-Type", "application/json");

            var data = JSON.stringify({
                "firstName": name,
                "lastName": lastName,
                "email": email,
                "phone": phone,
                "student": isStudent,
                "group": group,
                "comment": comment,
                "assignedAt": assignedAtDay + " " + assignedAtTime
            });

            xhr.send(data);
        }
    };
    //Удаление Класса с ошибкой при фокусе формы
    function removeError() {
        this.classList.remove('is-invalid');
    }

    //Модаль Event!---------------------------------------------------------------------------------------------
    let submitEvent = document.getElementById('submitEvent');
    //Валидация по сабмиту
    if(submitEvent != null) {
        //Div с ошибкой под формой
        let divEventFormError = document.createElement('div');
        divEventFormError.className = 'invalid-feedback';
        divEventFormError.innerHTML = 'Поле не заполнено';

        let inputAllEvent = document.querySelectorAll('form[name="formEvent"] input[type="text"],' +
            'form[name="formEvent"] input[type="email"]');

        submitEvent.onclick = function (e) {
            let valid = true;
            for (let i = 0; i<inputAllEvent.length; i++) {
                if (inputAllEvent[i].value === '') {
                    e.preventDefault();
                    inputAllEvent[i].classList.add('is-invalid');
                    inputAllEvent[i].onfocus = removeError;
                    inputAllEvent[i].after(divEventFormError);
                    valid = false;
                }
            }

            if(valid === true) {
                let name = document.querySelector('#nameEvent').value;
                let lastName = document.querySelector('#surnameEvent').value;
                let email = document.querySelector('#emailEvent').value;
                let jobs = document.querySelector('#jobs').value;
                let eventId = document.querySelector('#eventId').value;
                let isStudentEvent = document.querySelector('#studentEvent').checked;

                let xhr = new XMLHttpRequest();
                let url = serverApiUrl + "/events/entries";
                xhr.open("POST", url, true);
                xhr.setRequestHeader("Content-Type", "application/json");

                var data = JSON.stringify({
                    "eventId": eventId,
                    "firstName": name,
                    "lastName": lastName,
                    "email": email,
                    "company": jobs,
                    "student": isStudentEvent
                });

                xhr.send(data);
            }
            // //prevent GET request
            // e.preventDefault();
            // //hide modal
            // var eventModalForm = document.querySelector('#modalEvent');
            // eventModalForm.classList.remove('show');
            // eventModalForm.setAttribute('style', 'display: none;');
            // document.getElementsByTagName('body')[0].removeAttribute('class');
        };
    }





    //Модаль Вакансии------------------------------------------------------------------------
    // let vacancyPreviews = document.getElementsByClassName('vacancy-box');
    // for (let i = 0; i <vacancyPreviews.length; i++) {
    //     vacancyPreviews[i].addEventListener('click', openVacancyModal);
    // }
    // function openVacancyModal() {
    //     $('#modalVacancy').modal('toggle');
    //     let modalBody = document.getElementById('modalBodyVacancy');
    //
    //
    //
    //     let companyText = '';
    //     let positionText = '';
    //     let experienceText = '';
    //
    //     modalBody.getElementById('company').innerHTML = companyText;
    //     modalBody.getElementById('position').innerHTML = positionText;
    //     modalBody.getElementById('experience').innerHTML = experienceText;
    // }









    <!--Script stops video from playing when modal is closed-->
    $("#modalStories").on('hidden.bs.modal', function (e) {
        $("#modalStories iframe").attr("src", $("#modalStories iframe").attr("src"));
    });
    //Опции для сладера на странице проекта
    $('.multiple-items').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        speed:3000
    });






    //Страница Резюме!---------------------------------------------------------------------------------------------
    let submitResume = document.getElementById('submitResume');

    if (submitResume!==null) {
        submitResume.onclick = function (e) {
            var workExperienceData = [];
            var educationData = [];
            var projectsData = [];

            //main fields
            let resumeEmail = document.getElementById('emailResume');
            let resumeSkype = document.getElementById('skypeResume');
            let resumeLinkedin = document.getElementById('linkedinResume');
            let resumeName = document.getElementById('nameResume');
            let resumeSurname = document.getElementById('surnameResume');
            let resumePhone = document.getElementById('telResume');
            let resumePosition = document.getElementById('positionResume');
            let resumePersonalDetails = document.getElementById('personalDetailsResume');
            let resumeSkills = document.getElementById('skillsResume');

            let valid = true;

            //validate main info
            let inputAllResume = [resumeName, resumeSurname,resumePhone, resumeEmail, resumePosition, resumeSkills, resumePersonalDetails];
            for (let i = 0; i<inputAllResume.length; i++) {
                if (inputAllResume[i].value === '' && inputAllResume[i].className!=='form-control display_none') {
                    valid = false;
                    e.preventDefault();
                    inputAllResume[i].classList.add('is-invalid');
                    inputAllResume[i].onfocus = removeError;
                }
            }

            if (resumeEmail.value !== '') {
                let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                let emailIsValid = re.test(String(resumeEmail.value).toLowerCase());
                if (!emailIsValid) {
                    valid = false;
                    e.preventDefault();
                    resumeEmail.classList.add('is-invalid');
                    resumeEmail.onfocus = removeError;
                }
            }

            //validate all work exp wrappers
            let resumeAllWorkExperiences = document.getElementsByClassName('workExperienceWrapper');
            console.log("EXPS: " + resumeAllWorkExperiences.length);
            for(let i = 0; i < resumeAllWorkExperiences.length; i++) {
                let workExp = resumeAllWorkExperiences[i];

                let company = workExp.getElementsByClassName('companyExperience')[0];
                let position = workExp.getElementsByClassName('positionExperience')[0];
                let responsibilities = workExp.getElementsByClassName('responsibilityExperience')[0];
                let startDate = workExp.getElementsByClassName('startDataJobs')[0];
                let endDate = workExp.getElementsByClassName('endDataJobs')[0];
                let stillWorks = workExp.getElementsByClassName('stillWorks')[0];

                let elementsToCheck = [company, position,responsibilities, startDate, endDate];
                let needsToBeValidated = stillWorks.checked;
                for (let i = 0; i < elementsToCheck.length; i++) {
                    if(elementsToCheck[i].value !== '') {
                        needsToBeValidated = true;
                    }
                }

                if(needsToBeValidated) {
                    let inputAllResumeWorkExperience = [company, position,responsibilities, startDate];
                    for (let i = 0; i<inputAllResumeWorkExperience.length; i++) {
                        let element = inputAllResumeWorkExperience[i];
                        if (element.value === '' && element.className!=='form-control display_none') {
                            valid = false;
                            e.preventDefault();
                            element.classList.add('is-invalid');
                            element.onfocus = removeError;
                        }
                    }

                    if(endDate.value === '' && !stillWorks.checked)  {
                        valid = false;
                        e.preventDefault();
                        endDate.classList.add('is-invalid');
                        endDate.onfocus = removeError;
                    } else if(endDate.value !== '' && stillWorks.checked)  {
                        valid = false;
                        e.preventDefault();
                        endDate.classList.add('is-invalid');
                        endDate.onfocus = removeError;
                    }
                }
                let exp = {
                    "company": company.value,
                    "endDate": formatDateString(endDate.value),
                    "position": position.value,
                    "present": stillWorks.checked,
                    "responsibilities": responsibilities.value,
                    "startDate": formatDateString(startDate.value)
                }
                workExperienceData.push(exp);
            }

            //validate all education wrappers
            let resumeAllEducation = document.getElementsByClassName('educationWrapper');
            console.log("EDU: " + resumeAllEducation.length);
            for(let i = 0; i < resumeAllEducation.length; i++) {
                let education = resumeAllEducation[i];

                let educationPlace = education.getElementsByClassName('organizationEducation')[0];
                let specialization = education.getElementsByClassName('specializationEducation')[0];
                let startDate = education.getElementsByClassName('startDataEducation')[0];
                let endDate = education.getElementsByClassName('endDataEducation')[0];
                let stillStudying = education.getElementsByClassName('stillStudying')[0];

                let elementsToCheck = [educationPlace, specialization, startDate, endDate];
                let needsToBeValidated = stillStudying.checked;
                for (let i = 0; i < elementsToCheck.length; i++) {
                    if(elementsToCheck[i].value !== '') {
                        needsToBeValidated = true;
                    }
                }

                if(needsToBeValidated) {
                    let inputAllResumeEducation = [educationPlace, specialization, startDate];
                    for (let i = 0; i<inputAllResumeEducation.length; i++) {
                        let element = inputAllResumeEducation[i];
                        if (element.value === '' && element.className!=='form-control display_none') {
                            valid = false;
                            e.preventDefault();
                            element.classList.add('is-invalid');
                            element.onfocus = removeError;
                        }
                    }

                    if(endDate.value === '' && !stillStudying.checked)  {
                        valid = false;
                        e.preventDefault();
                        endDate.classList.add('is-invalid');
                        endDate.onfocus = removeError;
                    } else if(endDate.value !== '' && stillStudying.checked)  {
                        valid = false;
                        e.preventDefault();
                        endDate.classList.add('is-invalid');
                        endDate.onfocus = removeError;
                    }
                }

                let edu = {
                    "endDate": formatDateString(endDate.value),
                    "organization": educationPlace.value,
                    "present": stillStudying.checked,
                    "specialization": specialization.value,
                    "startDate": formatDateString(startDate.value)
                };
                educationData.push(edu);
            }

            //validate all project wrappers
            let resumeAllProjects = document.getElementsByClassName('projectWrapper');
            console.log("PROJ: " + resumeAllProjects.length);
            for(let i = 0; i < resumeAllProjects.length; i++) {
                let project = resumeAllProjects[i];

                let projectName = project.getElementsByClassName('nameProject')[0];
                let projectEnvironment = project.getElementsByClassName('environment')[0];
                let projectDescription = project.getElementsByClassName('descriptionProject')[0];
                let projectAchievements = project.getElementsByClassName('achievementProject')[0];

                let elementsToCheck = [projectName, projectEnvironment, projectDescription, projectAchievements];
                let needsToBeValidated = false;
                for (let i = 0; i < elementsToCheck.length; i++) {
                    if(elementsToCheck[i].value !== '') {
                        needsToBeValidated = true;
                    }
                }

                if(needsToBeValidated) {
                    for (let i = 0; i < elementsToCheck.length; i++) {
                        let element = elementsToCheck[i];
                        if (element.value === '' && element.className!=='form-control display_none') {
                            valid = false;
                            e.preventDefault();
                            element.classList.add('is-invalid');
                            element.onfocus = removeError;
                        }
                    }
                }

                let proj = {
                    "achievements": projectAchievements.value,
                    "description": projectDescription.value,
                    "environment": projectEnvironment.value,
                    "name": projectName.value
                };
                projectsData.push(proj);
            }

            if(valid) {
                console.log("FORM IS VALID AND CAN BE SENT...");
                let xhr = new XMLHttpRequest();
                let url = serverApiUrl + "/cvs";
                xhr.open("POST", url, true);
                xhr.setRequestHeader("Content-Type", "application/json");

                let data = JSON.stringify({
                    "email": resumeEmail.value,
                    "imageId": null,
                    "linkedin": resumeLinkedin.value,
                    "name": resumeName.value + " " + resumeSurname.value,
                    "personalDetails": resumePersonalDetails.value,
                    "phone": resumePhone.value,
                    "position": resumePosition.value,
                    "skills": resumeSkills.value,
                    "skype": resumeSkype.value,
                    "projects": projectsData,
                    "education": educationData,
                    "workExperience": workExperienceData
                });
                xhr.send(data);
            }
            e.preventDefault();
        };
    }
    let copyFormExperience = document.getElementsByClassName('workExperienceWrapper')[0];
    let addMoreJobs = document.getElementById('addMoreJobs');
    if (addMoreJobs!==undefined && addMoreJobs!==null && copyFormExperience!==null) {
        addMoreJobs.onclick = addElem;
        copyFormExperience = copyFormExperience.cloneNode(true);
    }
    let addMoreEducation = document.getElementById('addMoreEducation');
    let copyFormEducation = document.getElementsByClassName('educationWrapper')[0];
    if (addMoreEducation!==undefined && addMoreEducation!==null && copyFormEducation!==null  ) {
        addMoreEducation.onclick = addElem;
        copyFormEducation = copyFormEducation.cloneNode(true);
    }
    let addMoreProject = document.getElementById('addMoreProject');
    let copyFormProject = document.getElementsByClassName('projectWrapper')[0];
    if (addMoreProject!==undefined && addMoreProject!==null && copyFormProject!==null ) {
        addMoreProject.onclick = addElem;
        copyFormProject = copyFormProject.cloneNode(true);
    }

    let id = 0;
    function addElem (e) {
        ++id;
        let allElem;
        let newElem;
        let target = e.target;
        if (target.id === 'addMoreJobs') {
            allElem = document.getElementsByClassName('workExperienceWrapper');
            newElem = copyFormExperience.cloneNode(true);
            let input = newElem.querySelector('#presentDataJobs');
            let label = newElem.querySelector('label[for="presentDataJobs"]');
            let newId = input.id + id;
            label.setAttribute('for', `${newId}`);
            input.id = input.id + id;
        }
        else if (target.id === 'addMoreEducation') {
            allElem = document.getElementsByClassName('educationWrapper');
            newElem = copyFormEducation.cloneNode(true);
            let input = newElem.querySelector('#presentDataEducation');
            let label = newElem.querySelector('label[for="presentDataEducation"]');
            let newId = input.id + id;
            label.setAttribute('for', `${newId}`);
            input.id = input.id + id;

        }
        else if (target.id === 'addMoreProject') {
            allElem = document.getElementsByClassName('projectWrapper');
            newElem = copyFormProject.cloneNode(true);
        }

        let del = document.createElement('span');
        del.className = 'delElem';
        del.innerHTML = 'Удалить';
        del.onclick = delElem;
        let hr = document.createElement('hr');
        newElem.prepend(del);
        newElem.prepend(hr);
        allElem[allElem.length-1].after(newElem);

        initDatepicker();
    }

    // Работа imput data-календарь страница резюме
    function initDatepicker() {
        let datepicker = $('input.datepicker');
        $.each(datepicker,function(index,value){
            $(value).datepicker({
                uiLibrary: 'bootstrap4'
            });
        });
    }
    initDatepicker();

    function delElem (e) {
        let target = e.target;
        target.parentNode.remove();
    }

    //Открытие презентации
    $('#openPresentation').click(function (e) {
        $('#startPresentation').click();
    });
};
