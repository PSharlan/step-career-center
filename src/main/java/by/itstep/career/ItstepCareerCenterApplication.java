package by.itstep.career;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItstepCareerCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItstepCareerCenterApplication.class, args);
	}

}
