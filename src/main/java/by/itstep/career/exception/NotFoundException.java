package by.itstep.career.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class NotFoundException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public NotFoundException(final String message) {
        super(message);
        this.status = NOT_FOUND;
    }

}
