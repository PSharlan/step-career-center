package by.itstep.career.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class EntityAlreadyExistsException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public EntityAlreadyExistsException(final String message) {
        super(message);
        this.status = UNPROCESSABLE_ENTITY;
    }

}
