package by.itstep.career.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;

public class EntityIsDeletedException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public EntityIsDeletedException(final String message) {
        super(message);
        this.status = UNPROCESSABLE_ENTITY;
    }

}
