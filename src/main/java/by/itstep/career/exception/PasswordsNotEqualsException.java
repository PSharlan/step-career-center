package by.itstep.career.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

import static org.springframework.http.HttpStatus.FORBIDDEN;

public class PasswordsNotEqualsException extends RuntimeException {

    @Getter
    private final HttpStatus status;

    public PasswordsNotEqualsException(final String message) {
        super(message);
        this.status = FORBIDDEN;
    }

}
