package by.itstep.career.entity.event;

import by.itstep.career.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "event_entry")
@EqualsAndHashCode(callSuper = true)
public class EventEntryEntity extends BaseEntity {

    @Column(name = "company")
    private String company;

    @Column(name = "email")
    private String email;

    @ManyToOne
    @JoinColumn(name = "event_id")
    private EventEntity event;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "student")
    private Boolean student;

}
