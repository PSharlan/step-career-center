package by.itstep.career.entity.event.enums;

public enum EventStatusEntity {

    DEVELOPING,
    FINISHED,
    PUBLISHED

}
