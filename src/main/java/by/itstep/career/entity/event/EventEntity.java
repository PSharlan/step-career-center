package by.itstep.career.entity.event;

import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.event.enums.EventStatusEntity;
import by.itstep.career.entity.image.ImageEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.LAZY;

@Data
@Entity
@Table(name = "event")
@EqualsAndHashCode(callSuper = true)
public class EventEntity extends BaseEntity {

    @Column(name = "address")
    private String address;

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @ToString.Exclude
    @OneToMany(fetch = LAZY, cascade = ALL, mappedBy = "event")
    private List<EventEntryEntity> entries;

    @JoinColumn(name = "image_id")
    @OneToOne(fetch = FetchType.EAGER, cascade = ALL)
    private ImageEntity image;

    @Column(name = "name")
    private String name;

    @Enumerated(STRING)
    @Column(name = "status")
    private EventStatusEntity status;

    @Column(name = "time")
    private LocalDateTime time;

    @Column(name = "type")
    private String type;

}
