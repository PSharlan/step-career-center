package by.itstep.career.entity.image;


import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.image.enums.ImageTypeEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "image")
@EqualsAndHashCode(callSuper = true)
public class ImageEntity extends BaseEntity {

    @Column(name = "original")
    private String original;

    @Column(name = "preview")
    private String preview;

    @Column(name = "name")
    private String name;

    @Column(name = "main")
    private String main;

    @Enumerated(STRING)
    @Column(name = "image_type")
    private ImageTypeEntity imageType;

}
