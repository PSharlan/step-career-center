package by.itstep.career.entity.image.enums;

public enum ImageTypeEntity {

    CV_IMAGE,
    EVENT_IMAGE,
    USER_IMAGE,
    STORY_IMAGE,
    PROJECT_IMAGE

}
