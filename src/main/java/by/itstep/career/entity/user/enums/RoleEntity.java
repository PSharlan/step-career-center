package by.itstep.career.entity.user.enums;

public enum RoleEntity {

    ROLE_ADMIN,
    ROLE_CONSULTANT,
    ROLE_EVENT_MANAGER,
    ROLE_VACANCY_MANAGER,
    ROLE_STORY_MANAGER,
    ROLE_SUPERUSER

}
