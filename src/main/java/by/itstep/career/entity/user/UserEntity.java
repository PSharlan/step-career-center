package by.itstep.career.entity.user;

import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.user.enums.RoleEntity;
import lombok.*;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.FetchType.EAGER;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "`user`")
@EqualsAndHashCode(callSuper = true)
public class UserEntity extends BaseEntity {

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "password")
    private String password;

    @Enumerated(STRING)
    @Column(name = "role_name")
    @ElementCollection(targetClass = RoleEntity.class, fetch = EAGER)
    @CollectionTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"))
    private List<RoleEntity> roles;

    @Column(name = "username")
    private String username;

}
