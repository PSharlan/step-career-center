package by.itstep.career.entity.consultation.enums;

public enum ConsultationStatusEntity {

    NEW,
    CONFIRMED,
    PROVIDED,
    CANCELLED

}
