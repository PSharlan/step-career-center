package by.itstep.career.entity.consultation;

import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.consultation.enums.ConsultationStatusEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.time.LocalDateTime;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "consultation_entry")
@EqualsAndHashCode(callSuper = true)
public class ConsultationEntryEntity extends BaseEntity {

    @Column(name = "assigned")
    private LocalDateTime assignedAt;

    @Column(name = "comment")
    private String comment;

    @Column(name = "confirmed")
    private LocalDateTime confirmedAt;

    @Enumerated(STRING)
    @Column(name = "consultation_status")
    private ConsultationStatusEntity consultationStatus;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "`group`")
    private String group;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    private String phone;

    @Column(name = "student")
    private Boolean student;

}
