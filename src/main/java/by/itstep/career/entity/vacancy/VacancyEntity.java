package by.itstep.career.entity.vacancy;

import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.vacancy.enums.VacancyExperienceEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "vacancy")
@EqualsAndHashCode(callSuper = true)
public class VacancyEntity extends BaseEntity {

    @Column(name = "company")
    private String company;

    @Column(name = "description")
    private String description;

    @Column(name = "name")
    private String name;

    @Enumerated(STRING)
    @Column(name = "experience")
    private VacancyExperienceEntity experience;

}
