package by.itstep.career.entity.vacancy.enums;

public enum VacancyExperienceEntity {

    INTERN,
    JUNIOR,
    MIDDLE,
    SENIOR

}
