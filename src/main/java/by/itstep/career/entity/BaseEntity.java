package by.itstep.career.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@MappedSuperclass
@EqualsAndHashCode
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @CreatedDate
    @Column(name = "created")
    private LocalDateTime createdAt;

    @LastModifiedDate
    @Column(name = "updated")
    private LocalDateTime updatedAt;

    @Column(name = "deleted")
    private LocalDateTime deletedAt;

}
