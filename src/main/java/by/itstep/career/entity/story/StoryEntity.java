package by.itstep.career.entity.story;

import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.entity.story.enums.StoryStatusEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;

@Data
@Entity
@Table(name = "story")
@EqualsAndHashCode(callSuper = true)
public class StoryEntity extends BaseEntity {

    @Column(name = "author_name")
    private String authorName;

    @Column(name = "description")
    private String description;

    @Column(name = "header")
    private String header;

    @JoinColumn(name = "image_id")
    @OneToOne(fetch = FetchType.EAGER, cascade = ALL)
    private ImageEntity image;

    @Enumerated(STRING)
    @Column(name = "status")
    private StoryStatusEntity status;

    @Column(name = "video_embed_url")
    private String videoEmbedUrl;

    @Column(name = "video_url")
    private String videoUrl;

}
