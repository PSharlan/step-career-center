package by.itstep.career.entity.story.enums;

public enum StoryStatusEntity {

    NEW,
    PUBLISHED

}
