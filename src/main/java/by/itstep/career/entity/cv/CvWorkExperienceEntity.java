package by.itstep.career.entity.cv;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@Entity
@Table(name = "cv_work_experience")
public class CvWorkExperienceEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "cv_id")
    private CvEntity cv;

    @Column(name = "company")
    private String company;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "position")
    private String position;

    @Column(name = "present")
    private boolean present;

    @Column(name = "responsibilities")
    private String responsibilities;

    @Column(name = "start_date")
    private LocalDate startDate;

}
