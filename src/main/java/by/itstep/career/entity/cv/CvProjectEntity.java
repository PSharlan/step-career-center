package by.itstep.career.entity.cv;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@Entity
@Table(name = "cv_project")
public class CvProjectEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "cv_id")
    private CvEntity cv;

    @Column(name = "achievements")
    private String achievements;

    @Column(name = "description")
    private String description;

    @Column(name = "environment")
    private String environment;

    @Column(name = "name")
    private String name;

}
