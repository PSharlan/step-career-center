package by.itstep.career.entity.cv;

import by.itstep.career.entity.BaseEntity;
import by.itstep.career.entity.image.ImageEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;

@Data
@Entity
@Table(name = "cv")
@EqualsAndHashCode(callSuper = true)
public class CvEntity extends BaseEntity {

    @OneToMany(cascade = ALL, mappedBy = "cv")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<CvEducationEntity> education;

    @Column(name = "email")
    private String email;

    @Column(name = "file_url")
    private String fileUrl;

    @OneToOne(fetch = FetchType.EAGER, cascade = MERGE)
    @JoinColumn(name = "image_id")
    private ImageEntity image;

    @Column(name = "linkedin")
    private String linkedin;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "personal_details")
    private String personalDetails;

    @Column(name = "position")
    private String position;

    @OneToMany(cascade = ALL, mappedBy = "cv")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<CvProjectEntity> projects;

    @Column(name = "skills")
    private String skills;

    @Column(name = "skype")
    private String skype;

    @OneToMany(cascade = ALL, mappedBy = "cv")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<CvWorkExperienceEntity> workExperience;

}
