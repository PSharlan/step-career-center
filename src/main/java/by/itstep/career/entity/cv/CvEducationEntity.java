package by.itstep.career.entity.cv;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

import static javax.persistence.GenerationType.AUTO;

@Data
@Entity
@Table(name = "cv_education")
public class CvEducationEntity {

    @Id
    @GeneratedValue(strategy = AUTO)
    private UUID id;

    @ManyToOne
    @JsonIgnore
    @ToString.Exclude
    @JoinColumn(name = "cv_id")
    private CvEntity cv;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "organization")
    private String organization;

    @Column(name = "present")
    private boolean present;

    @Column(name = "specialization")
    private String specialization;

    @Column(name = "start_date")
    private LocalDate startDate;

}
