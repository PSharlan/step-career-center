package by.itstep.career.model.user.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserCreationRequest {

    @Email(message = "Invalid email")
    private String email;

    @NotEmpty(message = "First name can't be empty")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    private String lastName;

    @NotNull(message = "Password can't be null")
    @Size(min = 6, message = "Password is shorter then 6 characters")
    private String password;

    @NotEmpty(message = "Username can't be empty")
    private String username;

}
