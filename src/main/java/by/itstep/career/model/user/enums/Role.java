package by.itstep.career.model.user.enums;

public enum Role {

    ROLE_ADMIN,
    ROLE_CONSULTANT,
    ROLE_EVENT_MANAGER,
    ROLE_VACANCY_MANAGER,
    ROLE_STORY_MANAGER,
    ROLE_SUPERUSER

}
