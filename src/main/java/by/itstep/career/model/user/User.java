package by.itstep.career.model.user;

import by.itstep.career.model.user.enums.Role;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class User {

    private UUID id;
    private String email;
    private String firstName;
    private String lastName;
    private List<Role> roles;
    private String username;

}
