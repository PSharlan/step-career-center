package by.itstep.career.model.user.request;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ChangePasswordRequest {

    @Email(message = "Invalid email")
    private String email;

    @NotNull(message = "Password can't be null")
    @Size(min = 6, message = "Password is shorter then 6 characters")
    private String newPassword;

    @NotNull(message = "Password can't be null")
    private String oldPassword;

}
