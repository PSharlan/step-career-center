package by.itstep.career.model.auth;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class AuthenticationRequest {

    @NotEmpty(message="Username can't be empty")
    private String username;
    @NotNull(message="Password can't be null")
    @Size(min=6, message="Password is shorter then 6 characters")
    private String password;

}
