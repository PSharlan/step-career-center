package by.itstep.career.model.cv;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class CvEducation {

    private LocalDate endDate;
    private String organization;
    private boolean present;
    private String specialization;
    private LocalDate startDate;

}
