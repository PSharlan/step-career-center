package by.itstep.career.model.cv;

import by.itstep.career.model.image.Image;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class Cv {

    private UUID id;
    private List<CvEducation> education;
    private String email;
    private String fileUrl;
    private Image image;
    private String linkedin;
    private String name;
    private String phone;
    private String personalDetails;
    private String position;
    private List<CvProject> projects;
    private String skills;
    private String skype;
    private List<CvWorkExperience> workExperience;

}
