package by.itstep.career.model.cv.request;

import by.itstep.career.model.cv.CvEducation;
import by.itstep.career.model.cv.CvProject;
import by.itstep.career.model.cv.CvWorkExperience;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
public class CvCreationRequest {

    private List<CvEducation> education;

    @Email(message = "Invalid email")
    private String email;

    private UUID imageId;

    private String linkedin;

    @NotEmpty(message = "Name can't be empty")
    private String name;

    @NotEmpty(message = "Phone can't be empty")
    private String phone;

    @NotEmpty(message = "Personal details can't be empty")
    private String personalDetails;

    @NotEmpty(message = "Position can't be empty")
    private String position;

    private List<CvProject> projects;

    @NotEmpty(message = "Skills can't be empty")
    private String skills;

    @NotEmpty(message = "Skype can't be empty")
    private String skype;

    private List<CvWorkExperience> workExperience;

}
