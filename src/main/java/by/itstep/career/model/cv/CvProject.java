package by.itstep.career.model.cv;

import lombok.Data;

@Data
public class CvProject {

    private String achievements;
    private String description;
    private String environment;
    private String name;

}
