package by.itstep.career.model.cv;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;

@Data
public class CvWorkExperience {

    private String company;
    private LocalDate endDate;
    private String position;
    private boolean present;
    private String responsibilities;
    private LocalDate startDate;

}
