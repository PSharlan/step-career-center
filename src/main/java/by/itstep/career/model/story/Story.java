package by.itstep.career.model.story;

import by.itstep.career.model.image.Image;
import by.itstep.career.model.story.enums.StoryStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class Story {

    private UUID id;
    private String authorName;
    private String description;
    private String header;
    private Image image;
    private StoryStatus status;
    private String videoEmbedUrl;
    private String videoUrl;

}
