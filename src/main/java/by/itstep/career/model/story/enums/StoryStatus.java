package by.itstep.career.model.story.enums;

public enum StoryStatus {

    NEW,
    PUBLISHED

}
