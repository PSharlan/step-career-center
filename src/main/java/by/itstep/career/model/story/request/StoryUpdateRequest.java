package by.itstep.career.model.story.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class StoryUpdateRequest {

    @NotNull(message = "ID must be specified")
    private UUID id;

    @NotEmpty(message = "author name can't be empty")
    private String authorName;

    @NotEmpty(message = "description can't be empty")
    private String description;

    @NotEmpty(message = "header can't be empty")
    private String header;

    @NotEmpty(message = "video url can't be empty")
    private String videoUrl;

}
