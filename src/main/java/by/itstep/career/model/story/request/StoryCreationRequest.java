package by.itstep.career.model.story.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class StoryCreationRequest {

    @NotEmpty(message = "author name can't be empty")
    private String authorName;

    @NotEmpty(message = "description can't be empty")
    private String description;

    @NotEmpty(message = "header can't be empty")
    private String header;

    @NotNull(message = "Image must be specified")
    private UUID imageId;

    @NotEmpty(message = "video url can't be empty")
    private String videoUrl;

}
