package by.itstep.career.model.image;

import lombok.Data;

import java.util.UUID;

@Data
public class Image {

    private UUID id;
    private String preview;
    private String main;

}
