package by.itstep.career.model.event;

import by.itstep.career.model.event.enums.EventStatus;
import by.itstep.career.model.image.Image;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class EventPreview {

    private UUID id;
    private Image image;
    private String name;
    private EventStatus status;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime time;

    private String type;

}
