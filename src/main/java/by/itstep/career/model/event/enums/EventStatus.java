package by.itstep.career.model.event.enums;

public enum EventStatus {

    DEVELOPING,
    FINISHED,
    PUBLISHED

}
