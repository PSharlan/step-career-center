package by.itstep.career.model.event;


import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class EventEntry {

    private UUID id;
    private String company;
    private String email;
    private Event event;
    private String firstName;
    private String lastName;
    private Boolean student;

}
