package by.itstep.career.model.event.request;


import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class EventEntryCreationRequest {

    @NotEmpty(message = "Company or university name can't be empty")
    private String company;

    @Email(message = "Invalid email")
    private String email;

    @NotNull(message = "Event id must be specified")
    private UUID eventId;

    @NotEmpty(message = "First name can't be empty")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    private String lastName;

    @NotNull(message = "Is a student must be specified")
    private Boolean student;

}
