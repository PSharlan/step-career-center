package by.itstep.career.model.event.request;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class EventCreationRequest {

    @NotEmpty(message = "Address can't be empty")
    private String address;

    @NotEmpty(message = "Description can't be empty")
    private String description;

    @NotNull(message = "Image must be specified")
    private UUID imageId;

    @NotEmpty(message = "Name can't be empty")
    private String name;

    @NotNull(message = "Time must be specified")
    private LocalDateTime time;

    @NotEmpty(message = "Type can't be empty")
    private String type;

}
