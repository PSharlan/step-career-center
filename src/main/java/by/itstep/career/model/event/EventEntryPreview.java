package by.itstep.career.model.event;


import lombok.Data;

import java.util.UUID;

@Data
public class EventEntryPreview {

    private UUID id;
    private String company;
    private String email;
    private UUID eventId;
    private String eventName;
    private String firstName;
    private String lastName;
    private Boolean student;

}
