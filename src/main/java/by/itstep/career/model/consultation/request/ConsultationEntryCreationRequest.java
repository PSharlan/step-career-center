package by.itstep.career.model.consultation.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class ConsultationEntryCreationRequest {

    @NotNull(message = "Appointment time must be specified")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime assignedAt;

    private String comment;

    @Email(message = "Invalid email")
    private String email;

    @NotEmpty(message = "First name can't be empty")
    private String firstName;

    private String group;

    @NotEmpty(message = "Last name can't be empty")
    private String lastName;

    private String phone;

    @NotNull(message = "Is a student must be specified")
    private Boolean student;

}
