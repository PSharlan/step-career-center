package by.itstep.career.model.consultation.request;

import by.itstep.career.model.consultation.enums.ConsultationStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Data
public class ConsultationEntryUpdateRequest {

    @NotNull(message = "ID must be specified")
    private UUID id;

    @NotNull(message = "Appointment time must be specified")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime assignedAt;

    private String comment;

    @NotNull(message = "Status must be specified")
    private ConsultationStatus consultationStatus;

    @Email(message = "Invalid email")
    private String email;

    @NotEmpty(message = "First name can't be empty")
    private String firstName;

    private String group;

    @NotEmpty(message = "Last name can't be empty")
    private String lastName;

    private String phone;

    @NotNull(message = "Is a student must be specified")
    private Boolean student;

}
