package by.itstep.career.model.consultation.enums;

public enum ConsultationStatus {

    NEW,
    CONFIRMED,
    PROVIDED,
    CANCELLED

}
