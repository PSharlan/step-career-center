package by.itstep.career.model.vacancy;

import by.itstep.career.model.vacancy.enums.VacancyExperience;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

@Data
public class VacancyPreview {

    private UUID id;
    private String company;
    private String name;
    private VacancyExperience experience;

}
