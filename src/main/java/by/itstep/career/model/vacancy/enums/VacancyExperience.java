package by.itstep.career.model.vacancy.enums;

public enum VacancyExperience {

    INTERN,
    JUNIOR,
    MIDDLE,
    SENIOR

}
