package by.itstep.career.model.vacancy.request;

import by.itstep.career.model.vacancy.enums.VacancyExperience;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class VacancyCreationRequest {

    @NotEmpty(message = "Company name can't be empty")
    private String company;

    @NotEmpty(message = "Description can't be empty")
    private String description;

    @NotEmpty(message = "Name can't be empty")
    private String name;

    @NotNull(message = "Experience must be specified")
    private VacancyExperience experience;

}
