package by.itstep.career.model.vacancy;

import by.itstep.career.model.vacancy.enums.VacancyExperience;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
public class Vacancy {

    private UUID id;
    private String company;
    private LocalDateTime createdAt;
    private String description;
    private String name;
    private VacancyExperience experience;

}
