package by.itstep.career.service;

import by.itstep.career.model.user.request.ChangePasswordRequest;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.request.UserCreationRequest;
import by.itstep.career.model.user.request.UserUpdateRequest;
import by.itstep.career.model.user.enums.Role;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface UserService {

    User create(UserCreationRequest request);

    Page<User> findAll(Integer page, Integer size);

    User get(UUID id);

    User getByUsername(String username);

    User getByEmail(String email);

    User update(UserUpdateRequest request);

    User changePassword(ChangePasswordRequest request);

    User changeRole(UUID id, List<Role> roles);

    void delete(UUID id);

}
