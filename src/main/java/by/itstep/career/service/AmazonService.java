package by.itstep.career.service;

import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.entity.image.enums.ImageTypeEntity;
import by.itstep.career.utils.FileType;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface AmazonService {

    ImageEntity uploadImage(MultipartFile multipartFile, ImageTypeEntity type);

    ImageEntity uploadDefaultImage(File file, ImageTypeEntity type);

    String uploadFile(File file, FileType type);

    byte[] downloadCvImageAsBytes(String key);

}
