package by.itstep.career.service;

import by.itstep.career.model.vacancy.Vacancy;
import by.itstep.career.model.vacancy.VacancyPreview;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.model.vacancy.request.VacancyUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.UUID;

public interface VacancyService {

    Vacancy create(VacancyCreationRequest request);

    Page<VacancyPreview> findAll(Integer page, Integer size);

    Vacancy get(UUID id);

    Vacancy update(VacancyUpdateRequest request);

    void delete(UUID id);

}
