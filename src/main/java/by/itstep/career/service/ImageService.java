package by.itstep.career.service;

import by.itstep.career.entity.image.enums.ImageTypeEntity;
import by.itstep.career.model.image.Image;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface ImageService {

    Image save(MultipartFile image, ImageTypeEntity type);

}
