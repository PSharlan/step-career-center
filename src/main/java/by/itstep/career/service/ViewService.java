package by.itstep.career.service;

import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;

public interface ViewService {

    ModelAndView getMainPage();

    ModelAndView getAllVacanciesPage(Integer page);

    ModelAndView getAllStoriesPage(Integer page);

    ModelAndView getAllEventsPage(Integer page);

    ModelAndView getAllProjectsPage(Integer page);

    ModelAndView getPartnersPage();

    ModelAndView getSingleEventPage(UUID id);

    ModelAndView getCreateResumePage();

    ModelAndView getSingleProjectPage(Integer id);

}
