package by.itstep.career.service;

import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.consultation.ConsultationEntryPreview;
import by.itstep.career.model.consultation.enums.ConsultationStatus;
import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.consultation.request.ConsultationEntryUpdateRequest;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface ConsultationEntryService {

    ConsultationEntry create(ConsultationEntryCreationRequest request);

    Page<ConsultationEntryPreview> findAll(Integer page, Integer size);

    Page<ConsultationEntryPreview> findByEmail(String email, Integer page, Integer size);

    Page<ConsultationEntryPreview> findByPhone(String phone, Integer page, Integer size);

    Page<ConsultationEntryPreview> findByStatus(ConsultationStatus status, Integer page, Integer size);

    ConsultationEntry get(UUID id);

    ConsultationEntry update(ConsultationEntryUpdateRequest request);

    ConsultationEntry updateStatus(UUID id, ConsultationStatus status);

    void delete(UUID id);

    List<Integer> findAvailableTime(LocalDate day);

}
