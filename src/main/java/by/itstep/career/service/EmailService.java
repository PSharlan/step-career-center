package by.itstep.career.service;

import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.cv.Cv;
import by.itstep.career.model.event.EventEntry;

public interface EmailService {

    void sendNewEntryEmail(ConsultationEntry entry);

    void sendEntryConfirmationEmail(ConsultationEntry entry);

    void sendNewEntryEmail(EventEntry entry);

    void sendEntryConfirmationEmail(EventEntry entry);

    void sendNewCvEmail(Cv cv);

    void sendCvConfirmationEmail(Cv cv);

}
