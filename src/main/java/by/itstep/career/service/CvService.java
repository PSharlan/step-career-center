package by.itstep.career.service;

import by.itstep.career.model.image.Image;
import by.itstep.career.model.cv.request.CvCreationRequest;
import org.springframework.web.multipart.MultipartFile;

public interface CvService {

    String create(CvCreationRequest request);

}
