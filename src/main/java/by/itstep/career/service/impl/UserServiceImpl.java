package by.itstep.career.service.impl;

import by.itstep.career.config.AppConfig;
import by.itstep.career.entity.user.UserEntity;
import by.itstep.career.entity.user.enums.RoleEntity;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.enums.Role;
import by.itstep.career.model.user.request.ChangePasswordRequest;
import by.itstep.career.model.user.request.UserCreationRequest;
import by.itstep.career.model.user.request.UserUpdateRequest;
import by.itstep.career.repository.UserRepository;
import by.itstep.career.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static by.itstep.career.entity.user.enums.RoleEntity.ROLE_ADMIN;
import static by.itstep.career.mapper.UserMapper.USER_MAPPER;
import static by.itstep.career.utils.ExceptionUtils.*;
import static java.time.LocalDateTime.now;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder passwordEncoder;

    private final AppConfig appConfig;

    @PostConstruct
    public void init() {
        AppConfig.DefaultUser defaultUserInfo = appConfig.getDefaultUser();
        if (userRepository.existsByEmail(defaultUserInfo.getEmail())) {
            return;
        }
        final UserEntity user = UserEntity.builder()
                .email(defaultUserInfo.getEmail())
                .firstName("SUPERUSER")
                .lastName("SUPERUSER")
                .password(passwordEncoder.encode(defaultUserInfo.getPassword()))
                .roles(getAllRoles())
                .username(defaultUserInfo.getUsername())
                .build();
        user.setCreatedAt(now());
        userRepository.save(user);
        log.info("UserServiceImpl init() - default super user was successfully saved");
    }

    @Override
    @Transactional
    public User create(final UserCreationRequest request) {
        final UserEntity user = USER_MAPPER.toEntity(request);
        user.setRoles(Collections.singletonList(ROLE_ADMIN));
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setCreatedAt(now());

        throwIfUserExists(user);

        final UserEntity saved = userRepository.save(user);
        log.info("IN create - user: {} was successfully created", user);
        return USER_MAPPER.toModel(saved);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<User> findAll(final Integer page, final Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        final Page<User> users = userRepository.findAll(pageable).map(USER_MAPPER::toModel);
        log.info("IN findAll - {} user were found", users.getContent().size());
        return users;
    }

    @Override
    @Transactional(readOnly = true)
    public User getByUsername(final String username) {
        final UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> notFoundException(String.format("User with username : %s not found", username)));

        log.info("IN getByUsername - user: {} found by username: {}", user, username);
        return USER_MAPPER.toModel(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User get(final UUID id) {
        final UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("User with id : %s not found", id)));
        log.info("IN get - user: {} found by id: {}", user, id);
        return USER_MAPPER.toModel(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User getByEmail(final String email) {
        final UserEntity user = userRepository.findByEmail(email)
                .orElseThrow(() -> notFoundException(String.format("User is not found. Unknown email: %s", email)));

        log.info("IN getByUsername - user: {} found by email: {}", user, email);
        return USER_MAPPER.toModel(user);
    }

    @Override
    @Transactional
    public User update(final UserUpdateRequest request) {
        final UserEntity user = userRepository.findById(request.getId())
                .orElseThrow(() -> notFoundException(String.format("User with id : %s not found", request.getId())));

        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setUpdatedAt(now());

        final UserEntity updated = userRepository.save(user);

        log.info("IN update - customer: {} successfully updated", updated);
        return USER_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public User changePassword(final ChangePasswordRequest request) {
        final UserEntity user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> notFoundException(
                        String.format("User is not found. Unknown email: %s", request.getEmail())));

        final String oldPassword = request.getOldPassword();
        final String newPassword = request.getNewPassword();
        final String currentPassword = user.getPassword();
        
        if (!passwordEncoder.matches(oldPassword, currentPassword)) {
            throw passwordsNotEqualsException("Wrong password");
        }

        user.setPassword(passwordEncoder.encode(newPassword));
        user.setUpdatedAt(now());

        final UserEntity updated = userRepository.save(user);
        log.info("IN changePassword - customer: {} successfully updated", updated);
        return USER_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public User changeRole(final UUID id, final List<Role> roles) {
        final UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("User with id : %s not found", id)));

        final List<RoleEntity> rolesToSet = roles.stream().map(USER_MAPPER::toEntity)
                .collect(toList());

        user.setRoles(rolesToSet);
        user.setUpdatedAt(now());

        final UserEntity updated = userRepository.save(user);
        log.info("IN changeRole - customer: {} successfully updated", updated);
        return USER_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        final UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("User with id : %s not found", id)));

        user.setDeletedAt(now());
        final UserEntity updated = userRepository.save(user);
        log.info("IN delete - user: {} updated with status 'DELETED'", updated);
    }

    private void throwIfUserExists(final UserEntity user) {
        final String username = user.getUsername();
        if (userRepository.existsByUsername(username)) {
            throw entityAlreadyExistsException(String.format("Username: %s already exists", username));
        }
        
        final String email = user.getEmail();
        if (userRepository.existsByEmail(email)) {
            throw entityAlreadyExistsException(String.format("Email: %s already exists", email));
        }
    }

    private List<RoleEntity> getAllRoles() {
        return Arrays.stream(RoleEntity.values())
                .collect(toList());
    }

}
