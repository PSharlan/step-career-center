package by.itstep.career.service.impl;


import by.itstep.career.entity.event.EventEntity;
import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventPreview;
import by.itstep.career.model.event.enums.EventStatus;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import by.itstep.career.model.image.Image;
import by.itstep.career.repository.EventRepository;
import by.itstep.career.repository.ImageRepository;
import by.itstep.career.service.AmazonService;
import by.itstep.career.service.EventService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static by.itstep.career.entity.event.enums.EventStatusEntity.DEVELOPING;
import static by.itstep.career.entity.event.enums.EventStatusEntity.PUBLISHED;
import static by.itstep.career.entity.image.enums.ImageTypeEntity.EVENT_IMAGE;
import static by.itstep.career.mapper.EventMapper.EVENT_MAPPER;
import static by.itstep.career.mapper.ImageMapper.IMAGE_MAPPER;
import static by.itstep.career.utils.ExceptionUtils.notFoundException;
import static java.lang.String.format;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@AllArgsConstructor
public class EventServiceImpl implements EventService {

    private final AmazonService amazonService;
    private final EventRepository eventRepository;
    private final ImageRepository imageRepository;

    @Override
    @Transactional
    public Event create(final EventCreationRequest request) {
        final EventEntity toSave = EVENT_MAPPER.toEntity(request);
        final ImageEntity image = imageRepository.findById(request.getImageId()).orElseThrow(() ->
                notFoundException(format("Image with id : %s not found", request.getImageId())));

        toSave.setImage(image);
        toSave.setCreatedAt(now());
        toSave.setStatus(DEVELOPING);

        final EventEntity saved = eventRepository.save(toSave);
        log.info("IN create - event: {} successfully created", saved);
        return EVENT_MAPPER.toModel(saved);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventPreview> findAll(final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<EventPreview> events = eventRepository.findAll(pageable).map(EVENT_MAPPER::toPreview);

        log.info("IN findAll - {} events were found", events.getContent().size());
        return events;
    }

    @Override
    public Page<EventPreview> findAllPublished(final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<EventPreview> events = eventRepository.findByEventStatus(PUBLISHED.name(), pageable)
                .map(EVENT_MAPPER::toPreview);

        log.info("IN findAllPublished - {} events were found", events.getContent().size());
        return events;
    }

    @Override
    public Page<EventPreview> findByStatus(final EventStatus status, final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<EventPreview> events = eventRepository
                .findByEventStatus(EVENT_MAPPER.toEntity(status).name(), pageable)
                .map(EVENT_MAPPER::toPreview);

        log.info("IN findAllPublished - {} events were found", events.getContent().size());
        return events;
    }

    @Override
    @Transactional(readOnly = true)
    public Event get(UUID id) {
        final EventEntity event = eventRepository.findById(id)
                .orElseThrow(() -> notFoundException(format("Event with id : %s not found", id)));
        log.info("IN get - event: {} found by id: {}", event, id);
        return EVENT_MAPPER.toModel(event);
    }

    @Override
    @Transactional
    public Event update(final EventUpdateRequest request) {
        final EventEntity toUpdate = eventRepository.findById(request.getId())
                .orElseThrow(() -> notFoundException(format("Event with id : %s not found", request.getId())));

        toUpdate.setAddress(request.getAddress());
        toUpdate.setDescription(request.getDescription());
        toUpdate.setName(request.getName());
        toUpdate.setTime(request.getTime());
        toUpdate.setUpdatedAt(now());

        final EventEntity updated = eventRepository.save(toUpdate);
        log.info("IN update - event: {} successfully updated", updated);
        return EVENT_MAPPER.toModel(updated);
    }

    @Override
    public Event updateStatus(final UUID id, final EventStatus status) {
        final EventEntity event = eventRepository.findById(id)
                .orElseThrow(() -> notFoundException(format("Event with id : %s not found", id)));

        event.setStatus(EVENT_MAPPER.toEntity(status));
        event.setUpdatedAt(now());

        final EventEntity updated = eventRepository.save(event);
        log.info("IN updateStatus - event: {} successfully updated", updated);
        return EVENT_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        final EventEntity event = eventRepository.findById(id)
                .orElseThrow(() -> notFoundException(format("Event with id : %s not found", id)));

        event.setDeletedAt(now());

        final EventEntity updated = eventRepository.save(event);
        log.info("IN delete - event: {} updated with status 'DELETED'", updated);
    }

}
