package by.itstep.career.service.impl;

import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.entity.story.StoryEntity;
import by.itstep.career.model.image.Image;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.story.enums.StoryStatus;
import by.itstep.career.model.story.request.StoryCreationRequest;
import by.itstep.career.model.story.request.StoryUpdateRequest;
import by.itstep.career.repository.ImageRepository;
import by.itstep.career.repository.StoryRepository;
import by.itstep.career.service.AmazonService;
import by.itstep.career.service.StoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.STORY_IMAGE;
import static by.itstep.career.entity.story.enums.StoryStatusEntity.NEW;
import static by.itstep.career.entity.story.enums.StoryStatusEntity.PUBLISHED;
import static by.itstep.career.mapper.ImageMapper.IMAGE_MAPPER;
import static by.itstep.career.mapper.StoryMapper.STORY_MAPPER;
import static by.itstep.career.utils.ExceptionUtils.notFoundException;
import static by.itstep.career.utils.UriUtils.buildEmbedYoutubeLink;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@AllArgsConstructor
public class StoryServiceImpl implements StoryService {

    private final AmazonService amazonService;
    private final ImageRepository imageRepository;
    private final StoryRepository storyRepository;

    @Override
    @Transactional(readOnly = true)
    public Story get(final UUID id) {
        final StoryEntity story = storyRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("Story with id : %s not found", id)));
        log.info("IN get - story: {} found by id: {}", story, id);
        return STORY_MAPPER.toModel(story);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Story> findAll(final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<Story> stories = storyRepository.findAll(pageable).map(STORY_MAPPER::toModel);

        log.info("IN findAll - {} stories were found", stories.getContent().size());
        return stories;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Story> findAllPublished(final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<Story> stories = storyRepository.findByStoryStatus(PUBLISHED.name(), pageable)
                .map(STORY_MAPPER::toModel);

        log.info("IN findAllPublished - {} stories were found", stories.getContent().size());
        return stories;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Story> findByStatus(final StoryStatus status, final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<Story> stories = storyRepository
                .findByStoryStatus(STORY_MAPPER.toEntity(status).name(), pageable)
                .map(STORY_MAPPER::toModel);

        log.info("IN findAllPublished - {} stories were found", stories.getContent().size());
        return stories;
    }

    @Override
    @Transactional
    public Story create(final StoryCreationRequest request) {
        final ImageEntity image = imageRepository.findById(request.getImageId()).orElseThrow(() ->
                notFoundException(String.format("Image with id : %s not found", request.getImageId())));
        final StoryEntity toSave = STORY_MAPPER.toEntity(request);

        toSave.setVideoEmbedUrl(buildEmbedYoutubeLink(request.getVideoUrl()));
        toSave.setImage(image);
        toSave.setCreatedAt(now());
        toSave.setStatus(NEW);

        final StoryEntity saved = storyRepository.save(toSave);
        log.info("IN create - story: {} successfully created", saved);
        return STORY_MAPPER.toModel(saved);
    }

    @Override
    @Transactional
    public Story update(final StoryUpdateRequest request) {
        final StoryEntity toUpdate = storyRepository.findById(request.getId())
                .orElseThrow(() -> notFoundException(String.format("Story with id : %s not found", request.getId())));

        toUpdate.setAuthorName(request.getAuthorName());
        toUpdate.setDescription(request.getDescription());
        toUpdate.setHeader(request.getHeader());
        toUpdate.setVideoUrl(request.getVideoUrl());
        toUpdate.setVideoEmbedUrl(buildEmbedYoutubeLink(request.getVideoUrl()));
        toUpdate.setUpdatedAt(now());

        final StoryEntity updated = storyRepository.save(toUpdate);
        log.info("IN update - story: {} successfully updated", updated);
        return STORY_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public Story updateStatus(final UUID id, final StoryStatus status) {
        final StoryEntity story = storyRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("Story with id : %s not found", id)));

        story.setStatus(STORY_MAPPER.toEntity(status));
        story.setUpdatedAt(now());

        final StoryEntity updated = storyRepository.save(story);
        log.info("IN updateStatus - story: {} successfully updated", updated);
        return STORY_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        final StoryEntity story = storyRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("Story with id : %s not found", id)));

        story.setDeletedAt(now());

        final StoryEntity updated = storyRepository.save(story);
        log.info("IN delete - story: {} updated with status 'DELETED'", updated);
    }

}
