package by.itstep.career.service.impl;


import by.itstep.career.entity.user.UserEntity;
import by.itstep.career.entity.user.enums.RoleEntity;
import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.cv.Cv;
import by.itstep.career.model.event.EventEntry;
import by.itstep.career.repository.UserRepository;
import by.itstep.career.service.EmailService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.internet.MimeMessage;
import java.util.Arrays;

import static by.itstep.career.entity.user.enums.RoleEntity.*;

@Slf4j
@Service
@AllArgsConstructor
public class EmailServiceImpl implements EmailService {

    private static final String CONSULTATION_ENTRY_EMAIL_SUBJECT = "New consultation request";
    private static final String CONSULTATION_ENTRY_EMAIL_TEMPLATE = "email/notification/new-consultation-entry.html";

    private static final String CONSULTATION_ENTRY_CONFIRMATION_EMAIL_SUBJECT = "IT-step Consultation";
    private static final String CONSULTATION_ENTRY_CONFIRMATION_EMAIL_TEMPLATE = "email/confirmation/consultation-entry-confirmation.html";

    private static final String EVENT_ENTRY_EMAIL_SUBJECT = "New registration for the event";
    private static final String EVENT_ENTRY_EMAIL_TEMPLATE = "email/notification/new-event-entry.html";

    private static final String EVENT_ENTRY_CONFIRMATION_EMAIL_SUBJECT = "IT-step Event";
    private static final String EVENT_ENTRY_CONFIRMATION_EMAIL_TEMPLATE = "email/confirmation/event-entry-confirmation.html";

    private static final String CV_EMAIL_SUBJECT = "New cv";
    private static final String CV_EMAIL_TEMPLATE = "email/notification/new-cv.html";

    private static final String CV_CONFIRMATION_EMAIL_SUBJECT = "IT-step CV";
    private static final String CV_CONFIRMATION_EMAIL_TEMPLATE = "email/confirmation/cv-confirmation.html";

    private JavaMailSender emailSender;
    private TemplateEngine templateEngine;
    private UserRepository userRepository;

    @Override
    public void sendNewEntryEmail(final ConsultationEntry entry) {
        final Context context = prepareContext("entry", entry);
        final String[] receivers = getReceivers(ROLE_CONSULTANT);

        if (receivers.length == 0) {
            log.info("IN sendNewConsultationEntryEmail - email wasn't sent. No receivers");
            return;
        }
        log.info("IN sendNewConsultationEntryEmail - email receivers: {}", Arrays.toString(receivers));

        sendEmail(context, CONSULTATION_ENTRY_EMAIL_TEMPLATE, CONSULTATION_ENTRY_EMAIL_SUBJECT, receivers);
    }

    @Override
    public void sendEntryConfirmationEmail(final ConsultationEntry entry) {
        final Context context = prepareContext("entry", entry);
        final String receiver = entry.getEmail();
        log.info("IN sendConsultationEntryConfirmationEmail - email receivers: {}", receiver);

        sendEmail(context, CONSULTATION_ENTRY_CONFIRMATION_EMAIL_TEMPLATE, CONSULTATION_ENTRY_CONFIRMATION_EMAIL_SUBJECT, receiver);
    }

    @Override
    public void sendNewEntryEmail(final EventEntry entry) {
        final Context context = prepareContext("entry", entry);
        final String[] receivers = getReceivers(ROLE_EVENT_MANAGER);

        if (receivers.length == 0) {
            log.info("IN sendNewEventEntryEmail - email wasn't sent. No receivers");
            return;
        }

        log.info("IN sendNewEventEntryEmail - email receivers: {}", Arrays.toString(receivers));

        sendEmail(context, EVENT_ENTRY_EMAIL_TEMPLATE, EVENT_ENTRY_EMAIL_SUBJECT, receivers);
    }

    @Override
    public void sendEntryConfirmationEmail(final EventEntry entry) {
        final Context context = prepareContext("entry", entry);
        final String receiver = entry.getEmail();
        log.info("IN sendEventEntryConfirmationEmail - email receiver: {}", receiver);

        sendEmail(context, EVENT_ENTRY_CONFIRMATION_EMAIL_TEMPLATE, EVENT_ENTRY_CONFIRMATION_EMAIL_SUBJECT, receiver);
    }

    @Override
    public void sendNewCvEmail(final Cv cv) {
        final Context context = prepareContext("cv", cv);
        final String[] receivers = getReceivers(ROLE_VACANCY_MANAGER);

        if (receivers.length == 0) {
            log.info("IN sendNewCvEmail - email wasn't sent. No receivers");
            return;
        }

        log.info("IN sendNewCvEmail - email receivers: {}", Arrays.toString(receivers));

        sendEmail(context, CV_EMAIL_TEMPLATE, CV_EMAIL_SUBJECT, receivers);
    }

    @Override
    public void sendCvConfirmationEmail(final Cv cv) {
        final Context context = prepareContext("cv", cv);
        final String receiver = cv.getEmail();
        log.info("IN sendNewCvEmail - email receiver: {}", receiver);

        sendEmail(context, CV_CONFIRMATION_EMAIL_TEMPLATE, CV_CONFIRMATION_EMAIL_SUBJECT, receiver);
    }

    @SneakyThrows
    private void sendEmail(final Context context, final String templateName, final String subject,
                           final String... receivers) {
        final String html = templateEngine.process(templateName, context);
        final MimeMessage message = emailSender.createMimeMessage();
        final MimeMessageHelper helper = new MimeMessageHelper(message);

        helper.setTo(receivers);
        helper.setText(html, true);
        helper.setSubject(subject);

        emailSender.send(message);
        log.info("IN sendEmail - email was successfully sent");
    }

    private String[] getReceivers(final RoleEntity role) {
        return userRepository.findByRole(role.name())
                .stream()
                .map(UserEntity::getEmail)
                .toArray(String[]::new);
    }

    private Context prepareContext(final String key, final Object object) {
        final Context context = new Context();
        context.setVariable(key, object);
        return context;
    }

}
