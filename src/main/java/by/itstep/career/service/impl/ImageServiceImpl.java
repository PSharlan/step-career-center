package by.itstep.career.service.impl;

import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.entity.image.enums.ImageTypeEntity;
import by.itstep.career.model.image.Image;
import by.itstep.career.repository.ImageRepository;
import by.itstep.career.service.AmazonService;
import by.itstep.career.service.ImageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.STORY_IMAGE;
import static by.itstep.career.mapper.ImageMapper.IMAGE_MAPPER;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImageServiceImpl implements ImageService {

    private final ImageRepository imageRepository;
    private final AmazonService amazonService;

    @Override
    @Transactional
    public Image save(final MultipartFile image, final ImageTypeEntity type) {
        final ImageEntity uploaded = amazonService.uploadImage(image, type);
        final ImageEntity saved = imageRepository.save(uploaded);

        log.info("IN saveImage - image: {} successfully uploaded and saved", saved);
        return IMAGE_MAPPER.toModel(saved);
    }

}
