package by.itstep.career.service.impl;

import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.entity.image.enums.ImageTypeEntity;
import by.itstep.career.service.AmazonService;
import by.itstep.career.utils.FileType;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Objects;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.*;
import static by.itstep.career.service.impl.AmazonServiceImpl.ImageType.*;
import static by.itstep.career.utils.AmazonUtils.getReadOnlyPolicyJsonRequest;
import static by.itstep.career.utils.ImageUtils.resizeImage;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@RequiredArgsConstructor
public class AmazonServiceImpl implements AmazonService {

    private final AmazonS3 s3client;

    @Value("${amazonProperties.bucketName}")
    private String bucketName;

    @Value("${amazonProperties.endpointUrl}")
    private String endpointUrl;

    @PostConstruct
    public void init() {
        if (!s3client.doesBucketExist(bucketName)) {
            log.info("AmazonServiceImpl - creating new bucket: {}", bucketName);
            s3client.createBucket(bucketName);
            log.info("AmazonServiceImpl - sending request for read only policy");
            s3client.setBucketPolicy(bucketName, getReadOnlyPolicyJsonRequest(bucketName));
        }
    }

    @Override
    @SneakyThrows
    public ImageEntity uploadImage(final MultipartFile multipartFile, final ImageTypeEntity type) {
        final String fileName = multipartFile.getOriginalFilename();
        final File copyOfOriginal = new File(Objects.requireNonNull(fileName));

        try (final FileOutputStream fos = new FileOutputStream(copyOfOriginal)) {
            fos.write(multipartFile.getBytes());
        }

        final ImageEntity image = saveImage(copyOfOriginal, type);

        copyOfOriginal.delete();
        log.info("IN uploadImage - amazon: image was successfully uploaded");
        return image;
    }

    @Override
    public ImageEntity uploadDefaultImage(File file, ImageTypeEntity type) {
        final ImageEntity image = saveImage(file, type);

        log.info("IN uploadDefaultImage - amazon: image was successfully uploaded! Type: {}", type.name());
        return image;
    }

    private ImageEntity saveImage(File file, ImageTypeEntity type) {
        final String generatedName = generateFileName(file.getName());

        String original = "";
        String main = "";
        String preview = "";

        if (type == EVENT_IMAGE) {
            original = uploadSingleImage(file, generatedName, EVENT_ORIGINAL);
            main = uploadSingleImage(file, generatedName, EVENT_MAIN);
            preview = uploadSingleImage(file, generatedName, EVENT_PREVIEW);
        } else if (type == CV_IMAGE) {
            original = uploadSingleImage(file, generatedName, CV_ORIGINAL);
            main = uploadSingleImage(file, generatedName, CV_MAIN);
            preview = main;
        } else if (type == STORY_IMAGE) {
            original = uploadSingleImage(file, generatedName, STORY_ORIGINAL);
            main = uploadSingleImage(file, generatedName, STORY_MAIN);
            preview = main;
        }

        final ImageEntity image = new ImageEntity();
        image.setName(generatedName);
        image.setOriginal(original);
        image.setMain(main);
        image.setPreview(preview);
        image.setImageType(type);
        image.setCreatedAt(now());

        return image;
    }

    @Override
    public String uploadFile(final File file, final FileType type) {
        final String fileName = file.getName();
        final String place = bucketName + "/" + type.getFolder();
        final PutObjectRequest putObjectRequest = new PutObjectRequest(place, fileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead);
        s3client.putObject(putObjectRequest);
        log.info("IN uploadFile - file was successfully uploaded");
        return endpointUrl + type.getFolder() + "/" + fileName;
    }

    @Override
    @SneakyThrows
    public byte[] downloadCvImageAsBytes(final String key) {
        final GetObjectRequest getObjectRequest = new GetObjectRequest(bucketName, CV_MAIN.getFolder() + "/" + key);
        final S3Object s3Object = s3client.getObject(getObjectRequest);
        final S3ObjectInputStream objectInputStream = s3Object.getObjectContent();

        log.info("IN downloadCvImageAsBytes - amazon: file was successfully downloaded");
        return IOUtils.toByteArray(objectInputStream);
    }

    @SneakyThrows
    private String uploadSingleImage(final File localImageFile, final String generatedFileName, final ImageType type) {
        final File file = new File(type.name() + "_" + generatedFileName);

        try (final FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(Files.readAllBytes(localImageFile.toPath()));
        }

        if (type.getHeight() > 0) { // if not original
            resizeImage(file, type.getHeight(), type.getWidth());
        }

        final String url = uploadImageFile(file, generatedFileName, type);
        file.delete();
        return url;
    }

    private String uploadImageFile(final File file, final String generatedFileName, final ImageType type) {
        final String place = bucketName + "/" + type.getFolder();
        final PutObjectRequest putObjectRequest = new PutObjectRequest(place, generatedFileName, file)
                .withCannedAcl(CannedAccessControlList.PublicRead);

        s3client.putObject(putObjectRequest);
        return endpointUrl + type.getFolder() + "/" + generatedFileName;
    }

    @SneakyThrows
    private String generateFileName(final String originalFilename) {
        final String[] parts = Objects.requireNonNull(originalFilename).split("\\.");
        final String extension = "." + parts[parts.length - 1];
        return RandomStringUtils.randomAlphabetic(10) + extension;
    }

    @Getter
    public enum ImageType {
        EVENT_ORIGINAL("event/image/original"),
        EVENT_PREVIEW("event/image/preview", 350, 220),
        EVENT_MAIN("event/image/main", 1070, 673),
        CV_ORIGINAL("cv/image/original"),
        CV_MAIN("cv/image/main", 150, 150),
        STORY_ORIGINAL("story/image/original"),
        STORY_MAIN("story/image/main", 440, 280);

        private String folder;
        private int width;
        private int height;

        ImageType(final String folder, final int width, final int height) {
            this.folder = folder;
            this.width = width;
            this.height = height;
        }

        ImageType(final String folder) {
            this.folder = folder;
        }

    }

}
