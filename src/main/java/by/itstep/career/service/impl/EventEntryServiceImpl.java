package by.itstep.career.service.impl;


import by.itstep.career.entity.event.EventEntity;
import by.itstep.career.entity.event.EventEntryEntity;
import by.itstep.career.model.event.EventEntry;
import by.itstep.career.model.event.EventEntryPreview;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import by.itstep.career.repository.EventEntryRepository;
import by.itstep.career.repository.EventRepository;
import by.itstep.career.service.EmailService;
import by.itstep.career.service.EventEntryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static by.itstep.career.entity.event.enums.EventStatusEntity.PUBLISHED;
import static by.itstep.career.mapper.EventEntryMapper.EVENT_ENTRY_MAPPER;
import static by.itstep.career.utils.ExceptionUtils.*;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@AllArgsConstructor
public class EventEntryServiceImpl implements EventEntryService {

    private final EventEntryRepository eventEntryRepository;
    private final EventRepository eventRepository;
    private final EmailService emailService;

    @Override
    @Transactional
    public EventEntry create(final EventEntryCreationRequest request) {
        final EventEntity event = eventRepository.findById(request.getEventId())
                .orElseThrow(() -> notFoundException(String.format(
                        "Event with id : %s not found", request.getEventId())));

        if (event.getStatus() != PUBLISHED || event.getDeletedAt() != null) {
            throw entryIsNotPossibleException("Event with id: {} is not published or deleted");
        }

        boolean emailIsTaken = eventEntryRepository.existsByEmailAndEventId(event.getId(), request.getEmail());
        if (emailIsTaken) {
            throw entityAlreadyExistsException(String.format(
                    "Entry email: %s for event id: %s is taken", request.getEmail(), event.getId()));
        }

        final EventEntryEntity toSave = EVENT_ENTRY_MAPPER.toEntity(request);
        toSave.setCreatedAt(now());
        toSave.setEvent(event);
        final EventEntryEntity saved = eventEntryRepository.save(toSave);
        log.info("IN create - entry: {} successfully created", saved);

        final EventEntry entry = EVENT_ENTRY_MAPPER.toModel(saved);
        emailService.sendNewEntryEmail(entry);
        emailService.sendEntryConfirmationEmail(entry);

        return entry;
    }

    @Override
    @Transactional(readOnly = true)
    public EventEntry get(final UUID id) {
        final EventEntryEntity entry = eventEntryRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("Entry with id : %s not found", id)));
        log.info("IN get - entry: {} found by id: {}", entry, id);
        return EVENT_ENTRY_MAPPER.toModel(entry);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventEntryPreview> findByEventId(final UUID eventId, final Integer page, final Integer size) {
        if (!eventRepository.existsById(eventId)) {
            throw notFoundException(String.format("Event with id : %s not found", eventId));
        }

        final Pageable pageable = PageRequest.of(page, size);
        final Page<EventEntryPreview> entries = eventEntryRepository.findByEventId(eventId, pageable)
                .map(EVENT_ENTRY_MAPPER::toPreview);
        log.info("IN findByEventId - entries: {} found by event id: {}", entries.getContent().size(), eventId);
        return entries;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EventEntryPreview> findByEmail(final String email, final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<EventEntryPreview> entries = eventEntryRepository.findByEmail(email, pageable)
                .map(EVENT_ENTRY_MAPPER::toPreview);
        log.info("IN findByEmail - entries: {} found by email: {}", entries.getContent().size(), email);
        return entries;
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        final EventEntryEntity entry = eventEntryRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("Entry with id : %s not found", id)));

        entry.setDeletedAt(now());

        final EventEntryEntity updated = eventEntryRepository.save(entry);
        log.info("IN delete - entry: {} updated with status 'DELETED'", updated);
    }

}
