package by.itstep.career.service.impl;

import by.itstep.career.entity.consultation.ConsultationEntryEntity;
import by.itstep.career.entity.consultation.enums.ConsultationStatusEntity;
import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.consultation.ConsultationEntryPreview;
import by.itstep.career.model.consultation.enums.ConsultationStatus;
import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.consultation.request.ConsultationEntryUpdateRequest;
import by.itstep.career.repository.ConsultationEntryRepository;
import by.itstep.career.service.ConsultationEntryService;
import by.itstep.career.service.EmailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static by.itstep.career.entity.consultation.enums.ConsultationStatusEntity.NEW;
import static by.itstep.career.mapper.ConsultationEntryMapper.CONSULTATION_ENTRY_MAPPER;
import static by.itstep.career.model.consultation.enums.ConsultationStatus.CONFIRMED;
import static by.itstep.career.utils.ConsultationTimeUtils.DEFAULT_SLOTS_HRS;
import static by.itstep.career.utils.ExceptionUtils.*;
import static java.lang.String.format;
import static java.time.LocalDateTime.now;
import static java.time.LocalTime.MAX;
import static java.time.LocalTime.MIN;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
@AllArgsConstructor
public class ConsultationEntryServiceImpl implements ConsultationEntryService {

    private final ConsultationEntryRepository consultationEntryRepository;
    private final EmailService emailService;

    @Override
    @Transactional
    public ConsultationEntry create(final ConsultationEntryCreationRequest request) {
        final ConsultationEntryEntity toSave = CONSULTATION_ENTRY_MAPPER.toEntity(request);
        toSave.setCreatedAt(now());
        toSave.setConsultationStatus(NEW);
        final ConsultationEntryEntity saved = consultationEntryRepository.save(toSave);
        log.info("IN create - consultationEntry: {} successfully created", saved);

        ConsultationEntry entryModel = CONSULTATION_ENTRY_MAPPER.toModel(saved);
        emailService.sendNewEntryEmail(entryModel);
        emailService.sendEntryConfirmationEmail(entryModel);

        return entryModel;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ConsultationEntryPreview> findAll(final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<ConsultationEntryPreview> entries = consultationEntryRepository.findAll(pageable)
                .map(CONSULTATION_ENTRY_MAPPER::toPreview);

        log.info("IN findAll - {} consultation entries were found", entries.getContent().size());
        return entries;
    }

    @Override
    public Page<ConsultationEntryPreview> findByStatus(final ConsultationStatus status,
                                                       final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final ConsultationStatusEntity toFind = CONSULTATION_ENTRY_MAPPER.toEntity(status);
        final Page<ConsultationEntryPreview> entries = consultationEntryRepository
                .findByConsultationStatus(toFind.name(), pageable).map(CONSULTATION_ENTRY_MAPPER::toPreview);

        log.info("IN delete - findByStatus: {} consultation entries were found", entries.getContent().size());
        return entries;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ConsultationEntryPreview> findByEmail(final String email, final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<ConsultationEntryPreview> entries = consultationEntryRepository
                .findByEmail(email, pageable).map(CONSULTATION_ENTRY_MAPPER::toPreview);

        log.info("IN findByEmail - consultationEntry: {} found by email: {}", entries.getContent().size(), email);
        return entries;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ConsultationEntryPreview> findByPhone(final String phone, final Integer page, final Integer size) {
        final Pageable pageable = PageRequest.of(page, size);
        final Page<ConsultationEntryPreview> entries = consultationEntryRepository
                .findByPhone(phone, pageable).map(CONSULTATION_ENTRY_MAPPER::toPreview);

        log.info("IN findByPhone - consultationEntry: {} were found by phone {}", entries.getContent().size(), phone);
        return entries;
    }

    @Override
    @Transactional(readOnly = true)
    public ConsultationEntry get(final UUID id) {
        final ConsultationEntryEntity entry = consultationEntryRepository.findById(id)
                .orElseThrow(() -> notFoundException(format("consultationEntry with id : %s not found", id)));
        log.info("IN get - consultationEntry: {} found by id: {}", entry, id);
        return CONSULTATION_ENTRY_MAPPER.toModel(entry);
    }

    @Override
    @Transactional
    public ConsultationEntry update(final ConsultationEntryUpdateRequest request) {
        throwIfDeleted(request.getId());

        final ConsultationEntryEntity toUpdate = CONSULTATION_ENTRY_MAPPER.toEntity(request);

        toUpdate.setCreatedAt(get(request.getId()).getCreatedAt());
        toUpdate.setUpdatedAt(now());

        final ConsultationEntryEntity updated = consultationEntryRepository.save(toUpdate);
        log.info("IN update - consultationEntry: {} successfully updated", updated);
        return CONSULTATION_ENTRY_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public ConsultationEntry updateStatus(final UUID id, final ConsultationStatus status) {
        throwIfDeleted(id);

        final ConsultationEntry entry = get(id);
        final LocalDateTime date = now();
        entry.setConsultationStatus(status);
        if (status == CONFIRMED) {
            entry.setConfirmedAt(date);
        }
        final ConsultationEntryEntity toUpdate = CONSULTATION_ENTRY_MAPPER.toEntity(entry);
        toUpdate.setUpdatedAt(date);

        final ConsultationEntryEntity updated = consultationEntryRepository.save(toUpdate);
        log.info("IN updateStatus - consultationEntry: {} successfully updated", updated);
        return CONSULTATION_ENTRY_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        final ConsultationEntryEntity entry = CONSULTATION_ENTRY_MAPPER.toEntity(get(id));
        entry.setDeletedAt(now());
        final ConsultationEntryEntity updated = consultationEntryRepository.save(entry);
        log.info("IN delete - consultationEntry: {} updated with status 'DELETED'", updated);
    }

    @Override
    public List<Integer> findAvailableTime(final LocalDate day) {
        final LocalTime startTime = getMinLocalTime(day);
        log.info("IN findAvailableTime - day {}", day);

        final LocalDateTime startDate = LocalDateTime.of(day, startTime);
        final LocalDateTime endDate = LocalDateTime.of(day, MAX);

        final List<Date> timeSlots = consultationEntryRepository.findAllReservedTime(startDate, endDate);

        final List<Integer> reservedSlots = timeSlots.stream()
                .map(Date::getHours)
                .collect(toList());
        final List<Integer> availableSlots = DEFAULT_SLOTS_HRS.stream()
                .filter(s -> !reservedSlots.contains(s) && s > startTime.getHour())
                .collect(toList());

        log.info("IN findAvailableTime - found slots: {}", availableSlots);
        return availableSlots;
    }

    private LocalTime getMinLocalTime(final LocalDate date) {
        final LocalDate currentDate = LocalDate.now();
        final LocalTime currentTime = LocalTime.now();

        if (date.isBefore(currentDate)) {
            throw illegalArgumentException("Available consultation time slots can't be show for past time");
        }
        if (date.isAfter(currentDate)) {
            return MIN;
        }
        return currentTime;
    }

    private void throwIfDeleted(final UUID id) {
        final ConsultationEntryEntity entry = consultationEntryRepository.findById(id)
                .orElseThrow(() -> notFoundException(format("ConsultationEntry with id : %s not found", id)));
        if (entry.getDeletedAt() != null) {
            throw entityIsDeletedException(format("Consultation with id: %s is deleted", id));
        }
    }

}
