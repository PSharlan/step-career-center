package by.itstep.career.service.impl;

import by.itstep.career.entity.cv.CvEducationEntity;
import by.itstep.career.entity.cv.CvEntity;
import by.itstep.career.entity.cv.CvProjectEntity;
import by.itstep.career.entity.cv.CvWorkExperienceEntity;
import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.exception.NotFoundException;
import by.itstep.career.model.cv.request.CvCreationRequest;
import by.itstep.career.repository.CvRepository;
import by.itstep.career.repository.ImageRepository;
import by.itstep.career.service.AmazonService;
import by.itstep.career.service.CvService;
import by.itstep.career.service.EmailService;
import by.itstep.career.utils.CvUtils;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.CV_IMAGE;
import static by.itstep.career.mapper.CvMapper.CV_MAPPER;
import static by.itstep.career.utils.FileType.CV;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@RequiredArgsConstructor
public class CvServiceImpl implements CvService {

    private final AmazonService amazonService;
    private final CvRepository cvRepository;
    private final EmailService emailService;
    private final ImageRepository imageRepository;
    private final ResourceLoader resourceLoader;

    private static final String DEFAULT_IMAGE_NAME = "default_cv_image.png";
    private static final String DEFAULT_IMAGE_PATH = "classpath:./";
    private UUID defaultImageId;

    @SneakyThrows
    @PostConstruct
    private void init() {
        final Resource resource = resourceLoader.getResource(DEFAULT_IMAGE_PATH + DEFAULT_IMAGE_NAME);
        final File temp = new File("DEFAULT_" + DEFAULT_IMAGE_NAME);

        fillTempFile(resource, temp);

        final ImageEntity defaultImage = amazonService.uploadDefaultImage(temp, CV_IMAGE);
        final ImageEntity saved = imageRepository.save(defaultImage);
        defaultImageId = saved.getId();

        temp.delete();
        log.info("In CvServiceImpl init - default image was saved");
    }

    //TODO
    @Override
    public String create(CvCreationRequest request) {
        final CvEntity toSave = CV_MAPPER.toEntity(request);
        final UUID imageId = request.getImageId();

        ImageEntity image;
        if (request.getImageId() != null) {
             image = imageRepository.findById(imageId)
                    .orElseThrow(() -> new NotFoundException("Image with id: " + imageId + " for CV was not found"));
             log.info("IN create - image for cv was found");
        } else {
            image = imageRepository.findById(defaultImageId)
                    .orElseThrow(() -> new NotFoundException("Default image for CV was not found"));
            log.info("IN create - image for cv wasn't added. Default image will be added");
        }
        toSave.setImage(image);

        final byte[] bytes = getImageBytes(image);
        final File file = CvUtils.createCv(toSave, bytes);
        log.info("IN create - cv was locally created");

        final String link = amazonService.uploadFile(file, CV);
        file.delete();
        log.info("IN create - cv was uploaded. Link: {}", link);

        List<CvEducationEntity> education = toSave.getEducation();
        List<CvWorkExperienceEntity> workExperience = toSave.getWorkExperience();
        List<CvProjectEntity> projects = toSave.getProjects();

        toSave.setEducation(null);
        toSave.setWorkExperience(null);
        toSave.setProjects(null);

        toSave.setCreatedAt(now());
        toSave.setFileUrl(link);

        CvEntity saved = cvRepository.save(toSave);

        education.forEach(e -> e.setCv(saved));
        workExperience.forEach(e -> e.setCv(saved));
        projects.forEach(e -> e.setCv(saved));

        saved.setEducation(education);
        saved.setWorkExperience(workExperience);
        saved.setProjects(projects);

        cvRepository.save(saved);
        log.info("IN create - cv was saved: {}", saved);

        emailService.sendCvConfirmationEmail(CV_MAPPER.toModel(saved));
        emailService.sendNewCvEmail(CV_MAPPER.toModel(saved));

        return link;
    }

    private byte[] getImageBytes(ImageEntity image) {
        return amazonService.downloadCvImageAsBytes(image.getName());
    }

    private void fillTempFile(Resource resource, File temp) throws IOException {
        byte[] data;
        try (InputStream is = resource.getInputStream()) {
            data = new byte[is.available()];
            is.read(data);
        }

        try (FileOutputStream fos = new FileOutputStream(temp)) {
            fos.write(data);
        }
    }

}
