package by.itstep.career.service.impl;

import by.itstep.career.entity.vacancy.VacancyEntity;
import by.itstep.career.model.vacancy.Vacancy;
import by.itstep.career.model.vacancy.VacancyPreview;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.model.vacancy.request.VacancyUpdateRequest;
import by.itstep.career.repository.VacancyRepository;
import by.itstep.career.service.VacancyService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static by.itstep.career.mapper.VacancyMapper.VACANCY_MAPPER;
import static by.itstep.career.utils.ExceptionUtils.notFoundException;
import static java.time.LocalDateTime.now;

@Slf4j
@Service
@AllArgsConstructor
public class VacancyServiceImpl implements VacancyService {
    
    private final VacancyRepository vacancyRepository;
    
    @Override
    @Transactional
    public Vacancy create(final VacancyCreationRequest request) {
        final VacancyEntity toSave = VACANCY_MAPPER.toEntity(request);
        toSave.setCreatedAt(now());
        final VacancyEntity saved = vacancyRepository.save(toSave);
        log.info("IN create - vacancy: {} successfully created", saved);
        return VACANCY_MAPPER.toModel(saved);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<VacancyPreview> findAll(final Integer page, final Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        final Page<VacancyPreview> vacancies = vacancyRepository.findAll(pageable)
                .map(VACANCY_MAPPER::toPreview);
        log.info("IN findAll - {} vacancies were found", vacancies.getContent().size());
        return vacancies;
    }

    @Override
    @Transactional(readOnly = true)
    public Vacancy get(final UUID id) {
        final VacancyEntity vacancy = vacancyRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("vacancy with id : %s not found", id)));
        log.info("IN get - vacancy: {} found by id: {}", vacancy, id);
        return VACANCY_MAPPER.toModel(vacancy);
    }

    @Override
    @Transactional
    public Vacancy update(final VacancyUpdateRequest request) {
        final VacancyEntity toUpdate = vacancyRepository.findById(request.getId())
                .orElseThrow(() -> notFoundException(String.format("vacancy with id : %s not found", request.getId())));

        toUpdate.setCompany(request.getCompany());
        toUpdate.setDescription(request.getDescription());
        toUpdate.setExperience(VACANCY_MAPPER.toEntity(request.getExperience()));
        toUpdate.setName(request.getName());
        toUpdate.setUpdatedAt(now());

        final VacancyEntity updated = vacancyRepository.save(toUpdate);
        log.info("IN update - vacancy: {} successfully updated", updated);
        return VACANCY_MAPPER.toModel(updated);
    }

    @Override
    @Transactional
    public void delete(final UUID id) {
        final VacancyEntity vacancy = vacancyRepository.findById(id)
                .orElseThrow(() -> notFoundException(String.format("vacancy with id : %s not found", id)));

        vacancy.setDeletedAt(now());

        final VacancyEntity updated = vacancyRepository.save(vacancy);
        log.info("IN delete - vacancy: {} updated with status 'DELETED'", updated);
    }

}
