package by.itstep.career.service.impl;

import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventPreview;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.vacancy.VacancyPreview;
import by.itstep.career.service.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;

@Service
@AllArgsConstructor
public class ViewServiceImpl implements ViewService {

    private final ConsultationEntryService consultationEntryService;
    private final CvService cvService;
    private final EventEntryService eventEntryService;
    private final EventService eventService;
    private final UserService userService;
    private final VacancyService vacancyService;
    private final StoryService storyService;

    @Override
    public ModelAndView getMainPage() {
        Page<EventPreview> events = eventService.findAllPublished(0, 3);
        Page<VacancyPreview> vacancies = vacancyService.findAll(0, 9);
        Page<Story> stories = storyService.findAllPublished(0, 2);

        ModelAndView mav = new ModelAndView("view/index");

        mav.addObject("events", events.getContent());
        mav.addObject("vacancies", vacancies.getContent());

        mav.addObject("firstStory", stories.getContent().get(0));
        mav.addObject("secondStory", stories.getContent().get(1));

        return mav;
    }

    @Override
    public ModelAndView getAllVacanciesPage(Integer page) {
        Page<VacancyPreview> vacancies = vacancyService.findAll(page - 1, 18);
        ModelAndView mav = new ModelAndView("view/vacancies");
        mav.addObject("vacancies", vacancies.getContent());
        return mav;
    }

    @Override
    public ModelAndView getAllStoriesPage(Integer page) {
        Page<Story> stories = storyService.findAllPublished(page - 1, 3);
        ModelAndView mav = new ModelAndView("view/stories");
        mav.addObject("stories", stories.getContent());
        return mav;
    }

    @Override
    public ModelAndView getAllEventsPage(Integer page) {
        Page<EventPreview> events = eventService.findAllPublished(page - 1, 9);
        ModelAndView mav = new ModelAndView("view/events");
        mav.addObject("events", events.getContent());
        return mav;
    }

    @Override
    public ModelAndView getAllProjectsPage(Integer page) {
        ModelAndView mav = new ModelAndView("view/projects");
        return mav;
    }

    @Override
    public ModelAndView getPartnersPage() {
        ModelAndView mav = new ModelAndView("view/partners");
        return mav;
    }

    @Override
    public ModelAndView getSingleEventPage(UUID id) {
        Event event = eventService.get(id);
        ModelAndView mav = new ModelAndView("view/single-event");
        mav.addObject("event", event);
        return mav;
    }

    @Override
    public ModelAndView getCreateResumePage() {
        ModelAndView mav = new ModelAndView("view/create-cv");
        return mav;
    }

    @Override
    public ModelAndView getSingleProjectPage(Integer id) {
        if (id == 0) {
            return new ModelAndView("view/single-project");
        } else if (id == 1) {
            return new ModelAndView("view/single-project-dev");
        } else {
            return new ModelAndView("view/single-project-video");
        }
    }

}
