package by.itstep.career.service;

import by.itstep.career.model.event.EventEntry;
import by.itstep.career.model.event.EventEntryPreview;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import by.itstep.career.model.event.request.EventEntryUpdateRequest;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.UUID;

public interface EventEntryService {

    EventEntry create(EventEntryCreationRequest request);

    EventEntry get(UUID id);

    Page<EventEntryPreview> findByEventId(UUID eventId, Integer page, Integer size);

    Page<EventEntryPreview> findByEmail(String email, Integer page, Integer size);

    void delete(UUID id);

}
