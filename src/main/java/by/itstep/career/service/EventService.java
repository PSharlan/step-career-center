package by.itstep.career.service;

import by.itstep.career.model.image.Image;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventPreview;
import by.itstep.career.model.event.enums.EventStatus;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface EventService {

    Event get(UUID id);

    Page<EventPreview> findAll(Integer page, Integer size);

    Page<EventPreview> findAllPublished(Integer page, Integer size);

    Page<EventPreview> findByStatus(EventStatus status, Integer page, Integer size);

    Event create(EventCreationRequest request);

    Event update(EventUpdateRequest request);

    Event updateStatus(UUID id, EventStatus status);

    void delete(UUID id);

}
