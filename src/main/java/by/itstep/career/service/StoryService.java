package by.itstep.career.service;

import by.itstep.career.model.image.Image;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.story.enums.StoryStatus;
import by.itstep.career.model.story.request.StoryCreationRequest;
import by.itstep.career.model.story.request.StoryUpdateRequest;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface StoryService {

    Story get(UUID id);

    Page<Story> findAll(Integer page, Integer size);

    Page<Story> findAllPublished(Integer page, Integer size);

    Page<Story> findByStatus(StoryStatus status, Integer page, Integer size);

    Story create(StoryCreationRequest request);

    Story update(StoryUpdateRequest request);

    Story updateStatus(UUID id, StoryStatus status);

    void delete(UUID id);

}
