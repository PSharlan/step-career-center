package by.itstep.career.security;

import by.itstep.career.entity.user.UserEntity;
import by.itstep.career.entity.user.enums.RoleEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class JwtUserDetailsFactory {

    public static JwtUserDetails create(final UserEntity user){
        return new JwtUserDetails(user.getUsername(),
                user.getPassword(),
                user.getDeletedAt() == null,
                mapToGrantedAuthorities(user.getRoles()));
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(final List<RoleEntity> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.name()))
                .collect(toList());
    }

}
