package by.itstep.career.security;

import by.itstep.career.model.user.User;
import by.itstep.career.model.user.enums.Role;
import by.itstep.career.service.UserService;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class JwtTokenProvider {

    @Value("${jwt.secretKey}")
    private String secret;

    @Value("${jwt.expirationTime}")
    private long timeOut;

    private final UserDetailsService jwtUserDetailsService;
    private final UserService userService;

    @PostConstruct
    protected void init() {
        secret = Base64.getEncoder().encodeToString(secret.getBytes());
    }

    public String createToken(final String username) {
        final User user = userService.getByUsername(username);
        final Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", getRoleNames(user.getRoles()));
        claims.put("name", user.getUsername());

        final Date now = new Date();
        final Date validity = new Date(new Date().getTime() + timeOut);

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(validity)
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public Authentication getAuthentication(final String token) {
        final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
        final JwtPrincipal principal = new JwtPrincipal(claims.getBody().get("name", String.class),
                                                  claims.getBody().get("roles", List.class));

        return new JwtAuthenticationToken(principal);
    }

    public String getUsername(final String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody().getSubject();
    }

    public String resolveToken(final HttpServletRequest req) {
        final String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer_")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public boolean validateToken(final String token) {
        try {
            final Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);
            return !claims.getBody().getExpiration().before(new Date());
        } catch (JwtException | IllegalArgumentException e) {
            throw new JwtAuthenticationException("JWT token is expired or invalid");
        }
    }

    private List<String> getRoleNames(final List<Role> roles) {
        return roles.stream().map(Role::name).collect(Collectors.toList());
    }

    static class JwtAuthenticationException extends AuthenticationException {
        JwtAuthenticationException(final String message) {
            super(message);
        }
    }

}
