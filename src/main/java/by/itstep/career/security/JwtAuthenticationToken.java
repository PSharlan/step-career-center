package by.itstep.career.security;

import org.springframework.security.authentication.AbstractAuthenticationToken;

import javax.security.auth.Subject;

public class JwtAuthenticationToken extends AbstractAuthenticationToken {

    private Object principal;
    private Object credentials;

    public JwtAuthenticationToken(final JwtPrincipal principal) {
        super(principal.getAuthorities());
        this.principal = principal;
        super.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean implies(final Subject subject) {
        return false;
    }

}
