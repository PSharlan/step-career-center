package by.itstep.career.security;

import by.itstep.career.entity.user.UserEntity;
import by.itstep.career.model.user.User;
import by.itstep.career.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static by.itstep.career.utils.ExceptionUtils.notFoundException;
import static java.lang.String.format;

@Slf4j
@Service
@AllArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final UserEntity user = userRepository.findByUsername(username)
                .orElseThrow(() -> notFoundException(format("User with username : %s not found", username)));

        return JwtUserDetailsFactory.create(user);
    }

}
