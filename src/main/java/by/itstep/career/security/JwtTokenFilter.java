package by.itstep.career.security;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@AllArgsConstructor
public class JwtTokenFilter extends GenericFilterBean {

    private JwtTokenProvider jwtTokenProvider;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException {
        //get token from Http request
        final String token = jwtTokenProvider.resolveToken((HttpServletRequest) request);
        //validate token. Check secret key and expiration time
        if (token != null && jwtTokenProvider.validateToken(token)) {
            //create authentication object
            final Authentication authentication = jwtTokenProvider.getAuthentication(token);
            if (authentication != null) {
                //put authentication object to the Security Context
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        chain.doFilter(request, response);
    }

}
