package by.itstep.career.utils;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class ConsultationTimeUtils {

    public static final List<Integer> DEFAULT_SLOTS_HRS = new ArrayList<>();

    static {
        DEFAULT_SLOTS_HRS.add(10);
        DEFAULT_SLOTS_HRS.add(11);
        DEFAULT_SLOTS_HRS.add(12);
        DEFAULT_SLOTS_HRS.add(13);
        DEFAULT_SLOTS_HRS.add(14);
        DEFAULT_SLOTS_HRS.add(15);
        DEFAULT_SLOTS_HRS.add(16);
        DEFAULT_SLOTS_HRS.add(17);
        DEFAULT_SLOTS_HRS.add(18);
        DEFAULT_SLOTS_HRS.add(19);
    }

}
