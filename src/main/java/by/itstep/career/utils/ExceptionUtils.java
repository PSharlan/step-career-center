package by.itstep.career.utils;

import by.itstep.career.exception.*;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ExceptionUtils {

    public static NotFoundException notFoundException(final String message) {
        return new NotFoundException(message);
    }

    public static EntityAlreadyExistsException entityAlreadyExistsException(final String message) {
        return new EntityAlreadyExistsException(message);
    }

    public static EntityIsDeletedException entityIsDeletedException(final String message) {
        return new EntityIsDeletedException(message);
    }

    public static PasswordsNotEqualsException passwordsNotEqualsException(final String message) {
        return new PasswordsNotEqualsException(message);
    }

    public static IllegalArgumentException illegalArgumentException(final String message) {
        return new IllegalArgumentException(message);
    }

    public static EntryIsNotPossibleException entryIsNotPossibleException(final String message) {
        return new EntryIsNotPossibleException(message);
    }

    public static InvalidLinkException invalidLinkException(final String message) {
        return new InvalidLinkException(message);
    }

}
