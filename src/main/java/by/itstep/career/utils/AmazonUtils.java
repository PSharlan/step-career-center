package by.itstep.career.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AmazonUtils {

    private static final String READ_ONLY_POLICY_JSON;

    static {
        READ_ONLY_POLICY_JSON = "{\n" +
                "     \"Statement\": [\n" +
                "         {\n" +
                "             \"Action\": [\n" +
                "                 \"s3:GetBucketLocation\",\n" +
                "                 \"s3:ListBucket\"\n" +
                "             ],\n" +
                "             \"Effect\": \"Allow\",\n" +
                "             \"Principal\": \"*\",\n" +
                "             \"Resource\": \"arn:aws:s3:::%1$s\"\n" +
                "         }," +
                "         {\n" +
                "             \"Action\": \"s3:GetObject\",\n" +
                "             \"Effect\": \"Allow\",\n" +
                "             \"Principal\": \"*\",\n" +
                "             \"Resource\": \"arn:aws:s3:::%1$s/*\"\n" +
                "         }\n" +
                "       ],\n" +
                "     \"Version\": \"2012-10-17\"\n" +
                " }";
    }

    public static String getReadOnlyPolicyJsonRequest(String bucketName) {
        return String.format(READ_ONLY_POLICY_JSON, bucketName);
    }

}
