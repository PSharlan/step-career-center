package by.itstep.career.utils;

import by.itstep.career.entity.cv.CvEducationEntity;
import by.itstep.career.entity.cv.CvEntity;
import by.itstep.career.entity.cv.CvProjectEntity;
import by.itstep.career.entity.cv.CvWorkExperienceEntity;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.itextpdf.text.BaseColor.BLACK;
import static com.itextpdf.text.BaseColor.DARK_GRAY;
import static com.itextpdf.text.Font.FontFamily.HELVETICA;
import static com.itextpdf.text.Font.ITALIC;
import static com.itextpdf.text.Font.NORMAL;
import static java.time.format.DateTimeFormatter.ofPattern;

@UtilityClass
public class CvUtils {

    private static final DateTimeFormatter TIME_FORMATTER = ofPattern("dd.MM.yyyy");

    private static final Font MAIN_HEADER = new Font(HELVETICA, 20, NORMAL, BLACK);
    private static final Font HEADER_BIG = new Font(HELVETICA, 14, NORMAL, new BaseColor(1, 111, 255, 180));
    private static final Font HEADER = new Font(HELVETICA, 10, NORMAL, DARK_GRAY);
    private static final Font HEADER_SMALL = new Font(HELVETICA, 10, ITALIC, BLACK);
    private static final Font INFO = new Font(HELVETICA, 10, NORMAL, DARK_GRAY);
    private static final Font INFO_SMALL = new Font(HELVETICA, 9, NORMAL, DARK_GRAY);

    private static final String SKILLS_HEADER = "SKILLS";
    private static final String PERSONALITY_HEADER = "PERSONALITY";
    private static final String WORK_EXPERIENCE_HEADER = "WORK EXPERIENCE";
    private static final String EDUCATION_HEADER = "EDUCATION";
    private static final String PROJECTS_HEADER = "PROJECTS";

    @SneakyThrows
    public static File createCv(final CvEntity cv, final byte[] bytes) {
        final File file = new File(String.format("CV_%s_%s.pdf", cv.getEmail().split("@")[0], cv.getName()));
        final Document document = new Document(PageSize.A4);
        final PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file.getName()));
        writer.setPdfVersion(PdfWriter.VERSION_1_7);

        document.open();
        document.add(createHeader(cv, bytes));
        document.add(createSkillsDescription(cv));
        document.add(createPersonalityDescription(cv));
        document.add(createWorkExperience(cv));
        document.add(createEducation(cv));
        document.add(createProjects(cv));
        document.close();

        return file;
    }

    private static PdfPTable createHeader(final CvEntity cv, final byte[] bytes) throws IOException, DocumentException {
        PdfPTable table = new PdfPTable(2);
        table.setWidthPercentage(100);
        table.setWidths(new int[]{7,3});

        Paragraph pName = new Paragraph(new Chunk(cv.getName()));
        pName.setFont(MAIN_HEADER);
        Paragraph pPosition = new Paragraph(new Chunk(cv.getPosition()));
        pPosition.setFont(HEADER_BIG);
        pPosition.setSpacingAfter(10);
        Paragraph pEmail = new Paragraph(new Chunk("Email: " + cv.getEmail()));
        pEmail.setFont(HEADER_SMALL);
        Paragraph pPhone = new Paragraph(new Chunk("Phone: " + cv.getPhone()));
        pPhone.setFont(HEADER_SMALL);
        Paragraph pSkype = new Paragraph(new Chunk("Skype: " + cv.getSkype()));
        pSkype.setFont(HEADER_SMALL);
        Paragraph pLinkedin = new Paragraph(new Chunk("Linkedin: " + cv.getLinkedin()));
        pLinkedin.setFont(HEADER_SMALL);

        PdfPCell cName = new PdfPCell();
        cName.addElement(pName);
        cName.addElement(pPosition);
        cName.addElement(pEmail);
        cName.addElement(pPhone);
        cName.addElement(pSkype);
        cName.addElement(pLinkedin);
        cName.setBorder(Rectangle.NO_BORDER);

        Image i = Image.getInstance(bytes); //TODO
        PdfPCell cImage = new PdfPCell(new Phrase(new Chunk(i, 0, -150)));
        cImage.setFixedHeight(170);
        cImage.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cImage.setBorder(Rectangle.NO_BORDER);

        table.addCell(cName);
        table.addCell(cImage);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        return table;
    }

    private static PdfPTable createSkillsDescription(CvEntity cv) {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        Paragraph pSkills = new Paragraph(new Chunk(SKILLS_HEADER));
        pSkills.setFont(HEADER_BIG);
        pSkills.setSpacingAfter(5);
        Paragraph pSkillsDescription = new Paragraph(new Chunk(cv.getSkills()));
        pSkillsDescription.setFont(INFO_SMALL);

        PdfPCell cSkills = new PdfPCell();
        cSkills.addElement(pSkills);
        cSkills.addElement(pSkillsDescription);
        cSkills.setBorder(Rectangle.NO_BORDER);

        table.addCell(cSkills);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        return table;
    }

    private static PdfPTable createPersonalityDescription(CvEntity cv) {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        Paragraph pPersonality = new Paragraph(new Chunk(PERSONALITY_HEADER));
        pPersonality.setFont(HEADER_BIG);
        pPersonality.setSpacingAfter(5);
        Paragraph pPersonalityDescription = new Paragraph(new Chunk(cv.getPersonalDetails()));
        pPersonalityDescription.setFont(INFO_SMALL);

        PdfPCell cPersonality = new PdfPCell();
        cPersonality.addElement(pPersonality);
        cPersonality.addElement(pPersonalityDescription);
        cPersonality.setBorder(Rectangle.NO_BORDER);

        table.addCell(cPersonality);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        return table;
    }

    private static PdfPTable createWorkExperience(CvEntity cv) {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        Paragraph pWorkExperience = new Paragraph(new Chunk(WORK_EXPERIENCE_HEADER));
        pWorkExperience.setFont(HEADER_BIG);

        PdfPCell cWorkExperienceHeader = new PdfPCell();
        cWorkExperienceHeader.addElement(pWorkExperience);
        cWorkExperienceHeader.setBorder(Rectangle.NO_BORDER);
        table.addCell(cWorkExperienceHeader);

        List<CvWorkExperienceEntity> workExperience = cv.getWorkExperience();
        workExperience.forEach(w -> {
            Chunk chunkPositionHeader = new Chunk("Position: ");
            chunkPositionHeader.setFont(HEADER_SMALL);
            Chunk chunkEmployerHeader = new Chunk("Employer: ");
            chunkEmployerHeader.setFont(HEADER_SMALL);
            Chunk chunkResponsibilitiesHeader = new Chunk("Responsibilities: ");
            chunkResponsibilitiesHeader.setFont(HEADER_SMALL);
            Chunk chunkPosition = new Chunk(w.getPosition());
            chunkPosition.setFont(HEADER);

            Chunk chunkPositionDates = new Chunk(
                    String.format(" (%s - %s)",
                    w.getStartDate().format(TIME_FORMATTER),
                    w.isPresent() ? "present" : w.getEndDate().format(TIME_FORMATTER)));

            chunkPositionDates.setFont(HEADER_SMALL);
            Chunk chunkEmployer = new Chunk(w.getCompany());
            chunkEmployer.setFont(INFO);
            Chunk chunkResponsibilities = new Chunk(w.getResponsibilities());
            chunkResponsibilities.setFont(INFO_SMALL);


            Paragraph pPosition = new Paragraph();
            pPosition.add(chunkPositionHeader);
            pPosition.add(chunkPosition);
            pPosition.add(chunkPositionDates);

            Paragraph pEmployer = new Paragraph();
            pEmployer.add(chunkEmployerHeader);
            pEmployer.add(chunkEmployer);

            Paragraph pResponsibilities = new Paragraph();
            pResponsibilities.add(chunkResponsibilitiesHeader);
            pResponsibilities.add(chunkResponsibilities);

            PdfPCell cWorkExperience = new PdfPCell();
            cWorkExperience.addElement(pPosition);
            cWorkExperience.addElement(pEmployer);
            cWorkExperience.addElement(pResponsibilities);
            cWorkExperience.setBorder(Rectangle.NO_BORDER);

            table.addCell(cWorkExperience);
        });

        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        return table;
    }

    private static PdfPTable createEducation(CvEntity cv) {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        Paragraph pEducationHeader = new Paragraph(new Chunk(EDUCATION_HEADER));
        pEducationHeader.setFont(HEADER_BIG);

        PdfPCell cEducationHeader = new PdfPCell();
        cEducationHeader.addElement(pEducationHeader);
        cEducationHeader.setBorder(Rectangle.NO_BORDER);
        table.addCell(cEducationHeader);

        List<CvEducationEntity> education = cv.getEducation();
        education.forEach(e -> {
            Chunk chunkOrganizationHeader = new Chunk("Organization: ");
            chunkOrganizationHeader.setFont(HEADER_SMALL);
            Chunk chunkSpecializationHeader = new Chunk("Specialization: ");
            chunkSpecializationHeader.setFont(HEADER_SMALL);
            Chunk chunkOrganization = new Chunk(e.getOrganization());
            chunkOrganization.setFont(HEADER);
            Chunk chunkOrganizationDates = new Chunk(
                    String.format(" (%s - %s)",
                    e.getStartDate().format(TIME_FORMATTER),
                    e.isPresent() ? "present" : e.getEndDate().format(TIME_FORMATTER)));

            chunkOrganizationDates.setFont(HEADER_SMALL);
            Chunk chunkSpecialization = new Chunk(e.getSpecialization());
            chunkSpecialization.setFont(INFO);

            Paragraph pOrganization = new Paragraph();
            pOrganization.add(chunkOrganizationHeader);
            pOrganization.add(chunkOrganization);
            pOrganization.add(chunkOrganizationDates);

            Paragraph pSpecialization = new Paragraph();
            pSpecialization.add(chunkSpecializationHeader);
            pSpecialization.add(chunkSpecialization);

            PdfPCell cEducation = new PdfPCell();
            cEducation.addElement(pOrganization);
            cEducation.addElement(pSpecialization);
            cEducation.setBorder(Rectangle.NO_BORDER);

            table.addCell(cEducation);
        });

        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        return table;
    }

    private static PdfPTable createProjects(CvEntity cv) {
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        Paragraph pProjects = new Paragraph(new Chunk(PROJECTS_HEADER));
        pProjects.setFont(HEADER_BIG);

        PdfPCell cProjects = new PdfPCell();
        cProjects.addElement(pProjects);
        cProjects.setBorder(Rectangle.NO_BORDER);
        table.addCell(cProjects);

        List<CvProjectEntity> projects = cv.getProjects();
        projects.forEach(p -> {
            Chunk chunkNameHeader = new Chunk("Name: ");
            chunkNameHeader.setFont(HEADER_SMALL);
            Chunk chunkEnvironmentHeader = new Chunk("Environment: ");
            chunkEnvironmentHeader.setFont(HEADER_SMALL);
            Chunk chunkDescriptionHeader = new Chunk("Description: ");
            chunkDescriptionHeader.setFont(HEADER_SMALL);
            Chunk chunkAchievementsHeader = new Chunk("Achievements:");
            chunkAchievementsHeader.setFont(HEADER_SMALL);


            Chunk chunkName = new Chunk(p.getName());
            chunkName.setFont(HEADER);
            Chunk chunkEnvironment = new Chunk(p.getEnvironment());
            chunkEnvironment.setFont(INFO);
            Chunk chunkDescription = new Chunk(p.getDescription());
            chunkDescription.setFont(INFO_SMALL);
            Chunk chunkAchievements = new Chunk(p.getAchievements());
            chunkAchievements.setFont(INFO_SMALL);


            Paragraph pName = new Paragraph();
            pName.add(chunkNameHeader);
            pName.add(chunkName);

            Paragraph pEnvironment = new Paragraph();
            pEnvironment.add(chunkEnvironmentHeader);
            pEnvironment.add(chunkEnvironment);

            Paragraph pDescription = new Paragraph();
            pDescription.add(chunkDescriptionHeader);
            pDescription.add(chunkDescription);

            Paragraph pAchievements = new Paragraph();
            pAchievements.add(chunkAchievementsHeader);
            pAchievements.add(chunkAchievements);

            PdfPCell cProjectsDescription = new PdfPCell();
            cProjectsDescription.addElement(pName);
            cProjectsDescription.addElement(pEnvironment);
            cProjectsDescription.addElement(pDescription);
            cProjectsDescription.addElement(pAchievements);
            cProjectsDescription.setBorder(Rectangle.NO_BORDER);

            table.addCell(cProjectsDescription);
        });

        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        return table;
    }

}
