package by.itstep.career.utils;

import lombok.experimental.UtilityClass;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;

import static by.itstep.career.utils.ExceptionUtils.invalidLinkException;
import static java.lang.String.format;
import static java.net.URI.create;
import static org.springframework.web.util.UriComponentsBuilder.fromUri;

@UtilityClass
public class UriUtils {

    public static String buildEmbedYoutubeLink(final String uri) {
        final UriComponents uriComponents = fromUri(create(uri)).build();
        final MultiValueMap<String, String> queryParams = uriComponents.getQueryParams();
        final String videoId = queryParams.getFirst("v");

        if (videoId == null) {
            throw invalidLinkException("Youtube link hasn't 'v' query parameter");
        }

        return format("https://www.youtube.com/embed/%s", videoId);
    }

}
