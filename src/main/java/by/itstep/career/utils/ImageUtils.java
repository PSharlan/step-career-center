package by.itstep.career.utils;

import lombok.experimental.UtilityClass;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@UtilityClass
public class ImageUtils {

    public static File resizeImage(final File file, final int height, final int width) throws IOException {
        final BufferedImage image = ImageIO.read(file);

        final BufferedImage resized = resize(image, height, width);
        final File output = new File(file.getName());
        ImageIO.write(resized, "png", output);

        return output;
    }

    private static BufferedImage resize(final BufferedImage img, final int height, final int width) {
        final Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        final BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        final Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();
        return resized;
    }

}
