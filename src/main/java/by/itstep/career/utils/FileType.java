package by.itstep.career.utils;

import lombok.Getter;

public enum FileType {

    CV("cv");

    @Getter
    private String folder;


    FileType(final String folder) {
        this.folder = folder;
    }

}