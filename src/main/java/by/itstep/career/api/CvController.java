package by.itstep.career.api;

import by.itstep.career.model.cv.request.CvCreationRequest;
import by.itstep.career.model.image.Image;
import by.itstep.career.service.CvService;
import by.itstep.career.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.CV_IMAGE;
import static org.springframework.http.HttpStatus.CREATED;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/cvs")
@RequestMapping("/api/v1/cvs")
public class CvController {

    private final CvService cvService;
    private final ImageService imageService;

    @PostMapping
    @ApiOperation(value = "Create new cv", notes = "Required cv instances")
    public ResponseEntity<String> createCv(
            @ApiParam(value = "CvCreationRequest instance", required = true)
            @Valid @RequestBody final CvCreationRequest cv) {
        final String link = cvService.create(cv);
        return new ResponseEntity<>(link, CREATED);
    }

    @PostMapping("/images")
    @ApiOperation(value = "Upload event image ", notes = "Required multipart file")
    public ResponseEntity<Image> uploadCvImage(
            @ApiParam(value = "EventCreationRequest instance", required = true)
            @RequestPart(value = "file") final MultipartFile file) {
        final Image image = imageService.save(file, CV_IMAGE);
        return new ResponseEntity<>(image, CREATED);
    }

}
