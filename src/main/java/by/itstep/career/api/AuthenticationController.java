package by.itstep.career.api;

import by.itstep.career.model.auth.AuthenticationRequest;
import by.itstep.career.security.JwtTokenProvider;
import io.swagger.annotations.Api;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/auth")
@RequestMapping("/api/v1/auth")
public class AuthenticationController {

    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    @PostMapping("/login")
    public ResponseEntity<Map<Object, Object>> login(@RequestBody AuthenticationRequest req) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(req.getUsername(), req.getPassword()));
        //FIXME token have to contain username and ROLES?
        String token = jwtTokenProvider.createToken(req.getUsername());
        Map<Object, Object> responseMap = new HashMap<>();
        responseMap.put("username", req.getUsername());
        responseMap.put("token", token);
        return ResponseEntity.ok(responseMap);
    }

}

