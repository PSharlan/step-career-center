package by.itstep.career.api;

import by.itstep.career.service.ViewService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;

@Controller
@AllArgsConstructor
public class ViewController {

    private final ViewService viewService;

    @GetMapping(value = "/index")
    public ModelAndView mainView() {
        return viewService.getMainPage();
    }

    @GetMapping(value = "/vacancies")
    public ModelAndView vacanciesView(@RequestParam(value = "page") final Integer page) {
        return viewService.getAllVacanciesPage(page);
    }

    @GetMapping(value = "/stories")
    public ModelAndView storiesView(@RequestParam(value = "page") final Integer page) {
        return viewService.getAllStoriesPage(page);
    }

    @GetMapping(value = "/events")
    public ModelAndView eventsView(@RequestParam(value = "page") final Integer page) {
        return viewService.getAllEventsPage(page);
    }

    @GetMapping(value = "/events/{id}")
    public ModelAndView singleEventView(@PathVariable final UUID id) {
        return viewService.getSingleEventPage(id);
    }

    @GetMapping(value = "/projects")
    public ModelAndView partnersView(@RequestParam(value = "page") final Integer page) {
        return viewService.getAllProjectsPage(page);
    }

    @GetMapping(value = "/projects/{id}")
    public ModelAndView singeProjectView(@PathVariable final Integer id) {
        return viewService.getSingleProjectPage(id);
    }

    @GetMapping(value = "/create-cv")
    public ModelAndView createResumeView() {
        return viewService.getCreateResumePage();
    }

    @GetMapping(value = "/partners")
    public ModelAndView partnersView() {
        return viewService.getPartnersPage();
    }

}
