package by.itstep.career.api;

import by.itstep.career.model.vacancy.Vacancy;
import by.itstep.career.model.vacancy.VacancyPreview;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.model.vacancy.request.VacancyUpdateRequest;
import by.itstep.career.service.VacancyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/vacancies")
@RequestMapping("/api/v1/vacancies")
public class VacancyController {

    private final VacancyService vacancyService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_VACANCY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Create new vacancy", notes = "Required VacancyCreationRequest instances")
    public ResponseEntity<Vacancy> createVacancy(
            @ApiParam(value = "VacancyCreationRequest instance", required = true)
            @Valid @RequestBody final VacancyCreationRequest request) {
        final Vacancy created = vacancyService.create(request);
        return new ResponseEntity<>(created, CREATED);
    }

    @GetMapping
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Return all existing vacancy previews")
    public ResponseEntity<List<VacancyPreview>> getAllVacancies(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<VacancyPreview> vacancies = vacancyService.findAll(page, size);
        return new ResponseEntity<>(vacancies.getContent(), OK);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Return vacancy by id")
    public ResponseEntity<Vacancy> getVacancyById(
            @ApiParam(value = "Id of a vacancy to lookup for", required = true)
            @PathVariable final UUID id) {
        final Vacancy vacancy = vacancyService.get(id);
        return new ResponseEntity<>(vacancy, OK);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_VACANCY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update vacancy", notes = "Required VacancyUpdateRequest instance")
    public ResponseEntity<Vacancy> updateVacancy(
            @ApiParam(value = "Vacancy instance", required = true)
            @Valid @RequestBody final VacancyUpdateRequest request) {
        final Vacancy updated = vacancyService.update(request);
        return new ResponseEntity<>(updated, OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_VACANCY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Delete vacancy by id")
    public ResponseEntity<Void> deleteVacancy(
            @ApiParam(value = "Id of a vacancy to delete", required = true)
            @PathVariable final UUID id) {
        vacancyService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
