package by.itstep.career.api;

import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.consultation.ConsultationEntryPreview;
import by.itstep.career.model.consultation.enums.ConsultationStatus;
import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.consultation.request.ConsultationEntryUpdateRequest;
import by.itstep.career.service.ConsultationEntryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/consultations/entries")
@RequestMapping("/api/v1/consultations/entries")
public class ConsultationEntryController {

    private final ConsultationEntryService consultationEntryService;

    @PostMapping
    @ApiOperation(value = "Create new consultation entry", notes = "Required ConsultationEntry instances")
    public ResponseEntity<ConsultationEntry> createConsultationEntry(
            @ApiParam(value = "ConsultationEntryCreationRequest instance", required = true)
            @Valid @RequestBody final ConsultationEntryCreationRequest request) {
        final ConsultationEntry created = consultationEntryService.create(request);
        return new ResponseEntity<>(created, CREATED);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Return all existing consultation entries")
    public ResponseEntity<List<ConsultationEntryPreview>> getAllConsultationEntries(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<ConsultationEntryPreview> entries = consultationEntryService.findAll(page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @GetMapping("/time/available")
    @ApiOperation(value = "Return all available consultation time")
    public ResponseEntity<List<Integer>> getAvailableTime(
            @RequestParam(value = "day") @DateTimeFormat(pattern="yyyy-MM-dd") final LocalDate day) {
        final List<Integer> slots = consultationEntryService.findAvailableTime(day);
        return new ResponseEntity<>(slots, OK);
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Return consultation entry by id")
    public ResponseEntity<ConsultationEntry> getConsultationEntryById(
            @ApiParam(value = "Id of an consultation entry to lookup for", required = true)
            @PathVariable final UUID id) {
        final ConsultationEntry consultationEntry = consultationEntryService.get(id);
        return new ResponseEntity<>(consultationEntry, OK);
    }

    @GetMapping(value = "/status")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return consultation entries by consultation status")
    public ResponseEntity<List<ConsultationEntryPreview>> getConsultationEntriesByStatus(
            @RequestParam(value = "status") ConsultationStatus status,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<ConsultationEntryPreview> entries = consultationEntryService.findByStatus(status, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @GetMapping(value = "/email")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return consultation entries by email")
    public ResponseEntity<List<ConsultationEntryPreview>> getConsultationEntriesByEmail(
            @RequestParam(value = "email") final String email,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<ConsultationEntryPreview> entries = consultationEntryService.findByEmail(email, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @GetMapping(value = "/phone")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return consultation entries by phone")
    public ResponseEntity<List<ConsultationEntryPreview>> getConsultationEntriesByPhone(
            @RequestParam(value = "phone") final String phone,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<ConsultationEntryPreview> entries = consultationEntryService.findByPhone(phone, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_CONSULTANT') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update consultation entry", notes = "Required ConsultationEntryUpdateRequest instance")
    public ResponseEntity<ConsultationEntry> updateConsultationEntry(
            @ApiParam(value = "ConsultationEntry instance", required = true)
            @Valid @RequestBody final ConsultationEntryUpdateRequest request) {
        final ConsultationEntry updated = consultationEntryService.update(request);
        return new ResponseEntity<>(updated, OK);
    }

    @PutMapping(value = "/{id}/status")
    @PreAuthorize("hasRole('ROLE_CONSULTANT') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update consultation entry status", notes = "Required ConsultationStatus instance")
    public ResponseEntity<ConsultationEntry> updateConsultationEntryStatus(
            @ApiParam(value = "Id of an consultation entry to lookup for", required = true)
            @PathVariable final UUID id,
            @ApiParam(value = "ConsultationEntry instance", required = true)
            @Valid @RequestBody final ConsultationStatus status) {
        final ConsultationEntry updated = consultationEntryService.updateStatus(id, status);
        return new ResponseEntity<>(updated, OK);
    }


    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_CONSULTANT') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Delete consultation entry by id")
    public ResponseEntity<Void> deleteConsultationEntry(
            @ApiParam(value = "Id of a consultation entry to delete", required = true)
            @PathVariable final UUID id) {
        consultationEntryService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
