package by.itstep.career.api;

import by.itstep.career.model.user.request.ChangePasswordRequest;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.request.UserCreationRequest;
import by.itstep.career.model.user.request.UserUpdateRequest;
import by.itstep.career.model.user.enums.Role;
import by.itstep.career.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/users")
@RequestMapping("/api/v1/users")
public class UserController {

    private final UserService userService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Create new user", notes = "Required UserCreationRequest instance")
    public ResponseEntity<User> createUser(
            @ApiParam(value = "UserCreationRequest instance", required = true)
            @Valid @RequestBody final UserCreationRequest request) {
        final User user = userService.create(request);
        return new ResponseEntity<>(user, CREATED);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return all active users")
    public ResponseEntity<List<User>> getAllUsers(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<User> users = userService.findAll(page, size);
        return new ResponseEntity<>(users.getContent(), OK);
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return user by id")
    public ResponseEntity<User> getUserById(
            @ApiParam(value = "Id of a user to lookup for", required = true)
            @PathVariable final UUID id) {
        final User user = userService.get(id);
        return new ResponseEntity<>(user, OK);
    }

    @GetMapping(value = "/email")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return user by email")
    public ResponseEntity<User> getUserByEmail(
            @RequestParam(value = "email") final String email) {
        final User user = userService.getByEmail(email);
        return new ResponseEntity<>(user, OK);
    }

    @GetMapping(value = "/username")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return user by username")
    public ResponseEntity<User> getUserByUsername(
            @RequestParam(value = "username") final String username) {
        final User user = userService.getByUsername(username);
        return new ResponseEntity<>(user, OK);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update user", notes = "Required UserUpdateRequest instance")
    public ResponseEntity<User> updateUser(
            @ApiParam(value = "User instance", required = true)
            @Valid @RequestBody final UserUpdateRequest request) {
        final User updated = userService.update(request);
        return new ResponseEntity<>(updated, OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Delete user by id")
    public ResponseEntity<Void> deleteUser(
            @ApiParam(value = "Id of a user to delete", required = true)
            @PathVariable final UUID id) {
        userService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

    @PutMapping(value = "current/password")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Change user password", notes = "User email, old and new passwords are required.")
    public ResponseEntity<User> changePassword(
            @ApiParam(value = "Request info", required = true)
            @Valid @RequestBody final ChangePasswordRequest request) { //TODO changePasswordRequest contains email but should not!
        final User user = userService.changePassword(request);
        return new ResponseEntity<>(user, OK);
    }

    @PutMapping(value = "/{id}/roles")
    @PreAuthorize("hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update user roles", notes = "User id and Set of roles are required.")
    public ResponseEntity<User> changeRole(
            @ApiParam(value = "User id")
            @PathVariable final UUID id,
            @ApiParam(value = "List of all roles that have to be assigned")
            @RequestBody final List<Role> roles) {
        final User user = userService.changeRole(id, roles);
        return new ResponseEntity<>(user, OK);
    }

}
