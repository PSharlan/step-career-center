package by.itstep.career.api;

import by.itstep.career.model.event.EventEntry;
import by.itstep.career.model.event.EventEntryPreview;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import by.itstep.career.model.event.request.EventEntryUpdateRequest;
import by.itstep.career.service.EventEntryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/events")
@RequestMapping("/api/v1/events")
public class EventEntryController {

    private final EventEntryService eventEntryService;

    @PostMapping("/entries")
    @ApiOperation(value = "Create new event entry", notes = "Required EventEntryCreationRequest instances")
    public ResponseEntity<EventEntry> createEventEntry(
            @ApiParam(value = "EventEntryCreationRequest instance", required = true)
            @Valid @RequestBody final EventEntryCreationRequest request) {
        final EventEntry created = eventEntryService.create(request);
        return new ResponseEntity<>(created, CREATED);
    }

    @GetMapping(value = "/entries/{id}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Return event entry by id")
    public ResponseEntity<EventEntry> getEventEntryById(
            @ApiParam(value = "Id of an event entry to lookup for", required = true)
            @PathVariable final UUID id) {
        final EventEntry eventEntry = eventEntryService.get(id);
        return new ResponseEntity<>(eventEntry, OK);
    }

    @GetMapping(value = "/entries/email")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return entry previews by email")
    public ResponseEntity<List<EventEntryPreview>> getEventEntriesByEmail(
            @RequestParam(value = "email") final String email,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<EventEntryPreview> entries = eventEntryService.findByEmail(email, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @GetMapping(value = "/{id}/entries")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return entry previews by event id")
    public ResponseEntity<List<EventEntryPreview>> getEventEntriesByEventId(
            @ApiParam(value = "Id of an event to lookup for", required = true)
            @PathVariable UUID id,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<EventEntryPreview> entries = eventEntryService.findByEventId(id, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @DeleteMapping(value = "/entries/{id}")
    @PreAuthorize("hasRole('ROLE_EVENT_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Delete event entry by id")
    public ResponseEntity<Void> deleteEventEntry(
            @ApiParam(value = "Id of an event entry to delete", required = true)
            @PathVariable final UUID id) {
        eventEntryService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
