package by.itstep.career.api;

import by.itstep.career.model.image.Image;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventPreview;
import by.itstep.career.model.event.enums.EventStatus;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import by.itstep.career.service.EventService;
import by.itstep.career.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.CV_IMAGE;
import static by.itstep.career.entity.image.enums.ImageTypeEntity.EVENT_IMAGE;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/events")
@RequestMapping("/api/v1/events")
public class EventController {

    private final EventService eventService;
    private final ImageService imageService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_EVENT_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Create new event", notes = "Required EventCreationRequest instances")
    public ResponseEntity<Event> createEvent(
            @ApiParam(value = "EventCreationRequest instance", required = true)
            @Valid @RequestBody final EventCreationRequest request) {
        final Event created = eventService.create(request);
        return new ResponseEntity<>(created, CREATED);
    }

    @PostMapping("/images")
    @PreAuthorize("hasRole('ROLE_EVENT_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Upload event image ", notes = "Required multipart file")
    public ResponseEntity<Image> uploadEventImage(
            @ApiParam(value = "EventCreationRequest instance", required = true)
            @RequestPart(value = "file") final MultipartFile file) {
        final Image image = imageService.save(file, EVENT_IMAGE);
        return new ResponseEntity<>(image, CREATED);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Return all existing event previews")
    public ResponseEntity<List<EventPreview>> getAllEvents(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<EventPreview> events = eventService.findAll(page, size);
        return new ResponseEntity<>(events.getContent(), OK);
    }

    @GetMapping("/published")
    @ApiOperation(value = "Return all existing event previews")
    public ResponseEntity<List<EventPreview>> getAllPublishedEvents(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<EventPreview> events = eventService.findAllPublished(page, size);
        return new ResponseEntity<>(events.getContent(), OK);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Return event by id")
    public ResponseEntity<Event> getEventById(
            @ApiParam(value = "Id of an event to lookup for", required = true)
            @PathVariable final UUID id) {
        final Event event = eventService.get(id);
        return new ResponseEntity<>(event, OK);
    }

    @GetMapping(value = "/status")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return event reviews by event status")
    public ResponseEntity<List<EventPreview>> getEventsByStatus(
            @RequestParam(value = "status") final EventStatus status,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<EventPreview> entries = eventService.findByStatus(status, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_EVENT_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update event", notes = "Required EventUpdateRequest instance")
    public ResponseEntity<Event> updateEvent(
            @ApiParam(value = "EventUpdateRequest instance", required = true)
            @Valid @RequestBody final EventUpdateRequest request) {
        final Event updated = eventService.update(request);
        return new ResponseEntity<>(updated, OK);
    }

    @PutMapping("/{id}/status")
    @PreAuthorize("hasRole('ROLE_EVENT_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update event status", notes = "Required EventUpdateRequest instance")
    public ResponseEntity<Event> updateEventStatus(
            @ApiParam(value = "Id of an event to lookup for", required = true)
            @PathVariable final UUID id,
            @ApiParam(value = "EventUpdateRequest instance", required = true)
            @Valid @RequestBody final EventStatus status) {
        final Event updated = eventService.updateStatus(id, status);
        return new ResponseEntity<>(updated, OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_EVENT_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Delete event by id")
    public ResponseEntity<Void> deleteEvent(
            @ApiParam(value = "Id of an event to delete", required = true)
            @PathVariable final UUID id) {
        eventService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
