package by.itstep.career.api;

import by.itstep.career.model.image.Image;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.story.enums.StoryStatus;
import by.itstep.career.model.story.request.StoryCreationRequest;
import by.itstep.career.model.story.request.StoryUpdateRequest;
import by.itstep.career.service.ImageService;
import by.itstep.career.service.StoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static by.itstep.career.entity.image.enums.ImageTypeEntity.CV_IMAGE;
import static by.itstep.career.entity.image.enums.ImageTypeEntity.STORY_IMAGE;
import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestController
@AllArgsConstructor
@Api(value = "/api/v1/stories")
@RequestMapping("/api/v1/stories")
public class StoryController {

    private final StoryService storyService;
    private final ImageService imageService;

    @PostMapping
    @PreAuthorize("hasRole('ROLE_STORY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Create new story", notes = "Required StoryCreationRequest instances")
    public ResponseEntity<Story> createStory(
            @ApiParam(value = "StoryCreationRequest instance", required = true)
            @Valid @RequestBody final StoryCreationRequest request) {
        final Story created = storyService.create(request);
        return new ResponseEntity<>(created, CREATED);
    }

    @PostMapping("/images")
    @PreAuthorize("hasRole('ROLE_STORY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Upload event image ", notes = "Required multipart file")
    public ResponseEntity<Image> uploadStoryImage(
            @ApiParam(value = "EventCreationRequest instance", required = true)
            @RequestPart(value = "file") final MultipartFile file) {
        final Image image = imageService.save(file, STORY_IMAGE);
        return new ResponseEntity<>(image, CREATED);
    }

    @GetMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Return all existing story previews")
    public ResponseEntity<List<Story>> getAllStories(
            @RequestParam(value = "page") Integer page,
            @RequestParam(value = "size") Integer size) {
        final Page<Story> stories = storyService.findAll(page, size);
        return new ResponseEntity<>(stories.getContent(), OK);
    }

    @GetMapping("/published")
    @ApiOperation(value = "Return all existing story previews")
    public ResponseEntity<List<Story>> getAllPublishedStories(
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<Story> stories = storyService.findAllPublished(page, size);
        return new ResponseEntity<>(stories.getContent(), OK);
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Return story by id")
    public ResponseEntity<Story> getStoryById(
            @ApiParam(value = "Id of an story to lookup for", required = true)
            @PathVariable UUID id) {
        final Story story = storyService.get(id);
        return new ResponseEntity<>(story, OK);
    }

    @GetMapping(value = "/status")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "return story reviews by story status")
    public ResponseEntity<List<Story>> getStoriesByStatus(
            @RequestParam(value = "status") final StoryStatus status,
            @RequestParam(value = "page") final Integer page,
            @RequestParam(value = "size") final Integer size) {
        final Page<Story> entries = storyService.findByStatus(status, page, size);
        return new ResponseEntity<>(entries.getContent(), OK);
    }

    @PutMapping
    @PreAuthorize("hasRole('ROLE_STORY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update story", notes = "Required StoryUpdateRequest instance")
    public ResponseEntity<Story> updateStory(
            @ApiParam(value = "StoryUpdateRequest instance", required = true)
            @Valid @RequestBody final StoryUpdateRequest request) {
        final Story updated = storyService.update(request);
        return new ResponseEntity<>(updated, OK);
    }

    @PutMapping("/{id}/status")
    @PreAuthorize("hasRole('ROLE_STORY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Update story status", notes = "Required StoryUpdateRequest instance")
    public ResponseEntity<Story> updateStoryStatus(
            @ApiParam(value = "Id of an story to lookup for", required = true)
            @PathVariable final UUID id,
            @ApiParam(value = "StoryUpdateRequest instance", required = true)
            @Valid @RequestBody final StoryStatus status) {
        final Story updated = storyService.updateStatus(id, status);
        return new ResponseEntity<>(updated, OK);
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ROLE_STORY_MANAGER') or hasRole('ROLE_SUPERUSER')")
    @ApiOperation(authorizations = {@Authorization(value = "Authorization")},
            value = "Delete story by id")
    public ResponseEntity<Void> deleteStory(
            @ApiParam(value = "Id of an story to delete", required = true)
            @PathVariable final UUID id) {
        storyService.delete(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
