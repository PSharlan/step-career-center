package by.itstep.career.repository;

import by.itstep.career.entity.event.EventEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface EventRepository extends JpaRepository<EventEntity, UUID> {

    @Query(
            value = "SELECT * FROM career_center.event " +
                    "WHERE id = :id and deleted is null",
            nativeQuery = true)
    Optional<EventEntity> findById(@Param("id") UUID id);

    @Query(
            value = "SELECT * FROM career_center.event " +
                    "WHERE deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.event",
            nativeQuery = true)
    Page<EventEntity> findAll(Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.event " +
                    "WHERE status = :status and deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.event",
            nativeQuery = true)
    Page<EventEntity> findByEventStatus(@Param("status") String status, Pageable pageable);

}
