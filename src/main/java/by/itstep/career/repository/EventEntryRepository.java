package by.itstep.career.repository;

import by.itstep.career.entity.event.EventEntryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface EventEntryRepository extends JpaRepository<EventEntryEntity, UUID> {

    @Query(
            value = "SELECT * FROM career_center.event_entry " +
                    "WHERE id = :id and deleted is null",
            nativeQuery = true)
    Optional<EventEntryEntity> findById(@Param("id") UUID id);

    @Query(
            value = "SELECT * FROM career_center.event_entry " +
                    "WHERE email = :email and deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.event_entry",
            nativeQuery = true)
    Page<EventEntryEntity> findByEmail(@Param("email") String email, Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.event_entry " +
                    "WHERE event_id = :id and deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.event_entry",
            nativeQuery = true)
    Page<EventEntryEntity> findByEventId(@Param("id") UUID eventId, Pageable pageable);


    @Query(
            value = "SELECT count(*)> 0 FROM career_center.event_entry " +
                    "WHERE event_id = :eventId AND email = :email",
            nativeQuery = true)
    boolean existsByEmailAndEventId(@Param("eventId") UUID eventId, @Param("email") String email);

}
