package by.itstep.career.repository;

import by.itstep.career.entity.vacancy.VacancyEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface VacancyRepository extends JpaRepository<VacancyEntity, UUID> {

    @Query(
            value = "SELECT * FROM career_center.vacancy " +
                    "WHERE id = :id and deleted is null",
            nativeQuery = true)
    Optional<VacancyEntity> findById(@Param("id") UUID id);

    @Query(
            value = "SELECT * FROM career_center.vacancy " +
                    "WHERE deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.vacancy",
            nativeQuery = true)
    Page<VacancyEntity> findAll(Pageable pageable);

}
