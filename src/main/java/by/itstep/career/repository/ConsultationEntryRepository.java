package by.itstep.career.repository;

import by.itstep.career.entity.consultation.ConsultationEntryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ConsultationEntryRepository extends JpaRepository<ConsultationEntryEntity, UUID> {

    @Query(
            value = "SELECT * FROM career_center.consultation_entry " +
                    "WHERE id = :id and deleted is null;",
            nativeQuery = true)
    Optional<ConsultationEntryEntity> findById(@Param("id") UUID id);

    @Query(
            value = "SELECT * FROM career_center.consultation_entry " +
                    "WHERE deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.consultation_entry",
            nativeQuery = true)
    Page<ConsultationEntryEntity> findAll(Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.consultation_entry " +
                    "WHERE email = :email and deleted is null "+
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.consultation_entry",
            nativeQuery = true)
    Page<ConsultationEntryEntity> findByEmail(@Param("email") String email, Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.consultation_entry " +
                    "WHERE phone = :phone and deleted is null "+
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.consultation_entry",
            nativeQuery = true)
    Page<ConsultationEntryEntity> findByPhone(@Param("phone") String phone, Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.consultation_entry " +
                    "WHERE consultation_status = :status and deleted is null "+
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.consultation_entry",
            nativeQuery = true)
    Page<ConsultationEntryEntity> findByConsultationStatus(@Param("status") String status, Pageable pageable);

    @Query(
            value = "SELECT assigned FROM career_center.consultation_entry " +
                    "WHERE assigned > :startDate " +
                          "and assigned < :endDate " +
                          "and deleted is null",
            nativeQuery = true)
    List<Date> findAllReservedTime(@Param("startDate") LocalDateTime startDate, @Param("endDate") LocalDateTime endDate);

}
