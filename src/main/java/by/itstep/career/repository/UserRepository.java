package by.itstep.career.repository;

import by.itstep.career.entity.user.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Query(
            value = "SELECT * FROM career_center.user " +
                    "WHERE id = :id and deleted is null",
            nativeQuery = true)
    Optional<UserEntity> findById(@Param("id") UUID id);

    @Query(
            value = "SELECT * FROM career_center.user " +
                    "WHERE deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.user",
            nativeQuery = true)
    Page<UserEntity> findAll(Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.user " +
                    "WHERE username = :username and deleted is null",
            nativeQuery = true)
    Optional<UserEntity> findByUsername(@Param("username") String username);

    @Query(
            value = "SELECT * FROM career_center.user " +
                    "WHERE email = :email and deleted is null",
            nativeQuery = true)
    Optional<UserEntity> findByEmail(@Param("email") String email);

    @Query(
            value = "SELECT * FROM career_center.user " +
                    "JOIN career_center.user_role " +
                    "ON career_center.user.id = career_center.user_role.user_id " +
                    "WHERE career_center.user_role.role_name = :user_role",
            nativeQuery = true)
    List<UserEntity> findByRole(@Param("user_role") String role);

    @Query(
            value = "SELECT count(*) > 0 " +
                    "FROM career_center.user " +
                    "WHERE username = :username",
            nativeQuery = true)
    boolean existsByUsername(@Param("username") String username);

    @Query(
            value = "SELECT count(*) > 0 " +
                    "FROM career_center.user " +
                    "WHERE email = :email",
            nativeQuery = true)
    boolean existsByEmail(@Param("email") String email);

}
