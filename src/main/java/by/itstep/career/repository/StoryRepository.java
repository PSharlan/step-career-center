package by.itstep.career.repository;

import by.itstep.career.entity.story.StoryEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface StoryRepository extends JpaRepository<StoryEntity, UUID> {

    @Query(
            value = "SELECT * FROM career_center.story " +
                    "WHERE id = :id and deleted is null",
            nativeQuery = true)
    Optional<StoryEntity> findById(@Param("id") UUID id);

    @Query(
            value = "SELECT * FROM career_center.story " +
                    "WHERE deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.story",
            nativeQuery = true)
    Page<StoryEntity> findAll(Pageable pageable);

    @Query(
            value = "SELECT * FROM career_center.story " +
                    "WHERE status = :status and deleted is null " +
                    "ORDER BY created",
            countQuery = "SELECT count(*) FROM career_center.story",
            nativeQuery = true)
    Page<StoryEntity> findByStoryStatus(@Param("status") String status, Pageable pageable);

}
