package by.itstep.career.repository;

import by.itstep.career.entity.cv.CvEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CvRepository extends JpaRepository<CvEntity, UUID> {
}
