package by.itstep.career.mapper;

import by.itstep.career.entity.event.EventEntity;
import by.itstep.career.entity.event.enums.EventStatusEntity;
import by.itstep.career.model.event.Event;
import by.itstep.career.model.event.EventPreview;
import by.itstep.career.model.event.enums.EventStatus;
import by.itstep.career.model.event.request.EventCreationRequest;
import by.itstep.career.model.event.request.EventUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EventMapper {

    EventMapper EVENT_MAPPER = Mappers.getMapper(EventMapper.class);

    Event toModel(EventEntity event);

    EventPreview toPreview(EventEntity event);

    @Mapping(target = "image", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "entries", ignore = true)
    @Mapping(target = "status", ignore = true)
    EventEntity toEntity(EventCreationRequest request);

    EventStatusEntity toEntity(EventStatus status);

}
