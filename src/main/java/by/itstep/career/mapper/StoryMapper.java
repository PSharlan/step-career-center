package by.itstep.career.mapper;

import by.itstep.career.entity.story.StoryEntity;
import by.itstep.career.entity.story.enums.StoryStatusEntity;
import by.itstep.career.model.story.Story;
import by.itstep.career.model.story.enums.StoryStatus;
import by.itstep.career.model.story.request.StoryCreationRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StoryMapper {

    StoryMapper STORY_MAPPER = Mappers.getMapper(StoryMapper.class);

    Story toModel(StoryEntity story);

    @Mapping(target = "image", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "videoEmbedUrl", ignore = true)
    StoryEntity toEntity(StoryCreationRequest request);

    StoryStatusEntity toEntity(StoryStatus status);

}
