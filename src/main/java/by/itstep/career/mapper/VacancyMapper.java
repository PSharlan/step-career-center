package by.itstep.career.mapper;

import by.itstep.career.entity.vacancy.VacancyEntity;
import by.itstep.career.entity.vacancy.enums.VacancyExperienceEntity;
import by.itstep.career.model.vacancy.Vacancy;
import by.itstep.career.model.vacancy.VacancyPreview;
import by.itstep.career.model.vacancy.enums.VacancyExperience;
import by.itstep.career.model.vacancy.request.VacancyCreationRequest;
import by.itstep.career.model.vacancy.request.VacancyUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface VacancyMapper {

    VacancyMapper VACANCY_MAPPER = Mappers.getMapper(VacancyMapper.class);

    Vacancy toModel(VacancyEntity vacancy);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    VacancyEntity toEntity(VacancyCreationRequest request);

    VacancyPreview toPreview(VacancyEntity vacancyEntity);

    VacancyExperienceEntity toEntity(VacancyExperience experience);

}
