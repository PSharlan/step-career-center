package by.itstep.career.mapper;

import by.itstep.career.entity.event.EventEntryEntity;
import by.itstep.career.model.event.EventEntry;
import by.itstep.career.model.event.EventEntryPreview;
import by.itstep.career.model.event.request.EventEntryCreationRequest;
import by.itstep.career.model.event.request.EventEntryUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EventEntryMapper {

    EventEntryMapper EVENT_ENTRY_MAPPER = Mappers.getMapper(EventEntryMapper.class);

    EventEntry toModel(EventEntryEntity entry);

    @Mapping(target = "eventId", source = "event.id")
    @Mapping(target = "eventName", source = "event.name")
    EventEntryPreview toPreview(EventEntryEntity entry);

    @Mapping(target = "event", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    EventEntryEntity toEntity(EventEntryCreationRequest request);

}
