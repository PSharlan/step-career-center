package by.itstep.career.mapper;

import by.itstep.career.entity.cv.CvEntity;
import by.itstep.career.model.cv.Cv;
import by.itstep.career.model.cv.request.CvCreationRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CvMapper {

    CvMapper CV_MAPPER = Mappers.getMapper(CvMapper.class);

    Cv toModel(CvEntity entry);

    @Mapping(target = "image", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "fileUrl", ignore = true)
    CvEntity toEntity(CvCreationRequest request);

}
