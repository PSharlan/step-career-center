package by.itstep.career.mapper;

import by.itstep.career.entity.consultation.ConsultationEntryEntity;
import by.itstep.career.entity.consultation.enums.ConsultationStatusEntity;
import by.itstep.career.model.consultation.ConsultationEntry;
import by.itstep.career.model.consultation.ConsultationEntryPreview;
import by.itstep.career.model.consultation.enums.ConsultationStatus;
import by.itstep.career.model.consultation.request.ConsultationEntryCreationRequest;
import by.itstep.career.model.consultation.request.ConsultationEntryUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ConsultationEntryMapper {

    ConsultationEntryMapper CONSULTATION_ENTRY_MAPPER = Mappers.getMapper(ConsultationEntryMapper.class);

    ConsultationEntry toModel(ConsultationEntryEntity entry);

    ConsultationEntryPreview toPreview(ConsultationEntryEntity entry);

    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    ConsultationEntryEntity toEntity(ConsultationEntry entry);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "confirmedAt", ignore = true)
    @Mapping(target = "consultationStatus", ignore = true)
    ConsultationEntryEntity toEntity(ConsultationEntryCreationRequest request);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "confirmedAt", ignore = true)
    ConsultationEntryEntity toEntity(ConsultationEntryUpdateRequest request);

    ConsultationStatusEntity toEntity(ConsultationStatus status);

}
