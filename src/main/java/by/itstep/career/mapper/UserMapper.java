package by.itstep.career.mapper;

import by.itstep.career.entity.user.UserEntity;
import by.itstep.career.entity.user.enums.RoleEntity;
import by.itstep.career.model.user.User;
import by.itstep.career.model.user.enums.Role;
import by.itstep.career.model.user.request.UserCreationRequest;
import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import static org.mapstruct.factory.Mappers.getMapper;

@Mapper (builder = @Builder(disableBuilder = true))
public interface UserMapper {

    UserMapper USER_MAPPER = getMapper(UserMapper.class);

    User toModel(UserEntity user);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    @Mapping(target = "deletedAt", ignore = true)
    @Mapping(target = "roles", ignore = true)
    UserEntity toEntity(UserCreationRequest user);

    RoleEntity toEntity(Role role);

}
