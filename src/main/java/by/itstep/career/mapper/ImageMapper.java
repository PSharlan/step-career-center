package by.itstep.career.mapper;

import by.itstep.career.entity.image.ImageEntity;
import by.itstep.career.model.image.Image;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ImageMapper {

    ImageMapper IMAGE_MAPPER = Mappers.getMapper(ImageMapper.class);

    Image toModel(ImageEntity image);

}
